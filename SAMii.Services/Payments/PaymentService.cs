﻿using Ganemo.Framework.Util.Common;
using Ganemo.Framework.Util.Log4net;
using SAMii.Chargebee;
using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.Payments;
using SAMii.DTO.Students;
using SAMii.DTO.Teachers;
using SAMii.Interfaces.Auth;
using SAMii.Interfaces.Payments;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services.Payments
{
    public class PaymentService : IPaymentService
    {
        readonly IUserRepository _userRepo;
        readonly IUserAccessService _userAccessService;
        readonly IChargebeeApi _chargebeeService;
        readonly IBookingRepository _bookingRepository;
        readonly ILogger _logger;

        public PaymentService(IUserRepository userRepo,
             IChargebeeApi chargebeeService,
             IBookingRepository bookingRepository,
             ILogger logger,
             IUserAccessService userAccessService)
        {
            _userRepo = userRepo;
            _userAccessService = userAccessService;
            _chargebeeService = chargebeeService;
            _bookingRepository = bookingRepository;
            _logger = logger;
        }


        public CardData GetUserCardInfo(long userid)
        {
            var user = _userRepo.GetUserProfile(userid);
            CardData data = new CardData();

            if (user.BillingStatus != Enum.SubscriptionStatus.None)
                data = _chargebeeService.GetSubscriptionCardData(GUIDHelper.GetTokenStringRepresentation(user.Identifier));

            return data;
        }

        public bool CreateOrUpdateSubscription(BillingCreateData data)
        {
            bool result = false;
            if (data != null)
            {
                var user = this._userRepo.GetUserProfile(data.UserId);

                if (user.BillingStatus == Enum.SubscriptionStatus.None)
                {
                    SubscriptionCreateRequest request = new SubscriptionCreateRequest
                    {
                        User = user,
                        CB_Token = data.CB_Token
                    };
                    var response = _chargebeeService.CreateSubscription(request);
                    this._userRepo.UpdateUserBillingStatus(Enum.SubscriptionStatus.Active, user.Id);
                    result = response.Success;
                }
                else
                {
                    var response = _chargebeeService.UpdateSubscription(data.CB_Token, GUIDHelper.GetTokenStringRepresentation(user.Identifier));
                    result = response.Success;
                }
            }
            return result;
        }

        public Tuple<bool,bool> BookingRecurringPayments(BookingData booking)
        {
            int quantity = 3 - booking.InCreditCount; // Recurring payment should have incredit count always 3 as per the requirement
            if (quantity > 0)
            {
                string description = string.Format("#{0} - Lesson Booking Charge", booking.Id);
                var lesson = booking.LessonSlot.LessonDate.Lesson;

                BookingAddOnModel model = new BookingAddOnModel()
                {
                    SubscriptionId = GUIDHelper.GetTokenStringRepresentation(booking.Student.User.Identifier),
                    PaymentType = Enum.BookingPaymentType.Reoccurring,
                    LessonsQunatity = quantity,
                    UnitPrice = this.GetLessonPriceInCents(lesson.Price, lesson.IsCostAbsorb)
                };

                BookingPaymetResponse response = this._chargebeeService.RecurringBookingPaymentImmediate(model, description);
                if (response != null && response.Success)
                {
                    response.Payment.BookingId = booking.Id;
                    response.Payment.DateCreated = DateTime.UtcNow;
                    response.Success = _bookingRepository.AddBookingPayment(response.Payment);
                }

                return new Tuple<bool, bool>(false, response.Success);
            }
            return new Tuple<bool, bool>(true,false);
        }
        public bool LessonBookingPayment(LessonData lessonData, StudentData studentData, BookingRequest bookingRequest, long bookingId)
        {
            BookingAddOnModel model = new BookingAddOnModel();

            int lessonCount = bookingRequest.WeeksPaid.HasValue ? bookingRequest.WeeksPaid.Value : 3;

            model.SubscriptionId = GUIDHelper.GetTokenStringRepresentation(studentData.User.Identifier);
            model.LessonsQunatity = lessonCount;
            model.PaymentType = bookingRequest.PaymentType;
            model.UnitPrice = this.GetLessonPriceInCents(lessonData.Price, lessonData.IsCostAbsorb);

            BookingPaymetResponse response = this._chargebeeService.LessonBookingPayment(model);
            if(response != null && response.Success)
            {
                response.Payment.BookingId = bookingId;
                response.Payment.DateCreated = DateTime.UtcNow;
                response.Success = _bookingRepository.AddBookingPayment(response.Payment);
            }

            return response.Success;
        }

        private int GetLessonPriceInCents(Decimal lessonPrice, bool absorbCost)
        {
            int price = 0;

            price = (Decimal.ToInt32(lessonPrice)) * 100;

            if(!absorbCost)
            {
                var costs = (Decimal.ToInt32(lessonPrice) * AppConstants.LessonFeePercentage / 100);
                price = price + (int) (costs * 100);
            }
            return price;
        }

        public decimal GetTeacherLessonPrice(Decimal lessonPrice, bool absorbCost)
        {
            var price = lessonPrice;

            if (!absorbCost)
            {
                var cost = (lessonPrice * (decimal)AppConstants.LessonFeePercentage / 100);
                price = price + cost;
            }
            return price;
        }


        public bool ProcessRefunds(BookingCancelRequest request)
        {
            bool success = false;

            if (request != null && request.BookingId > 0 && request.CancelType == Enum.BookingCancelType.All)
            {
                var booking = _bookingRepository.GetBookingPaymentsByBkId(request.BookingId);
                var lesson = booking.LessonSlot.LessonDate.Lesson;

                var refundUnits = booking.InCreditCount;
                var refundAmount = refundUnits * this.GetLessonPriceInCents(lesson.Price, lesson.IsCostAbsorb);

                var invoiceid = this.GetRefundInvoiceId(booking);

                if (!string.IsNullOrWhiteSpace(invoiceid))
                {
                    BookingRefundRequest model = new BookingRefundRequest()
                    {
                        InvoiceId = invoiceid,
                        Quantity = refundUnits,
                        RefundAmount = refundAmount,
                        BookingId = request.BookingId
                    };

                    BookingRefundResponse response = this._chargebeeService.BookingRefunds(model);
                    if (response != null && response.Success)
                    {
                        response.Payment.BookingId = booking.Id;
                        response.Payment.DateCreated = DateTime.UtcNow;
                        response.Success = _bookingRepository.AddBookingRefundPayment(response.Payment);

                        success = true;
                    }
                }
            }

            return success;
        }

        private string GetRefundInvoiceId(BookingData booking)
        {
            if(booking != null && booking.Payments != null)
            {
                var firstPayment = booking.Payments.OrderBy(x => x.DateCreated).FirstOrDefault();

                if (firstPayment != null)
                   return firstPayment.GatewayId;
            }
            return null;
        }
    }
}
