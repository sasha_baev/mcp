﻿using SAMii.DTO;
using SAMii.DTO.Teachers;
using SAMii.Interfaces.Payments;
using SAMii.Repository.Interface;
using SAMii.Xero;
using SAMii.Xero.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services.Payments
{
    public class XeroService : IXeroService
    {
        private readonly ITeacherRepository _teacherRepository;
        private readonly IXeroHelperApi _xeroHelperApi;
        //private readonly IUserAccessService _userAccessService;

        public XeroService(ITeacherRepository provider, IXeroHelperApi xeroHelperApi)
            //IUserAccessService userAccessService)
        {
            _teacherRepository = provider;
            _xeroHelperApi = xeroHelperApi;
            //_userAccessService = userAccessService;
        }

        public void AddorUpdateContact(TeacherBillingData request)
        {
            if (request != null && request.TeacherBillingId > 0)
            {
                bool create = false;

                var record = _teacherRepository.GetTeacherBilling(request.TeacherBillingId);
                TeacherData teacherData = null;

                if (record == null || string.IsNullOrWhiteSpace(record.XeroContactId))
                {
                    teacherData = _teacherRepository.GetTeacherData(request.TeacherBillingId);
                    create = true;
                }
                else
                {
                    //if (request.XeroContactId != record.XeroContactId)
                    //    _userAccessService.ThrowForbiddenException();
                }
    

                if (create)
                {
                    ContactModel model = new ContactModel()
                    {
                        Firstname = teacherData.User.FirstName,
                        Lastname = teacherData.User.LastName,
                        Email = teacherData.User.Email,
                        AccountName = request.AccountName,
                        AccountNumber = request.AccountNumber,
                        BSB = request.BSB,
                        ABN = request.ABN
                    };
                    model.Fullname = model.GetFullName();

                    var contactID = this._xeroHelperApi.CreateContact(model);
                    request.XeroContactId = contactID;
                }
                else
                {
                    ContactModel model = new ContactModel()
                    {
                        AccountName = request.AccountName,
                        AccountNumber = request.AccountNumber,
                        BSB = request.BSB,
                        ABN = request.ABN,
                        XeroContactId = record.XeroContactId
                    };

                    var contactID = this._xeroHelperApi.UpdateContact(model);
                }
            }
        }

        public void GetBankDetails(TeacherBillingData data)
        {
            if (data != null && !string.IsNullOrWhiteSpace(data.XeroContactId))
            {
                var model = this._xeroHelperApi.GetContact(data.XeroContactId);

                if (model != null)
                {
                    data.ABN = model.ABN;
                    data.AccountNumber = model.AccountNumber;
                    data.BSB = model.BSB;
                }
            }
        }
        public bool CreateBill(TeacherPayoutData data)
        {
            return _xeroHelperApi.CreateBill(data);
        }
    }
}
