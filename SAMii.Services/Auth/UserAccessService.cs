﻿using Ganemo.Framework.Util.Log4net;
using SAMii.Interfaces.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services.Auth
{
    public class UserAccessService : IUserAccessService
    {
        readonly ILogger _logger;

        public UserAccessService(ILogger logger)
        {
            _logger = logger;
        }

        public void ThrowForbiddenException()
        {
            throw new Exception("UnauthorizedAccessException");
        }
    }
}
