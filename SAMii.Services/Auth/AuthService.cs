﻿using Ganemo.Framework.Util.Common;
using Ganemo.Framework.Util.Log4net;
using SAMii.DTO;
using SAMii.DTO.Auth;
using SAMii.DTO.User;
using SAMii.Enum;
using SAMii.Interfaces.Auth;
using SAMii.Interfaces.S3;
using SAMii.Interfaces.Utilities;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SAMii.Services.Auth
{
    public class AuthService : IAuthService
    {
        readonly ILogger _logger;
        readonly IUserRepository _userRepository;
        readonly IStudentRepository _studentRepo;
        readonly IParentRepository _parentRepo;
        readonly IValidatePassword _password;
        readonly IEmailService _emailService;
        readonly IS3Service _s3Service;

        // auth service can not use any other service (except email) as it might causes circular dependency
        public AuthService(ILogger logger, IUserRepository userRepository, IStudentRepository studentRepo, IParentRepository parentRepo,
                            IValidatePassword validate, IEmailService emailService, IS3Service s3Service)
        {
            _logger = logger;
            _password = validate;
            _userRepository = userRepository;
            _studentRepo = studentRepo;
            _parentRepo = parentRepo;
            _emailService = emailService;
            _s3Service = s3Service;
        }

        public async Task<LoginResponse> Login(LoginRequest request, string ipAddress)
        {
            if (string.IsNullOrWhiteSpace(request.Email) || string.IsNullOrWhiteSpace(request.Password))
            {
                return new LoginResponse { Result = LoginResponseEnum.NeedsBoth };
            }

            var user = _userRepository.SignOn(request.Email);
            if (user == null)
            {
                return new LoginResponse { Result = LoginResponseEnum.Unknown };
            }

            if (user.Password == null)
            {
                return new LoginResponse { Result = LoginResponseEnum.NotActivated };
            }

            if (!_password.Validate(user.Password, request.Password))
            {
                return new LoginResponse { Result = LoginResponseEnum.Unknown };
            }

            return await GetLoginResponse(user, ipAddress);
        }

        public SignUpResponse SignUp(SignUpRequest data)
        {
            var response = new SignUpResponse() { Result = SignUpResponseEnum.Fail };
            if (data != null)
            {
                // Check the email address is available
                if (_userRepository.GetByEmail(data.EmailAddress) != null)
                {
                    response.Result = SignUpResponseEnum.FailEmailAddressInUse;
                }
                else
                {
                    UserData newUser = null;
                    if (! String.IsNullOrEmpty(data.InvitationToken))
                        newUser = this.SignUpFromInvitation(data);
                    else
                        newUser = _userRepository.CreateUser(data.EmailAddress, _password.HashPassword(data.Password));


                    if (newUser != null && newUser.Id > 0)
                    {
                        this._emailService.SendRegisterNewUserEmail(newUser);
                        response.Result = SignUpResponseEnum.Success;
                    }
                }
            }
            return response;
        }

        private UserData SignUpFromInvitation(SignUpRequest data)
        {
            UserData newUser = null;
            var invitation = _userRepository.UpdateUserInvitation(GUIDHelper.GetTokenFromStringRepresentation(data.InvitationToken));

            // currently only parent can invite student
            if (invitation != null)
                newUser = _userRepository.CreateUser(data.EmailAddress, _password.HashPassword(data.Password), true);

            if (newUser != null)
            {
                var parent = _parentRepo.GetParent(invitation.FromUserId);
                _studentRepo.InitialiseStudent(newUser.Id, parent.Id);
            }

            return newUser;
        }


        private async Task<LoginResponse> GetLoginResponse(UserData user, string ipAddress)
        {
            if (user == null)
                return new LoginResponse { Result = LoginResponseEnum.Unknown };

            if (user.Suspended || user.IsDeleted)
                return new LoginResponse { Result = LoginResponseEnum.Suspended };

            if (!user.Activated)
                return new LoginResponse { Result = LoginResponseEnum.NotActivated };

            _userRepository.Login(user.Id, ipAddress);

            var res = new LoginResponse()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserRoleType = user.RoleType,
                AccessToken = this.CreateJwtToken(user),
                IsProfileSetup = (user.ProfileStep == GetRoleStepComplete(user)),
                IsInvited = user.IsInvited
            };
            if (user.AvatarS3Key != null)
            {
                using (Stream stream = await _s3Service.DownloadFile(user.AvatarS3Key))
                {
                    byte[] bytes;
                    using (var memoryStream = new MemoryStream())
                    {
                        stream.CopyTo(memoryStream);
                        bytes = memoryStream.ToArray();
                    }
                    res.AvatarFileData = Convert.ToBase64String(bytes);

                    return res;
                }
            } else
            {
                return res;
            }
        }

        public string CreateJwtToken(UserData user)
        {
            return _password.GenerateToken(user.Id, user.RoleType.ToString());
        }

        public ActivateUserResponse Activate(ActivateUserRequest data)
        {
            if (!String.IsNullOrEmpty(data.Token))
            {
                Guid? guid = GUIDHelper.GetTokenFromStringRepresentation(data.Token);
                if (guid.HasValue)
                {
                    return _userRepository.Activate(guid.Value);
                }
            }

            return new ActivateUserResponse { Result = TokenResponseEnum.TokenInvalid };
        }

        public ResetPasswordResponse ResetPasswordRequest(ForgotRequest data, string ipAddress)
        {
            var response = new ResetPasswordResponse() { Success = true };
            if (data != null && !String.IsNullOrEmpty(data.Email))
            {
                UserData user = _userRepository.ResetPassword(data.Email);

                if (user != null)
                {
                    // send email
                    _emailService.SendResetPasswordEmail(user);
                    _logger.Info(data.Email + " reset password. Reset Token: " + user.ResetToken);
                }
            }
            return response;
        }

        public VerifyTokenResponse VerifyResetToken(VerifyTokenRequest request)
        {
            VerifyTokenResponse response = new VerifyTokenResponse() { Result = TokenResponseEnum.TokenInvalid };

            Guid? guid = GUIDHelper.GetTokenFromStringRepresentation(request.Token);
            if (guid.HasValue)
            {
                UserData user = _userRepository.GetUserByResetToken(guid);

                if (user != null && user.ResetToken.HasValue)
                {
                    if (DateTime.Compare(DateTime.UtcNow, user.ResetTokenExpiry.Value) > 0)
                        response.Result = TokenResponseEnum.TokenExpired;
                    else
                    {
                        response.Result = TokenResponseEnum.NoError;
                        response.UserData = user;
                    }
                }
            }
            return response;
        }

        public PasswordChangeResponse ResetPassword(long userId, PasswordChangeRequest request)
        {
            var token = _userRepository.GetToken(userId);

            if (token == null || !_password.Validate(token, request.OldPassword))
                return new PasswordChangeResponse { Status = PasswordChangeEnum.InvalidCurrentPassword };

            if (request == null && String.IsNullOrEmpty(request.NewPassword))
                return new PasswordChangeResponse { Status = PasswordChangeEnum.InvalidNewPassword };

            _userRepository.ChangePassword(request.Id, _password.HashPassword(request.NewPassword));

            return new PasswordChangeResponse { Status = PasswordChangeEnum.Success };
        }

        public PasswordChangeResponse PublicResetPassword(PublicPasswordChangeRequest request)
        {
            VerifyTokenResponse res = VerifyResetToken(new VerifyTokenRequest { Token = request.Token });

            if (res.UserData != null && res.UserData.Id == request.Id)
            {
                _userRepository.ChangePassword(request.Id, _password.HashPassword(request.NewPassword));
                return new PasswordChangeResponse { Status = PasswordChangeEnum.Success };
            }
            return new PasswordChangeResponse() { Status = PasswordChangeEnum.InvalidNewPassword };
        }

        public int GetRoleStepComplete(UserData user) {
            int completeStep = -1;
            if (user.RoleType == UserType.Teacher)
            {
                completeStep = AppConstants.TeacherProfileComplete;
            }
            else if (user.RoleType == UserType.Student)
            {
                completeStep = AppConstants.StudentProfileComplete;
            }
            else if (user.RoleType == UserType.Parent)
            {
                completeStep = AppConstants.ParentProfileComplete;
            }

            return completeStep;
        }

        public UserInvitationData GetUserInvitation(VerifyTokenRequest request)
        {
            Guid? guid = GUIDHelper.GetTokenFromStringRepresentation(request.Token);
            if (guid.HasValue)
            {
                UserInvitationData invitation = _userRepository.GetUserInvitationByToken(guid);

                if (invitation != null && invitation.TokenExpiry.HasValue && DateTime.Compare(invitation.TokenExpiry.Value, DateTime.UtcNow) > 0)
                     return invitation;                   
            }
            return null;
        }
    }
}
