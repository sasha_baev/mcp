﻿using SAMii.DTO;
using SAMii.Interfaces;
using SAMii.Interfaces.Auth;
using SAMii.Interfaces.S3;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepo;
        readonly IAuthService _authService;
        readonly IInstrumentRepository _instrumentRepo;
        readonly IS3Service _s3Service;
        readonly ITeacherRepository _teacherRepository;
        readonly IStudentRepository _studentRepository;
        readonly IParentRepository _parentRepository;

        public UserService(IUserRepository userRepo, 
                           IAuthService authService, 
                           IInstrumentRepository instrumentRepo, 
                           IS3Service s3Service,
                           ITeacherRepository teacherRepository,
                           IStudentRepository studentRepository,
                           IParentRepository parentRepository)
        {
            _userRepo = userRepo;
            _authService = authService;
            _instrumentRepo = instrumentRepo;
            _s3Service = s3Service;
            _teacherRepository = teacherRepository;
            _studentRepository = studentRepository;
            _parentRepository = parentRepository;
        }

        public string ASSET_LOC = System.Configuration.ConfigurationManager.AppSettings["DefaultAssets"];

        public string GetUserAvatar(long userId)
        {
            var userData = _userRepo.GetUserProfile(userId);
            if (userData.AvatarS3Key != null)
                return _s3Service.GetPresignedUrl(userData.AvatarS3Key, DateTime.Now.AddMinutes(60));

            return null;
        }

        public UserRoleUpdateResponse UpdateUserRole(UserRoleUpdateRequest request)
        {
           // update user role
           var userData = _userRepo.UpdateUserRole(request);

           if (request.UserRoleType == Enum.UserType.Teacher)
           {
                _teacherRepository.InitialiseTeacher(userData);

                var defaultImagePath = "";
                string filePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                filePath = Path.Combine(filePath, "DefaultAssets", "DefaultIntroductionImage.png");
                if (File.Exists(filePath))
                {
                    defaultImagePath = filePath;
                } else
                {
                    string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                    UriBuilder uri = new UriBuilder(codeBase);
                    defaultImagePath = Path.Combine(Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path)), "User", "DefaultAssets", "DefaultIntroductionImage.png");

                }

                var introductionMediaS3Key = "IntroductionMedia/" + userData.Identifier.ToString();
                Stream fileStream = new FileStream(defaultImagePath, FileMode.Open, FileAccess.Read);
                Task.Run(() => _s3Service.UploadFile(introductionMediaS3Key, fileStream, "png"));
                
           } else if (request.UserRoleType == Enum.UserType.Student)
           {
                _studentRepository.InitialiseStudent(userData.Id);
           } else if (request.UserRoleType == Enum.UserType.Parent)
            {
                _parentRepository.InitialiseParent(userData.Id);
            }

            // refresh jwt token
            var newJWTToken = _authService.CreateJwtToken(userData);


            return new UserRoleUpdateResponse()
            {
                UserRoleType = request.UserRoleType,
                AccessToken = newJWTToken,
                IsProfileSetUp = (userData.ProfileStep == _authService.GetRoleStepComplete(userData))
            };
        }
    }
}
