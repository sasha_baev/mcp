﻿using Ganemo.Framework.Util.Log4net;
using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.Lessons;
using SAMii.DTO.Payments;
using SAMii.Interfaces;
using SAMii.Interfaces.Lessons;
using SAMii.Interfaces.Payments;
using SAMii.Interfaces.S3;
using SAMii.Interfaces.Utilities;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services.Lessons
{
    public class LessonService : ILessonService
    {
        readonly ILessonRepository _lessonRepository;
        readonly IUserRepository _userRepository;
        readonly ITeacherRepository _teacherRepository;
        readonly IS3Service _s3Service;
        readonly ILogger _logger;
        readonly IBookingService _bookingService;
        public LessonService(ILessonRepository lessonRepo, 
            IUserRepository userRepository, 
            ITeacherRepository teacherRepository, 
            IS3Service s3Service,
            ILogger logger,
            IBookingService bookingService)
        {
            _lessonRepository = lessonRepo;
            _userRepository = userRepository;
            _teacherRepository = teacherRepository;
            _s3Service = s3Service;
            _logger = logger;
            _bookingService = bookingService;
        }

        public LessonCreatePageModel GetNewLesson(LessonNewPageRequest request)
        {
            var teacher = _teacherRepository.GetTeacher(request.UserId);
            var currentLessons = _lessonRepository.GetAll(request.UserId);
            LessonData lesson = null;
            if (request.LessonId.HasValue)
            {
                lesson = GetById(request.LessonId.Value);
                currentLessons.Remove(currentLessons.First(x => x.Id == lesson.Id));
            } else
            {
                lesson = new LessonData {
                    TeacherId = teacher.Id,
                    Teacher = teacher
                };
            }

            return new LessonCreatePageModel
            {
                Lesson = lesson,
                Instruments = teacher.Instruments,
                TeacherLocations = teacher.TeacherLocations,
                CurrentLessons = currentLessons,
                LessonFeePercentage = AppConstants.LessonFeePercentage
            };
        }

        public LessonData GetById(long id)
        {
            var result = _lessonRepository.GetById(id);
            if (result.LessonIntroductionS3Key != null)
                result.LessonIntroductionPresignedUrl = _s3Service.GetPresignedUrl(result.LessonIntroductionS3Key, DateTime.Now.AddMinutes(60));

            foreach (var resource in result.LessonResources)
            {
                if (resource.LessonResourceS3Url != null)
                    resource.LessonResourcePresignedUrl = _s3Service.GetPresignedUrl(resource.LessonResourceS3Url, DateTime.Now.AddMinutes(60));
            }

            return result;
        }

        public IList<LessonData> GetAll(long userId)
        {
            return _lessonRepository.GetAll(userId);
        }

        public LessonDashboardPageModel GetLessonDashboard(long userId)
        {
            LessonDashboardPageModel pageModel = new LessonDashboardPageModel() { };
            var teacher = _teacherRepository.GetTeacher(userId);

            pageModel.LessonSummary = _lessonRepository.GetAll(userId);
            pageModel.TeacherLocations = _teacherRepository.GetTeacherLocations(teacher.Id);
            pageModel.Unavailabilities = _lessonRepository.GetUnavailabilitiesByTeacherId(teacher.Id);
            return pageModel;
        }

        public async Task<LessonData> AddLesson(LessonSaveRequest request)
        {
            var teacher = _teacherRepository.GetTeacher(request.UserId);
            request.Lesson.TeacherId = teacher.Id;

            var result = _lessonRepository.AddLesson(request.Lesson);

            if (request.Lesson.LessonResources != null)
            {
                var resourcesToDelete = result.LessonResources;
                foreach (var resource in request.Lesson.LessonResources)
                {
                    resource.LessonId = result.Id;
                    if (resource.NewResource)
                    {
                        resource.LessonResourceS3Url = "Lesson/" + result.Guid.ToString() + "/Resource/" + Guid.NewGuid();
                        var fileData = Convert.FromBase64String(resource.FileData);
                        Stream fileStream = new MemoryStream(fileData);
                        await (Task.Run(() => _s3Service.UploadFile(resource.LessonResourceS3Url, fileStream, resource.MimeType)));
                    }
                    else
                    {
                        var savedResource = result.LessonResources.First(x => x.Id == resource.Id);
                        resource.LessonResourceS3Url = savedResource.LessonResourceS3Url;
                        resource.FileName = savedResource.FileName;
                        resource.LessonId = savedResource.LessonId;
                        resourcesToDelete.Remove(savedResource);
                    }
                }

                foreach (var resource in resourcesToDelete)
                {
                    _s3Service.DeleteKey(resource.LessonResourceS3Url);
                }

                result.LessonResources = _lessonRepository.UpdateLessonResources(new LessonResourceUpdateRequest
                {
                    LessonId = result.Id,
                    LessonResources = request.Lesson.LessonResources
                });
            }

            return result;
        }

        public void DeleteLesson(long lessonId)
        {
            // Todo: check user permissions
            _lessonRepository.DeleteLesson(lessonId);
        }

        public UnavailabilityPageModel GetUnavailability(UnavailabilityNewPageRequest request)
        {
            var teacher = _teacherRepository.GetTeacher(request.UserId);
            UnavailabilityData unavailability = null;
            if (request.UnavailabilityId.HasValue)
            {
                unavailability = _lessonRepository.GetUnavailabilityById(request.UnavailabilityId.Value);
            }
            else
            {
                unavailability = new UnavailabilityData
                {
                    TeacherId = teacher.Id,
                    Teacher = teacher
                };
            }

            return new UnavailabilityPageModel
            {
                Unavailability = unavailability
            };
        }

        public UnavailabilityData SaveUnavailability(UnavailabilityData request)
        {
            var tuple = _lessonRepository.SaveUnavailability(request);
            var bookingCancellations = tuple.Item2;

            // Future optimisation: extend CancelBooking method to include a data range
            foreach (var cancellation in bookingCancellations)
            {
                _bookingService.CancelBooking(cancellation);
            }

            return tuple.Item1;
        }


        private async Task<S3FileData> UpdateLessonIntroductionS3Key(LessonSaveRequest request)
        {
            LessonData lesson = null;
            if (request.Lesson.Id != 0)
                lesson = _lessonRepository.GetById(request.Lesson.Id);

            S3FileData s3FileData = new S3FileData();

            if (request.NewLessonIntroduction)
            {
                // lesson / guid / introduction
                // lesson / guid / resource / resource guid
                var lessonIntroductionS3Key = "Lesson/" + lesson.Guid.ToString() + "/Introduction";
                if (request.LessonIntroductionFileData != null)
                {
                    var lessonIntroFileData = Convert.FromBase64String(request.LessonIntroductionFileData);
                    Stream fileStream = new MemoryStream(lessonIntroFileData);
                    await(Task.Run(() => _s3Service.UploadFile(lessonIntroductionS3Key, fileStream, request.LessonIntroductionMimeType)));

                    s3FileData.S3Key = lessonIntroductionS3Key;
                    s3FileData.FileName = request.LessonIntroductionFileName;
                } else
                {
                    _s3Service.DeleteKey(lessonIntroductionS3Key);
                    s3FileData.S3Key = null;
                    s3FileData.FileName = null;
                    s3FileData.Url = request.Lesson.LessonIntroductionVideoUrl;
                }
            } else
            {
                s3FileData.S3Key = lesson?.LessonIntroductionS3Key;
                s3FileData.FileName = lesson?.LessonIntroductionS3Key;
                s3FileData.Url = lesson?.LessonIntroductionVideoUrl;
            }

            return s3FileData;
        }

    }
}
