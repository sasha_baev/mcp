﻿using SAMii.DTO;
using SAMii.DTO.Students;
using SAMii.Interfaces;
using SAMii.Interfaces.S3;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentRepository;
        private readonly IUserRepository _userRepository;
        private readonly IBookingRepository _bookingRepository;
        private readonly IS3Service _s3Service;

        public StudentService(IStudentRepository studentRepository, IUserRepository userRepository, IBookingRepository bookingRepository, IS3Service s3Service)
        {
            _studentRepository = studentRepository;
            _userRepository = userRepository;
            _bookingRepository = bookingRepository;
            _s3Service = s3Service;
        }

        public StudentProfilePageModel GetStudentProfile(long userId)
        {
            var student = _studentRepository.GetStudent(userId);

            return new StudentProfilePageModel
            {
                Student = student
            };
        }

        public StudentData SaveStudent(StudentData request)
        {
            _userRepository.UpdateUser(request.User);
            return _studentRepository.UpdateStudent(request);
        }

        public StudentDashboardPageModel GetDashboard(long userId)
        {
            var student = _studentRepository.GetStudent(userId);
            var bookings = _bookingRepository.GetBookingsByStudentId(student.Id);

            foreach(var booking in bookings)
            {
                foreach (var resource in booking.LessonSlot.LessonDate.Lesson.LessonResources)
                {
                    if (resource.LessonResourceS3Url != null)
                        resource.LessonResourcePresignedUrl = _s3Service.GetPresignedUrl(resource.LessonResourceS3Url, DateTime.Now.AddMinutes(60));
                }

                if (booking.LessonSlot.LessonDate.Lesson.Teacher.User.AvatarS3Key != null)
                    booking.LessonSlot.LessonDate.Lesson.Teacher.User.AvatarPresignedUrl = _s3Service.GetPresignedUrl(booking.LessonSlot.LessonDate.Lesson.Teacher.User.AvatarS3Key, DateTime.Now.AddMinutes(60));
            }

            return new StudentDashboardPageModel
            {
                Bookings = bookings
            };
        }

        public async Task<bool> UpdateProfileFromLesson(StudentUpdateRequest request)
        {
            var userProfile = _userRepository.GetUserProfile(request.UserId);

            if (request.NewAvatar)
            {
                var avatarS3Key = "Avatar/" + userProfile.Identifier.ToString();
                if (request.AvatarFileData != null)
                {
                    var avatarFileData = Convert.FromBase64String(request.AvatarFileData);
                    Stream fileStream = new MemoryStream(avatarFileData);
                    await(Task.Run(() => _s3Service.UploadFile(avatarS3Key, fileStream, request.AvatarMimeType)));

                    request.AvatarS3Key = avatarS3Key;
                }
                else
                {
                    _s3Service.DeleteKey(avatarS3Key);
                    request.AvatarS3Key = null;
                    request.AvatarFileName = null;
                }
            }
            else
            {
                request.AvatarS3Key = userProfile.AvatarS3Key;
                request.AvatarFileName = userProfile.AvatarFileName;
            }

            _userRepository.UpdateUserDetails(request);
            return true;
        }
    }
}


