﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.Interfaces;
using SAMii.Interfaces.S3;

namespace HS.CMS.Services.S3
{
    public class S3Service : IS3Service
    {
        private Amazon.RegionEndpoint AmazonEndpoint = Amazon.RegionEndpoint.APSoutheast2;
        public S3Service()
        {
        }

        public string BUCKET_NAME = System.Configuration.ConfigurationManager.AppSettings["AWS3.Bucket"];
        public string ACCESS_KEY = System.Configuration.ConfigurationManager.AppSettings["AWS3.AccessKey"];
        public string SECRET_KEY = System.Configuration.ConfigurationManager.AppSettings["AWS3.SecretKey"];

        public void UploadFile(string key, string filePath, string contentType)
        {
            using (AmazonS3Client s3 = new AmazonS3Client(ACCESS_KEY, SECRET_KEY, this.AmazonEndpoint))
            {
                PutObjectRequest por = new PutObjectRequest();
                por.BucketName = BUCKET_NAME;
                por.Key = key;
                por.FilePath = filePath;
                por.ContentType = contentType;

                s3.PutObjectAsync(por);
            }
        }

        public void UploadFile(string key, System.IO.Stream fileStream, string contentType)
        {
            using (AmazonS3Client s3 = new AmazonS3Client(ACCESS_KEY, SECRET_KEY, this.AmazonEndpoint))
            {
                PutObjectRequest por = new PutObjectRequest();
                por.BucketName = BUCKET_NAME;
                por.Key = key;
                por.InputStream = fileStream;
                por.ContentType = contentType;

                s3.PutObjectAsync(por);
            }
        }

        public async Task<Stream> DownloadFile(string key)
        {
            using (AmazonS3Client s3 = new AmazonS3Client(ACCESS_KEY, SECRET_KEY, this.AmazonEndpoint))
            {
                Stream fileStream = new MemoryStream();
                GetObjectRequest request = new GetObjectRequest { BucketName = BUCKET_NAME, Key = key };
                using (GetObjectResponse response = await s3.GetObjectAsync(request))
                {
                    response.ResponseStream.CopyTo(fileStream);
                }
                fileStream.Position = 0;
                return fileStream;
            }
        }

        public void DeleteKey(string key)
        {
            using (AmazonS3Client s3 = new AmazonS3Client(ACCESS_KEY, SECRET_KEY, this.AmazonEndpoint))
            {
                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = BUCKET_NAME,
                    Key = key
                };
                s3.DeleteObjectAsync(deleteObjectRequest);
            }
        }

        public string GetPresignedUrl(string key, DateTime expiryTime)
        {
            string url = null;
            using (AmazonS3Client s3 = new AmazonS3Client(ACCESS_KEY, SECRET_KEY, this.AmazonEndpoint))
            {
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
                request.BucketName = BUCKET_NAME;
                request.Key = key;
                request.Expires = expiryTime;
                url = s3.GetPreSignedURL(request);
            }

            return url;
        }


    }
}
