﻿using SAMii.DTO.Parents;
using SAMii.DTO.Students;
using SAMii.DTO.User;
using SAMii.Interfaces;
using SAMii.Interfaces.S3;
using SAMii.Interfaces.Utilities;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services
{
    public class ParentService : IParentService
    {
        private readonly IParentRepository _parentRepo;
        private readonly IUserRepository _userRepo;
        private readonly IEmailService _emailService;
        private readonly IS3Service _s3Service;
        public ParentService(IParentRepository parentRepo, 
                             IUserRepository userRepo, 
                             IEmailService emailService,
                             IS3Service s3Service)
        {
            _parentRepo = parentRepo;
            _userRepo = userRepo;
            _emailService = emailService;
            _s3Service = s3Service;
        }

        public ParentProfilePageModel GetParentProfile(long userId)
        {
            ParentData parent = _parentRepo.GetParent(userId);

            return new ParentProfilePageModel
            {
                Parent = parent
            };
        }

        public ParentData SaveParent(ParentData request)
        {
            _userRepo.UpdateUser(request.User);
            return _parentRepo.UpdateParent(request);
        }

        public bool InviteStudent(UserInvitationData request)
        {            
            var invitation = _userRepo.CreateUserInvitation(request);
            if (invitation != null)
                _emailService.SendStudentInvitationEmail(invitation);

            return true;
        }

        public ParentDashboardPageModel GetParentDashboard(long userId)
        {
            var pageModel = new ParentDashboardPageModel();
            pageModel.Parent = _parentRepo.GetParentDashboard(userId);
            pageModel.Invitations = _userRepo.GetUserInvitationByFromId(userId);

            // retrieve s3 presigned url
            foreach(var student in pageModel.Parent.Students)
            {
                if (! String.IsNullOrEmpty(student.User.AvatarS3Key))
                    student.User.AvatarPresignedUrl = _s3Service.GetPresignedUrl(student.User.AvatarS3Key, DateTime.Now.AddMinutes(60));
            }

            return pageModel;
        }

        public IList<StudentData> GetStudentList(long userId)
        {
            var parentData = _parentRepo.GetParentDashboard(userId);
            return parentData.Students;
        }
    }
}
