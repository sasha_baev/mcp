﻿using SAMii.DTO;
using SAMii.DTO.Teachers;
using SAMii.Interfaces;
using SAMii.Interfaces.Payments;
using SAMii.Interfaces.S3;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services
{
    public class TeacherService : ITeacherService
    {
        private readonly ITeacherRepository _teacherRepository;
        private readonly IUserRepository _userRepository;
        private readonly IS3Service _s3Service;
        private readonly IInstrumentRepository _instrumentRepository;
        private readonly ILessonRepository _lessonRepository;
        private readonly IBookingRepository _bookingRepository;
        private readonly IXeroService _xeroService;

        public TeacherService(ITeacherRepository provider, IUserRepository userProvider, 
            IS3Service s3Service, IInstrumentRepository instrumentRepository, 
            ILessonRepository lessonRepository, IXeroService xeroService,
            IBookingRepository bookingRepository)
        {
            _teacherRepository = provider;
            _userRepository = userProvider;
            _s3Service = s3Service;
            _instrumentRepository = instrumentRepository;
            _lessonRepository = lessonRepository;
            _xeroService = xeroService;
            _bookingRepository = bookingRepository;
        }

        public TeacherDashboardPageModel GetTeacherDashboard(long userId)
        {
            var teacher = _teacherRepository.GetTeacher(userId);
            var bookings = _bookingRepository.GetBookingsByTeacherId(teacher.Id);
            foreach (var booking in bookings)
            {
                foreach (var resource in booking.LessonSlot.LessonDate.Lesson.LessonResources)
                {
                    if (resource.LessonResourceS3Url != null)
                        resource.LessonResourcePresignedUrl = _s3Service.GetPresignedUrl(resource.LessonResourceS3Url, DateTime.Now.AddMinutes(60));
                }

                if (booking.LessonSlot.LessonDate.Lesson.Teacher.User.AvatarS3Key != null)
                    booking.LessonSlot.LessonDate.Lesson.Teacher.User.AvatarPresignedUrl = _s3Service.GetPresignedUrl(booking.LessonSlot.LessonDate.Lesson.Teacher.User.AvatarS3Key, DateTime.Now.AddMinutes(60));
            }

            return new TeacherDashboardPageModel
            {
                Bookings = bookings
            };
        }

        public TeacherSearchPageData GetTeacherSearchPage(TeacherProfileSearchRequest request)
        {
            var instruments = _instrumentRepository.GetInstruments();
            var search = GetTeacherProfiles(request);

            return new TeacherSearchPageData
            {
                Instruments = instruments,
                TeacherSearch = search
            };
        }

        public TeacherProfileSearchResponse GetTeacherProfiles(TeacherProfileSearchRequest request)
        {
            var result = _teacherRepository.GetTeacherProfiles(request);
            foreach (var res in result.Teachers) {
                if (res.User.AvatarS3Key != null)
                    res.User.AvatarPresignedUrl = _s3Service.GetPresignedUrl(res.User.AvatarS3Key, DateTime.Now.AddMinutes(60));
            }
            // Note: will need to change getTeacherProfiles function when implementing proximity search
            // Set TotalTeachersFound to total records found
            // Set DetectedCity and DetectedState to location detected from search string, if valid
            return new TeacherProfileSearchResponse
            {
                Teachers = result.Teachers,
                TotalTeachersFound = result.TotalCount,
                DetectedCity = null,
                DetectedState = null
            };
        }

        public TeacherProfileViewPageModel GetProfileView(TeacherProfileViewRequest request)
        {
            var teacherData = _teacherRepository.GetTeacher(request.UserId);
            var weeklyLessons = _lessonRepository.GetTeacherLessonByDateRange(teacherData.Id, request.WeekStartDate, request.WeekStartDate.AddDays(7));
            var unavailabilities = _lessonRepository.GetUnavailabilitiesByDateRange(teacherData.Id, request.WeekStartDate, request.WeekStartDate.AddDays(7));

            if (teacherData.User.AvatarS3Key != null)
                teacherData.User.AvatarPresignedUrl = _s3Service.GetPresignedUrl(teacherData.User.AvatarS3Key, DateTime.Now.AddMinutes(60));


            if (teacherData.IntroductionMediaS3Key != null)
                teacherData.IntroductionMediaPresignedUrl = _s3Service.GetPresignedUrl(teacherData.IntroductionMediaS3Key, DateTime.Now.AddMinutes(60));

            if (teacherData.BannerS3Key != null)
                teacherData.BannerPresignedUrl = _s3Service.GetPresignedUrl(teacherData.BannerS3Key, DateTime.Now.AddMinutes(60));

            if (teacherData.Testimonials != null)
            {
                foreach (var testimonial in teacherData.Testimonials)
                {
                    if (!String.IsNullOrEmpty(testimonial.TestimonialS3Key))
                        testimonial.PresignedURL = _s3Service.GetPresignedUrl(testimonial.TestimonialS3Key, DateTime.Now.AddMinutes(60));
                }
            }
            return new TeacherProfileViewPageModel
            {
                TeacherData = teacherData,
                WeeklyLessons = weeklyLessons,
                Unavailabilities = unavailabilities
            };
        }

        public TeacherProfilePageModel GetProfile(long id)
        {
            TeacherProfilePageModel pageModel = new TeacherProfilePageModel();
            pageModel.ProfileData = _teacherRepository.GetTeacher(id);
            pageModel.Instruments = _instrumentRepository.GetInstruments();

            if (pageModel.ProfileData.User.AvatarS3Key != null)
                pageModel.ProfileData.User.AvatarPresignedUrl = _s3Service.GetPresignedUrl(pageModel.ProfileData.User.AvatarS3Key, DateTime.Now.AddMinutes(60));
            

            if (pageModel.ProfileData.IntroductionMediaS3Key != null)
                pageModel.ProfileData.IntroductionMediaPresignedUrl = _s3Service.GetPresignedUrl(pageModel.ProfileData.IntroductionMediaS3Key, DateTime.Now.AddMinutes(60));


            // get education s3 url
            if (pageModel.ProfileData != null && pageModel.ProfileData.Educations != null)
            {
                foreach(var education in pageModel.ProfileData.Educations)
                {
                    if (! string.IsNullOrEmpty(education.AttachmentS3Key))
                        education.PresignedURL = _s3Service.GetPresignedUrl(education.AttachmentS3Key, DateTime.Now.AddMinutes(60));
                }
            }

            // get clearance s3 url
            if (pageModel.ProfileData != null && pageModel.ProfileData.Clearances != null)
            {
                foreach (var clearance in pageModel.ProfileData.Clearances)
                {
                    clearance.PresignedURL = _s3Service.GetPresignedUrl(clearance.ClearanceS3Key, DateTime.Now.AddMinutes(60));
                }
            }

            // get testimonial s3 url 
            if (pageModel.ProfileData != null && pageModel.ProfileData.Testimonials != null)
            {
                foreach (var testimonial in pageModel.ProfileData.Testimonials)
                {
                    testimonial.PresignedURL = _s3Service.GetPresignedUrl(testimonial.TestimonialS3Key, DateTime.Now.AddMinutes(60));
                }
            }
            return pageModel;
        }

        public TeacherProfileSetupPageModel TeacherProfileSetupPage(long userId)
        {
            var user = _userRepository.GetUserProfile(userId);
            var result = _teacherRepository.GetTeacherProfileSetupPage(userId);

            result.Setup.FirstName = user.FirstName;
            result.Setup.LastName = user.LastName;
            result.Setup.DateOfBirth = user.DOB;
            result.Setup.PhoneNumber = user.PhoneNumber;
            result.Setup.TeachingAddress = user.Address;
            result.Setup.UserId = user.Id;

            result.Instruments = _instrumentRepository.GetInstruments();
            result.ProfileStep = user.ProfileStep;

            if (result.Billing != null)
            {
                // GET BANK DETAILS FROM XERO
                this._xeroService.GetBankDetails(result.Billing);
            }

            return result;
        }

        public TeacherProfileSetupResponse TeacherProfileSetup(TeacherProfileSetupRequest request)
        {
            if (request == null)
                throw new Exception("Bad Request");

            var userData = _userRepository.UpdateUserDetails(request);

            // update new instruments
            var newInstruments = request.Instruments.Where(x => x.Id == 0)
                                        .Select(x => new InstrumentRequest
                                        {
                                            Name = x.Name,
                                            UserId = request.UserId
                                        })
                                        .ToList();

            if (newInstruments.Count > 0)
            {
                var newInstrumentData = _instrumentRepository.AddInstruments(newInstruments);
                foreach (var inst in newInstrumentData)
                {
                    var requestInstrument = request.Instruments.First(x => x.Name == inst.Name);
                    requestInstrument.Id = inst.Id;
                }
            }

            // save profile
            var setupTuple = _teacherRepository.TeacherProfileSetup(request);

            return new TeacherProfileSetupResponse 
            {
                UserId = userData.Id,
                FirstName = userData.FirstName,
                LastName = userData.LastName,
                DateOfBirth = userData.DOB,
                PhoneNumber = userData.PhoneNumber,
                TeachingAddress = userData.Address,
                TeachingAddressAlsoResidential = request.TeachingAddressAlsoResidential,
                Instruments = setupTuple.Item1,
                TeacherLocations = setupTuple.Item2
            };
        }

        public bool UpdateTeacherBilling(TeacherBillingData request)
        {
            bool updated = false;
            if (request != null && request.TeacherBillingId > 0)
            {
                this._xeroService.AddorUpdateContact(request);

                //DON'T STORE BANK DETAILS LOCALLY FOR PCI COMPLIANCE
                request.AccountNumber = string.Empty;
                request.BSB = string.Empty;
                request.ABN = string.Empty;

                _teacherRepository.UpdateTeacherBilling(request);
                updated = true;
            }
            return updated;
        }

        public async Task<TeacherProfileUpdateResponse>UpdateProfile(TeacherProfileUpdateRequest request)
        {
            if (request == null)
                // TODO: probably do this
                throw new Exception("Bad Request");

            // update user
            var avatar = await UpdateAvatarS3Key(request);
            request.TeacherProfileData.User.AvatarS3Key = avatar.S3Key;
            request.TeacherProfileData.User.AvatarFileName = avatar.FileName;
            _userRepository.UpdateUser(request.TeacherProfileData.User);

            var introductionMedia = await UpdateIntroductionMediaS3Key(request);
            request.TeacherProfileData.IntroductionMediaS3Key = introductionMedia.S3Key;
            request.TeacherProfileData.IntroductionMediaFileName = introductionMedia.FileName;
            request.TeacherProfileData.IntroductionMediaVideoUrl = introductionMedia.Url;

            var banner = await UpdateBannerS3Key(request);
            request.TeacherProfileData.BannerS3Key = banner.S3Key;
            request.TeacherProfileData.BannerFileName = banner.FileName;

            if (request.TeacherProfileData.Clearances != null)
                request.TeacherProfileData.Clearances = await UpdateClearanceS3Keys(request);

            if (request.TeacherProfileData.Educations != null)
                request.TeacherProfileData.Educations = await UpdateEducationS3Keys(request);

            if (request.TeacherProfileData.Testimonials != null)
                request.TeacherProfileData.Testimonials = await UpdateTestimonialS3Keys(request);

            if (string.IsNullOrWhiteSpace(request.TeacherProfileData.FacebookLink))
                request.TeacherProfileData.FacebookLink = null;

            if (string.IsNullOrWhiteSpace(request.TeacherProfileData.InstagramLink))
                request.TeacherProfileData.InstagramLink = null;

            if (string.IsNullOrWhiteSpace(request.TeacherProfileData.LinkedInLink))
                request.TeacherProfileData.LinkedInLink = null;

            if (string.IsNullOrWhiteSpace(request.TeacherProfileData.YouTubeLink))
                request.TeacherProfileData.YouTubeLink = null;

            // update new instruments
            var newInstruments = request.TeacherProfileData.Instruments.Where(x => x.Id == 0)
                                        .Select(x => new InstrumentRequest
                                        {
                                            Name = x.Name,
                                            UserId = request.TeacherProfileData.User.Id
                                        })
                                        .ToList();

            if (newInstruments.Count > 0)
            {
                var newInstrumentData = _instrumentRepository.AddInstruments(newInstruments);
                foreach (var inst in newInstrumentData)
                {
                    var requestInstrument = request.TeacherProfileData.Instruments.First(x => x.Name == inst.Name);
                    requestInstrument.Id = inst.Id;
                }
            }

            // save profile
            _teacherRepository.UpdateProfile(request);

            // data reload
            return new TeacherProfileUpdateResponse
            {
                BannerS3URL = (banner != null && banner.S3Key != null) ? _s3Service.GetPresignedUrl(banner.S3Key, DateTime.Now.AddMinutes(60)) : null,
                AvatarS3URL = (avatar != null && avatar.S3Key != null) ? _s3Service.GetPresignedUrl(avatar.S3Key, DateTime.Now.AddMinutes(60)) : null,
                IntroductionMediaS3URL = (introductionMedia != null && introductionMedia.S3Key != null) ? _s3Service.GetPresignedUrl(introductionMedia.S3Key, DateTime.Now.AddMinutes(60)) : null,
                BannerFileName = banner?.FileName,
                AvatarFileName = avatar?.FileName,
                IntroductionMediaFileName = introductionMedia?.FileName,
                Educations = this.GetEducations(request.TeacherProfileData.Id),
                Testimonials = this.GetTestimonials(request.TeacherProfileData.Id),
                TeacherLocations = _teacherRepository.GetTeacherLocations(request.TeacherProfileData.Id)
            };
        }

        public TeacherLocationData AddTeacherLocation(TeacherLocationData request)
        {
            return _teacherRepository.AddTeacherLocation(request);
        }

        private async Task<S3FileData> UpdateBannerS3Key(TeacherProfileUpdateRequest request)
        {
            var teacher = _teacherRepository.GetTeacher(request.TeacherProfileData.User.Id);
            S3FileData s3FileData = new S3FileData();

            if (request.NewBanner)
            {
                var bannerS3Key = "Banner/" + teacher.User.Identifier.ToString();
                if (request.BannerFileData != null)
                {
                    var bannerFileData = Convert.FromBase64String(request.BannerFileData);
                    Stream fileStream = new MemoryStream(bannerFileData);
                    await (Task.Run(() => _s3Service.UploadFile(bannerS3Key, fileStream, request.BannerMimeType)));

                    s3FileData.S3Key = bannerS3Key;
                    s3FileData.FileName = request.BannerFileName;
                }
                else
                {
                    _s3Service.DeleteKey(bannerS3Key);
                    s3FileData.S3Key = null;
                    s3FileData.FileName = null;
                }
            }
            else
            {
                s3FileData.S3Key = teacher.BannerS3Key;
                s3FileData.FileName = teacher.BannerFileName;
            }
            return s3FileData;
        }

        private async Task<S3FileData> UpdateAvatarS3Key(TeacherProfileUpdateRequest request)
        {
            var userProfile = _userRepository.GetUserProfile(request.TeacherProfileData.UserId);
            S3FileData s3FileData = new S3FileData();

            if (request.NewAvatar)
            {
                var avatarS3Key = "Avatar/" + userProfile.Identifier.ToString();
                if (request.AvatarFileData != null)
                {
                    var avatarFileData = Convert.FromBase64String(request.AvatarFileData);
                    Stream fileStream = new MemoryStream(avatarFileData);
                    await (Task.Run(() => _s3Service.UploadFile(avatarS3Key, fileStream, request.AvatarMimeType)));

                    s3FileData.S3Key = avatarS3Key;
                    s3FileData.FileName = request.AvatarFileName;
                }
                else
                {
                    _s3Service.DeleteKey(avatarS3Key);
                    s3FileData.S3Key = null;
                    s3FileData.FileName = null;
                }
            }
            else
            {
                s3FileData.S3Key = userProfile.AvatarS3Key;
                s3FileData.FileName = userProfile.AvatarFileName;
            }

            return s3FileData;
        }

        private async Task<S3FileData> UpdateIntroductionMediaS3Key(TeacherProfileUpdateRequest request)
        {
            var teacher = _teacherRepository.GetTeacher(request.TeacherProfileData.User.Id);
            S3FileData s3FileData = new S3FileData();

            if (request.NewIntroductionMedia)
            {
                var introductionMediaS3Key = "IntroductionMedia/" + teacher.User.Identifier.ToString();
                if (request.IntroductionMediaFileData != null)
                {
                    var introductionmediaFileData = Convert.FromBase64String(request.IntroductionMediaFileData);
                    Stream fileStream = new MemoryStream(introductionmediaFileData);
                    await (Task.Run(() => _s3Service.UploadFile(introductionMediaS3Key, fileStream, request.IntroductionMediaMimeType)));

                    s3FileData.S3Key = introductionMediaS3Key;
                    s3FileData.FileName = request.IntroductionMediaFileName;
                }
                else
                {
                    _s3Service.DeleteKey(introductionMediaS3Key);
                    s3FileData.S3Key = null;
                    s3FileData.FileName = null;
                    s3FileData.Url = request.IntroductionMediaVideoUrl;
                }
            }
            else
            {
                s3FileData.S3Key = teacher.IntroductionMediaS3Key;
                s3FileData.FileName = teacher.IntroductionMediaFileName;
                s3FileData.Url = teacher.IntroductionMediaVideoUrl;
            }
            return s3FileData;
        }

        private async Task<IList<ClearanceData>> UpdateClearanceS3Keys(TeacherProfileUpdateRequest request)
        {
            if (request.TeacherProfileData.Clearances != null)
            {
                request.TeacherProfileData.Clearances = _teacherRepository.AddNewClearances(request.TeacherProfileData.Id, request.TeacherProfileData.Clearances);
                var userProfile = _userRepository.GetUserProfile(request.TeacherProfileData.UserId);
                var clearancesToDelete = new List<ClearanceData>();
                foreach (var clearance in request.TeacherProfileData.Clearances)
                {
                    if (clearance.NewClearance)
                    {
                        var S3Key = "Clearance/" + userProfile.Identifier.ToString() + "/" + clearance.GUID.ToString();
                        if (clearance.FileData != null)
                        {
                            var fileData = Convert.FromBase64String(clearance.FileData);
                            Stream fileStream = new MemoryStream(fileData);
                            await (Task.Run(() => _s3Service.UploadFile(S3Key, fileStream, clearance.MimeType)));
                            clearance.ClearanceS3Key = S3Key;
                        }
                        else
                        {
                            clearancesToDelete.Add(clearance);
                            _s3Service.DeleteKey(S3Key);
                        }
                    }
                }

                foreach (var clearance in clearancesToDelete)
                    request.TeacherProfileData.Clearances.Remove(clearance);

                return request.TeacherProfileData.Clearances;
            }
            return null;
        }

        private IList<TestimonialData> GetTestimonials (long teacherProfileId)
        {
            var response = _teacherRepository.GetTestimonials(teacherProfileId);
            // get education s3 url
            if (response != null)
            {
                foreach (var testimonial in response)
                {
                    if (!String.IsNullOrEmpty(testimonial.TestimonialS3Key))
                        testimonial.PresignedURL = _s3Service.GetPresignedUrl(testimonial.TestimonialS3Key, DateTime.Now.AddMinutes(60));
                }
            }
            return response;
        }

        private IList<EducationData> GetEducations(long teacherProfileId)
        {
            var response = _teacherRepository.GetEducations(teacherProfileId);
            // get education s3 url
            if (response != null)
            {
                foreach (var education in response)
                {
                    if (! String.IsNullOrEmpty(education.AttachmentS3Key)) 
                        education.PresignedURL = _s3Service.GetPresignedUrl(education.AttachmentS3Key, DateTime.Now.AddMinutes(60));
                }
            }

            return response;
        }

        private async Task<IList<EducationData>> UpdateEducationS3Keys(TeacherProfileUpdateRequest request)
        {
            var userProfile = _userRepository.GetUserProfile(request.TeacherProfileData.UserId);
            var educationRecords = _teacherRepository.GetEducations(request.TeacherProfileData.Id);
            foreach (var education in request.TeacherProfileData.Educations)
            {
                if (education.Id == 0)
                {
                    education.Identifier = Guid.NewGuid();
                }

                if (education.NewAttachment)
                {
                    // only updated education records do not have guid at this point
                    // since all new record are assigned new guid previously
                    if (education.Identifier == Guid.Empty)
                        education.Identifier = educationRecords.First(x => x.Id == education.Id).Identifier;

                    var S3Key = "Education/" + userProfile.Identifier.ToString() + "/" + education.Identifier.ToString();
                    if (education.FileData != null)
                    {
                        var fileData = Convert.FromBase64String(education.FileData);
                        Stream fileStream = new MemoryStream(fileData);
                        await (Task.Run(() => _s3Service.UploadFile(S3Key, fileStream, education.MimeType)));
                        education.AttachmentS3Key = S3Key;
                    }
                }
            }

            // remove all attachments of education that is going to be deleted
            var updatedRequest = request.TeacherProfileData.Educations.Where(x => x.Id != 0);
            var deletedRecords = educationRecords.Where(x => ! updatedRequest.Any(y => y.Id == x.Id));
            foreach(var record in deletedRecords)
            {
                if (!String.IsNullOrEmpty(record.AttachmentS3Key))
                    _s3Service.DeleteKey(record.AttachmentS3Key);
            }

            return request.TeacherProfileData.Educations;
        }


        private async Task<IList<TestimonialData>> UpdateTestimonialS3Keys(TeacherProfileUpdateRequest request)
        {
            var userProfile = _userRepository.GetUserProfile(request.TeacherProfileData.UserId);
            var testimonialRecords = _teacherRepository.GetTestimonials(request.TeacherProfileData.Id);

            foreach (var testimonial in request.TeacherProfileData.Testimonials)
            {
                if (testimonial.NewTestimonial)
                {
                    // only updated testimonial records do not have guid at this point
                    // since all new record are assigned new guid previously
                    if (testimonial.Id == 0)
                    {
                        testimonial.Identifier = Guid.NewGuid();
                    } else
                    {
                        if (testimonial.Identifier == Guid.Empty || testimonial.Identifier == null)
                            testimonial.Identifier = testimonialRecords.First(x => x.Id == testimonial.Id).Identifier;
                    }

                    var S3Key = "Testimonial/" + userProfile.Identifier.ToString() + "/" + testimonial.Identifier.ToString();
                    if (testimonial.FileData != null)
                    {
                        var fileData = Convert.FromBase64String(testimonial.FileData);
                        Stream fileStream = new MemoryStream(fileData);
                        await (Task.Run(() => _s3Service.UploadFile(S3Key, fileStream, testimonial.MimeType)));
                        testimonial.TestimonialS3Key = S3Key;
                    }
                }

                // remove all testimonial image that is going to be deleted
                var updatedRequest = request.TeacherProfileData.Testimonials.Where(x => x.Id != 0);
                var deletedRecords = testimonialRecords.Where(x => !updatedRequest.Any(y => y.Id == x.Id));
                foreach (var record in deletedRecords)
                {
                    if (!String.IsNullOrEmpty(record.TestimonialS3Key))
                        _s3Service.DeleteKey(record.TestimonialS3Key);
                }
            }
            return request.TeacherProfileData.Testimonials;
        }
    }
}


