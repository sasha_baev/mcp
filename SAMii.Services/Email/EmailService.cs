﻿using Ganemo.Framework.Util.Common;
using Ganemo.Framework.Util.Email;
using Ganemo.Framework.Util.Log4net;
using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.User;
using SAMii.Interfaces.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services.Email
{
    public enum EmailType
    {
        Default = 0,
        Welcome = 1,
        ResetPassword = 2,
        RegisterNewUser = 3,
        BookingTopup = 4,
        StudentInvitation = 5,
        BookingCancellation = 6
    }

    public class EmailService : IEmailService
    {
        readonly IEmailer _emailer;
        readonly ILogger _logger;

        public EmailService(ILogger logger, IEmailer emailer)
        {
            _logger = logger;
            _emailer = emailer;
        }

        public string WEBSITE_HOST = System.Configuration.ConfigurationManager.AppSettings["WebsiteHost"];
        public string TEMPLATE_LOC = System.Configuration.ConfigurationManager.AppSettings["EmailTemplates"];

        #region Urls
        private string BuildUrl(string path)
        {
            return string.Concat(WEBSITE_HOST.TrimEnd("/".ToCharArray()), "/", path);
        }
        public string ActivateUserUrl(string token)
        {
            return BuildUrl(string.Format("activate/{0}", token));
        }
        public string ResetPasswordUrl(string token)
        {
            return BuildUrl(string.Format("reset/{0}", token));
        }
        public string SignupInvitationUrl(string token)
        {
            return BuildUrl(string.Format("signup/{0}", token));
        }

        private string UrlUserLogin()
        {
            return WEBSITE_HOST;
        }

        #endregion

        #region Private
        private string getEmailFilePath(EmailType type)
        {
            string templateName = this.getTemplateNameByType(type);

            string filePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            filePath = Path.Combine(filePath, "Templates", templateName);

            if (File.Exists(filePath))
                return filePath;

            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));

            if (!string.IsNullOrWhiteSpace(TEMPLATE_LOC))
                filePath = Path.Combine(path, TEMPLATE_LOC, templateName);
            else
                filePath = Path.Combine(path, "Email", "Templates", templateName);

            if (File.Exists(filePath))
                return filePath;

            return null;
        }

        private string getTemplateNameByType(EmailType type)
        {
            string name = "";
            switch (type)
            {
                case EmailType.ResetPassword:
                    name = "ResetPassword.html";
                    break;
                case EmailType.RegisterNewUser:
                    name = "NewUserActivation.html";
                    break;
                case EmailType.BookingTopup:
                    name = "BookingTopup";
                    break;
                case EmailType.StudentInvitation:
                    name = "StudentInvitation.html";
                    break;
                case EmailType.BookingCancellation:
                    name = "BookingCancellation.html";
                    break;
            };
            return name;
        }
        private void SendEmailAsync(MailAddress to, string subject, string messageBody, byte[] attachment = null, string attachmentName = null, EmailType type = EmailType.Default)
        {
            SendEmailTemplateAsync(to, null, subject, messageBody, attachment, attachmentName, type);
        }

        private void SendEmailTemplateAsync(MailAddress to, MailAddress replyTo, string subject, string messageBody, byte[] attachment, string attachmentName, EmailType type)
        {
            string htmlBody = getMessageBody(messageBody, type);

            // Asynchronously run this method as it blocks while sending email
            Task.Run(() =>
            {
                try
                {
                    _emailer.SendEmail(to, replyTo, subject, htmlBody, attachment, attachmentName);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "SendEmailErrorAsync");
                }
            });
        }
        private string getMessageBody(string messageBody, EmailType type)
        {
            string htmlBody = "";

            try
            {
                if (type == EmailType.Default)
                {
                    string filePath = getEmailFilePath(type);

                    if (File.Exists(filePath))
                    {
                        string str = File.ReadAllText(filePath);
                        str = str.Replace("{{emailtext}}", messageBody);
                        htmlBody = str;
                    }
                }
                else if (!string.IsNullOrWhiteSpace(messageBody))
                    htmlBody = messageBody;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, string.Format("SendEmailError"));
            }

            return htmlBody;
        }
        #endregion

        public string SendResetPasswordEmail(UserData user)
        {
            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                string subject = "Reset Password to SAMii";
                string body = null;
                EmailType type = EmailType.ResetPassword;
                string filePath = getEmailFilePath(type);

                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    string va = File.ReadAllText(filePath);

                    var subs = new Dictionary<string, string> {
                            { "{{ResetUrl}}", ResetPasswordUrl(GUIDHelper.GetTokenStringRepresentation(user.ResetToken.Value)) },

                    };

                    va = subs.Aggregate(va, (result, s) => result.Replace(s.Key, s.Value));

                    body = va;
                }

                MailAddress addr = new MailAddress(user.Email, user.FirstName + user.LastName);
                this.SendEmailAsync(addr, subject, body, null, null, type);

                return GUIDHelper.GetTokenStringRepresentation(user.ResetToken.Value);
            }

            return String.Empty;
        }

        public string SendRegisterNewUserEmail(UserData user)
        {
            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                string subject = "Welcome to SAMii";
                string body = null;
                EmailType type = EmailType.RegisterNewUser;
                string filePath = getEmailFilePath(type);

                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    string va = File.ReadAllText(filePath);

                    var subs = new Dictionary<string, string> {
                            { "{{ActivateUrl}}", ActivateUserUrl(GUIDHelper.GetTokenStringRepresentation(user.ActivationToken.Value)) },

                    };

                    va = subs.Aggregate(va, (result, s) => result.Replace(s.Key, s.Value));

                    body = va;
                }

                MailAddress addr = new MailAddress(user.Email, user.FirstName + user.LastName);
                this.SendEmailAsync(addr, subject, body, null, null, type);

                return GUIDHelper.GetTokenStringRepresentation(user.ActivationToken.Value);
            }

            return String.Empty;
        }

        public void SendBookingTopupEmail(UserData user)
        {
            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                string subject = "Lesson Booking Topup";
                string body = null;
                EmailType type = EmailType.BookingTopup;
                string filePath = getEmailFilePath(type);

                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    string va = File.ReadAllText(filePath);

                    var subs = new Dictionary<string, string> {
                            { "{{FullName}}", user.FullName },

                    };

                    va = subs.Aggregate(va, (result, s) => result.Replace(s.Key, s.Value));

                    body = va;
                }

                MailAddress addr = new MailAddress(user.Email, user.FirstName + user.LastName);
                this.SendEmailAsync(addr, subject, body, null, null, type);
            }
        }

        public void SendStudentInvitationEmail(UserInvitationData invitation)
        {
            if (invitation != null &&  ! String.IsNullOrEmpty(invitation.ToEmail))
            {
                string subject = "Welcome to SAMii";
                string body = null;
                EmailType type = EmailType.StudentInvitation;
                string filePath = getEmailFilePath(type);

                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    string va = File.ReadAllText(filePath);

                    var subs = new Dictionary<string, string> {
                            { "{{SignupURL}}", SignupInvitationUrl(GUIDHelper.GetTokenStringRepresentation(invitation.Token)) }
                    };

                    va = subs.Aggregate(va, (result, s) => result.Replace(s.Key, s.Value));

                    body = va;
                }

                MailAddress addr = new MailAddress(invitation.ToEmail, String.Empty);
                this.SendEmailAsync(addr, subject, body, null, null, type);
            }
        }

        public void SendBookingCancellationEmails(BookingCancellationEmailData data)
        {
            string subject = "Your lesson has been cancelled";
            string teacherBody = null;
            string studentBody = null;
            EmailType type = EmailType.BookingCancellation;
            string filePath = getEmailFilePath(type);

            if (!string.IsNullOrWhiteSpace(filePath))
            {
                string teacherva = File.ReadAllText(filePath);
                string studentva = File.ReadAllText(filePath);

                var lessonString = "";
                if (data.CancelType == Enum.BookingCancelType.Single)
                {
                    lessonString = "Your lesson on the " + data.CancellationDate.ToString("dd MMMM, yyyy");
                } else
                {
                    lessonString = "Your lessons from the " + data.CancellationDate.ToString("dd MMMM, yyyy") + " onwards";
                }


                var teacherCancelString = lessonString + " with " + data.Student.User.FirstName + " " + data.Student.User.LastName + " to teach " + data.Lesson.Instrument.Name + " has been cancelled.";
                var studentCancelString = lessonString + " with " + data.Teacher.User.FirstName + " " + data.Teacher.User.LastName + " for " + data.Lesson.Instrument.Name + " has been cancelled.";

                var teacherSubs = new Dictionary<string, string> {
                            { "{{cancelString}}", teacherCancelString }
                    };

                var studentSubs = new Dictionary<string, string> {
                            { "{{cancelString}}", studentCancelString }
                    };

                teacherva = teacherSubs.Aggregate(teacherva, (result, s) => result.Replace(s.Key, s.Value));
                teacherBody = teacherva;

                studentva = studentSubs.Aggregate(studentva, (result, s) => result.Replace(s.Key, s.Value));
                studentBody = studentva;
            }

            MailAddress teacherAddr = new MailAddress(data.Teacher.User.Email, String.Empty);
            MailAddress studentAddr = new MailAddress(data.Student.User.Email, String.Empty);

            this.SendEmailAsync(teacherAddr, subject, teacherBody, null, null, type);
            this.SendEmailAsync(studentAddr, subject, studentBody, null, null, type);
        }
        public void SendErrorNotification(string email, string context, string exception)
        {
            _emailer.SendEmail(email, context, exception);
        }
    }
}
