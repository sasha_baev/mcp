﻿using Ganemo.Framework.Util.Email;
using Ganemo.Framework.Util.Log4net;
using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.Lessons;
using SAMii.DTO.Payments;
using SAMii.DTO.Teachers;
using SAMii.Interfaces.Payments;
using SAMii.Interfaces.Schedule;
using SAMii.Interfaces.Utilities;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services.Schedule
{
    public class ScheduleService : IScheduleService
    {
        readonly ILogger _logger;
        readonly IBookingRepository _bookingRepository;
        readonly IUserRepository _userRepository;
        readonly ILessonRepository _lessonRepository;
        readonly ITeacherRepository _teacherRepository;
        readonly IPaymentService _paymentService;
        readonly IEmailService _emailService;
        readonly IXeroService _xeroService;
        readonly IEmailer _emailer;

        public ScheduleService(ILogger logger,
            ILessonRepository lessonRepo,
            IUserRepository userRepository,
            ITeacherRepository teacherRepository,
            IPaymentService paymentService,
            IEmailService emailService,
            IEmailer emailer,
            IXeroService xeroService,
            IBookingRepository bookingRepository)
        {
            _logger = logger;
            _bookingRepository = bookingRepository;
            _lessonRepository = lessonRepo;
            _userRepository = userRepository;
            _teacherRepository = teacherRepository;
            _paymentService = paymentService;
            _emailService = emailService;
            _emailer = emailer;
            _xeroService = xeroService;
        }

        public int StudentMarkAttendace(int markAttendanceDays)
        {
            var currentDate = DateTime.UtcNow.AddDays(markAttendanceDays);

            int attendanceCount = 0;
            IList<LessonSlotData> slots = this._lessonRepository.GetLessonSlotsByDateRange(currentDate, currentDate);
            List<LessonSlotData> todaySlots = new List<LessonSlotData>();

            //STEP 1: ToDo: Filter slots for unavailability

            //STEP 2: Filter slots for the Day
            foreach (LessonSlotData lessonSlotData in slots)
            {
                if (lessonSlotData.LessonDate.Day == currentDate.DayOfWeek)
                    todaySlots.Add(lessonSlotData);
            }

            //STEP 3: Get Booking for the slots
            IList<long> slotIds = todaySlots.Select(x => x.Id).ToList();
            IList<BookingData> bookings = this._bookingRepository.GetBookingsByLessonSlots(slotIds);

            if (bookings != null)
            {
                List<BookingAttendanceData> attendanceList = new List<BookingAttendanceData>();
                foreach (BookingData booking in bookings)
                {
                    //STEP 4: Mark Attendance If not cancelled
                    //Cancelled / Absent for that day check BookingCancellation
                    if (this.CheckBookingCancelled(booking))
                    {
                        var model = new BookingAttendanceData()
                        {
                            BookingId = booking.Id,
                            AttendedDate = currentDate
                        };
                        attendanceList.Add(model);
                        attendanceCount++;
                    }
                }

                //STEP 5: Save Attendance to database
                this._bookingRepository.SaveStudentAttendance(attendanceList);
            }
            return attendanceCount;
        }

        private bool CheckBookingCancelled(BookingData booking)
        {
            bool cancelled = false;

            if (booking.Cancelled)
            {
                if (!booking.CancelledEndDate.HasValue)
                    cancelled = true;
                else if (DateTime.Compare(DateTime.UtcNow, booking.CancelledEndDate.Value) > 0)
                    cancelled = true;
            }
            else
            {
                var records = this._bookingRepository.GetBookingCancellationDatas(booking.Id);

                if(records != null)
                {
                    foreach(BookingCancellationData date in records)
                    {
                        if(DateTime.Compare(date.CancelledDate.Date, DateTime.UtcNow.Date) == 0)
                        {
                            cancelled = true;
                            break;
                        }
                    }
                }
            }
            return cancelled;
        }

        public Tuple<int, int> ProcessBookingRecurringPayments()
        {
            int notifyCount = 0;
            int paymentsCount = 0;

            IList<BookingData> bookings = this._bookingRepository.GetAllActiveBookings();
            List<BookingData> paymentFailedList = new List<BookingData>();

            foreach (BookingData booking in bookings)
            {
                if (booking.PaymentType == Enum.BookingPaymentType.Fixed &&
                    !booking.Cancelled && booking.InCreditCount <= 2)
                {
                    //Send notification for booking with less than or equal to 2 lessons as per the requirement
                    _emailService.SendBookingTopupEmail(booking.Student.User);
                    notifyCount++;
                }
                else if (booking.PaymentType == Enum.BookingPaymentType.Reoccurring && !booking.Cancelled)
                {
                    Tuple<bool,bool> result = this._paymentService.BookingRecurringPayments(booking);

                    //If not in credit
                    if (!result.Item1)
                    {
                        if(result.Item2)
                            paymentsCount++;
                        else
                            paymentFailedList.Add(booking);
                    }
                }
            }
            var ids = bookings != null && bookings.Count > 0 ? string.Join(",", bookings.Select(x => x.Id.ToString()).ToList()) : string.Empty;
            _logger.Info("Process-BookingIDs: " + ids);

            if (paymentFailedList != null && paymentFailedList.Count > 0)
            {
                var str = string.Join(",", paymentFailedList.Select(x => x.Id.ToString()).ToList());
                _logger.Info("ProcessPaymentFailed-IDs: " + str);
            }

            return new Tuple<int, int>(notifyCount, paymentsCount);
        }

        public void SendErrorNotification(string email, string context, string exception)
        {
            _emailer.SendEmail(email, context, exception);
        }

        public void ProcessTeacherPayments()
        {
            //Get list of teachers to be paid fornightly or monthly
            List<TeacherBillingData> teachers = this.getPayoutTeachers();
            List<long> payoutTeacherIds = teachers.Select(x => x.TeacherBillingId).ToList();

            //STEP 1 - Get All booking attendance which are not paid
            IList<BookingAttendanceData> attendanceList = this._bookingRepository.GetUnpaidBookingAttendance();

            //STEP2 - build Line items for invoice - lesson quantity
            Dictionary<long, InvoiveLineItem> dict = new Dictionary<long, InvoiveLineItem>();
            Dictionary<long, TeacherPayoutData> teacherDict = new Dictionary<long, TeacherPayoutData>();

            foreach(BookingAttendanceData data in attendanceList)
            {
                if(!data.TeacherPaid)
                {
                    var lesson = data.Booking.LessonSlot.LessonDate.Lesson;

                    if (payoutTeacherIds.Contains(lesson.TeacherId))
                    {
                        //Calculate Lesson Quantity by attendance
                        if (dict.ContainsKey(lesson.Id))
                        {
                            dict[lesson.Id].Quantity += 1;
                        }
                        else
                        {
                            InvoiveLineItem item = new InvoiveLineItem()
                            {
                                Description = lesson.Title,
                                Quantity = 1,
                                UnitAmount = _paymentService.GetTeacherLessonPrice(lesson.Price, lesson.IsCostAbsorb),
                                TeacherId = lesson.TeacherId
                            };
                            dict.Add(lesson.Id, item);
                        }

                        //Add booking attendance to teacher
                        if(!teacherDict.ContainsKey(lesson.TeacherId))
                        {
                            TeacherPayoutData payoutData = new TeacherPayoutData()
                            {
                                TeacherId = lesson.TeacherId,
                                InvoiceRef = "DEMO - " + Ganemo.Framework.Util.Common.Random.GetRandomString(12),
                                Bookings = new List<BookingAttendanceData>(),
                                XeroContactId = this.getTeacherXeroContactId(teachers, lesson.TeacherId)
                            };
                            teacherDict.Add(lesson.TeacherId, payoutData);
                        }
                        teacherDict[lesson.TeacherId].Bookings.Add(data);
                    }
                }
            }


            //STEP 3 - Group the lesson items by TeacherID
            List<InvoiveLineItem> test = dict.Values.ToList<InvoiveLineItem>();
            var teacherLessonBookingGroup = dict.Values.ToList<InvoiveLineItem>().GroupBy(x => x.TeacherId);

            foreach(var group in teacherLessonBookingGroup)
            {
                if (!teacherDict.ContainsKey(group.Key))
                    throw new Exception("Error in payout calculation");

                var teacherPayOut = teacherDict[group.Key];
                teacherPayOut.LineItems = group.ToList();
            }


            //STEP 4 - Create Bill in Xero and Update DB
            _logger.Info("Teacher PayoutCount:" + teacherDict.Count);
            this.TeacherPaymentsCreateBill(teacherDict.Values.ToList<TeacherPayoutData>());
        }

        private List<TeacherBillingData> getPayoutTeachers()
        {
            //Get list of teachers to be paid fornightly or monthly
            IList<TeacherBillingData> teacher = this._teacherRepository.GetTeacherBillings();
            List<TeacherBillingData> list = new List<TeacherBillingData>();


            //Add monthly billing teachers
            DateTime date = DateTime.UtcNow;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            if(DateTime.Compare(date, lastDayOfMonth) > 0)
            {
                var records = teacher.Where(x => !x.IsWeeklyPayment);
                if (records != null)
                    list.AddRange(records);
            }

            //Add fortnight billing teachers
            DateTime? fortnightDate = this._teacherRepository.GetLastFortnightDate();
           
            bool add = false;

            if (!fortnightDate.HasValue)
                add = true;
            else
            {
                var billingDate = new DateTime(fortnightDate.Value.Year, fortnightDate.Value.Month, fortnightDate.Value.Day);
                billingDate = billingDate.AddDays(7);
                if (DateTime.Compare(date, billingDate) > 0)
                    add = true;
            }

            if(add)
            {
                this._teacherRepository.UpdateFortnightDate();
                var records = teacher.Where(x => x.IsWeeklyPayment);
                if (records != null)
                    list.AddRange(records);
            }

            return list;
        }

        private string getTeacherXeroContactId(List<TeacherBillingData> list, long teacherId)
        {
            return list.First(x => x.TeacherBillingId == teacherId).XeroContactId;
        }

        private void TeacherPaymentsCreateBill(List<TeacherPayoutData> list)
        {
            var total = 0M;
            foreach (TeacherPayoutData data in list)
            {
                try
                {
                    //Add teacher bill to pay in XERO
                    var created = _xeroService.CreateBill(data);

                    if (created)
                    {
                        //Insert TeacherPayout Record after bill created
                        var teacherPayoutId = _teacherRepository.AddTeacherPayout(data);

                        //Mark Bookings as paid
                        this._bookingRepository.MarkPaidBookingAttendance(data.Bookings, teacherPayoutId);

                        total += data.AmountPaid;
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "PayoutError:" + data.TeacherId);
                }
            }
            _logger.Info("Teachers-PayoutTotal:" + total);
        }
    }
}
