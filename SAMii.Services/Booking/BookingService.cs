﻿using Ganemo.Framework.Util.Log4net;
using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.Teachers;
using SAMii.Interfaces;
using SAMii.Interfaces.Payments;
using SAMii.Interfaces.S3;
using SAMii.Interfaces.Utilities;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Services
{
    public class BookingService : IBookingService
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly ITeacherRepository _teacherRepository;
        private readonly ILessonRepository _lessonRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IPaymentService _paymentService;
        private readonly IEmailService _emailService;
        private IS3Service _s3Service;
        readonly ILogger _logger;

        public BookingService(IBookingRepository bookingRepository, ITeacherRepository teacherRepository,
            IPaymentService paymentService,
            ILessonRepository lessonRepository, IStudentRepository studentRepository, IS3Service s3Service, ILogger logger,
            IEmailService emailService)
        {
            _bookingRepository = bookingRepository;
            _teacherRepository = teacherRepository;
            _lessonRepository = lessonRepository;
            _studentRepository = studentRepository;
            _s3Service = s3Service;
            _paymentService = paymentService;
            _logger = logger;
            _emailService = emailService;
        }

        public NewBookingPageData GetNewBooking(NewBookingRequest request)
        {
            var teacher = _teacherRepository.GetTeacher(request.TeacherUserId);
            var student = _studentRepository.GetStudent(request.StudentUserId);

            var lessons = _lessonRepository.GetAllPublished(request.TeacherUserId);
            var unavailabilities = _lessonRepository.GetUnavailabilitiesByTeacherId(teacher.Id);

            if (student.User.AvatarS3Key != null)
                student.User.AvatarPresignedUrl = _s3Service.GetPresignedUrl(student.User.AvatarS3Key, DateTime.Now.AddMinutes(60));

            var lessonsByInstrument = new List<InstrumentLessons>();
            foreach(var instrument in teacher.Instruments)
            {
                var lessonList = lessons.Where(x => x.InstrumentId == instrument.Id)
                                        .OrderBy(x => x.StartDate)
                                        .ToList();
                if (lessonList.Count > 0)
                    lessonsByInstrument.Add(new InstrumentLessons
                    {
                        Instrument = instrument,
                        Lessons = lessonList
                    });
            }

            return new NewBookingPageData
            {
                Teacher = teacher,
                Student = student,
                LessonsByInstrument = lessonsByInstrument.OrderBy(x => x.Instrument.Name)
                                                         .ToList(),
                LessonFeePercentage = AppConstants.LessonFeePercentage,
                Unavailabilities = unavailabilities
            };
        }

        public BookingCreateResponse CreateBooking(BookingRequest request)
        {
            bool created = false;
            var lesson = _lessonRepository.GetBySlotId(request.LessonSlotId);
            var student = _studentRepository.GetStudent(request.StudentUserId);

            long bookingId = _bookingRepository.CreateBooking(request, lesson, student);
            if (bookingId == 0)
                return new BookingCreateResponse
                {
                    Success = false,
                    ErrorMessage = "Lesson is full."
                };

            if (bookingId > 0) //if booked make payment
            {
                created = _paymentService.LessonBookingPayment(lesson, student, request, bookingId);
            }

            if (!created)
            {
                _bookingRepository.DeleteBooking(bookingId);
                _logger.Info("Booking with Id of " + bookingId + " has been deleted, due to payment failure.");
                return new BookingCreateResponse
                {
                    Success = false,
                    ErrorMessage = "Payment could not be processed."
                };
            }

            return new BookingCreateResponse
            {
                Success = true,
            };
        }

        public BookingCancelResponse CancelBooking(BookingCancelRequest request)
        {
            var result = _bookingRepository.CancelBooking(request);

            if (result)
            {
                var booking = _bookingRepository.GetBookingById(request.BookingId);

                if(request.CancelType == Enum.BookingCancelType.All)
                {
                    var refunded = this._paymentService.ProcessRefunds(request);

                    if(!refunded)
                    {
                        string msg = string.Format("Cancellation refund payment failed for Booking id {0}, refer to system logs", request.BookingId);
                        this._emailService.SendErrorNotification("hari@ganemogroup.com", "Refund payment failed", msg);
                    }
                }

                _emailService.SendBookingCancellationEmails(new BookingCancellationEmailData
                {
                    Teacher = booking.LessonSlot.LessonDate.Lesson.Teacher,
                    Student = booking.Student,
                    CancellationDate = request.CancelDate,
                    Lesson = booking.LessonSlot.LessonDate.Lesson,
                    CancelType = request.CancelType
                });

                return new BookingCancelResponse
                {
                    Success = true
                };
            } else
            {
                return new BookingCancelResponse
                {
                    Success = false
                };
            }
        }
    }
}


