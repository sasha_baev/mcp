﻿using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.Lessons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Interface
{
    public interface ILessonRepository
    {
        IList<LessonData> GetAll(long userId);
        IList<LessonData> GetAllPublished(long userId);
        LessonData GetById(long id);
        LessonData GetBySlotId(long lessonSlotId);
        LessonData AddLesson(LessonData request);
        IList<LessonData> GetTeacherLessonByDateRange(long teacherId, DateTime startDate, DateTime endDate);
        void DeleteLesson(long lessonId);
        IList<LessonResourceData> UpdateLessonResources(LessonResourceUpdateRequest request);
        IList<LessonSlotData> GetLessonSlotsByDateRange(DateTime startDate, DateTime endDate);
        IList<UnavailabilityData> GetUnavailabilitiesByTeacherId(long id);
        UnavailabilityData GetUnavailabilityById(long id);
        IList<UnavailabilityData> GetUnavailabilitiesByDateRange(long teacherId, DateTime startDate, DateTime endDate);
        Tuple<UnavailabilityData, IList<BookingCancelRequest>>  SaveUnavailability(UnavailabilityData request);
    }
}
