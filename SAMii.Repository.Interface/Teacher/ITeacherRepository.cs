﻿using SAMii.DTO;
using SAMii.DTO.Teachers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Interface
{
    public interface ITeacherRepository
    {
        void InitialiseTeacher(UserData user);
        TeacherSearchResponse GetTeacherProfiles(TeacherProfileSearchRequest request);
        TeacherData GetTeacher(long userId);
        bool UpdateProfile(TeacherProfileUpdateRequest request);
        TeacherProfileSetupPageModel GetTeacherProfileSetupPage(long userId);
        Tuple<IList<InstrumentData>, IList<TeacherLocationData>> TeacherProfileSetup(TeacherProfileSetupRequest request);
        void UpdateTeacherBilling(TeacherBillingData request);
        IList<ClearanceData> AddNewClearances(long teacherProfileId, IList<ClearanceData> request);
        IList<EducationData> GetEducations(long teacherProfileId);
        IList<TestimonialData> GetTestimonials(long teacherProfileId);
        IList<TeacherLocationData> GetTeacherLocations(long teacherId);
        TeacherLocationData AddTeacherLocation(TeacherLocationData request);
        TeacherBillingData GetTeacherBilling(long teacherId);
        TeacherData GetTeacherData(long teacherId);
        long AddTeacherPayout(TeacherPayoutData data);
        DateTime? GetLastFortnightDate();
        IList<TeacherBillingData> GetTeacherBillings();
        void UpdateFortnightDate();
    }
}
