﻿using SAMii.DTO.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Interface
{
    public interface IStudentRepository
    {
        void InitialiseStudent(long userId, long? parentId = null);
        StudentData GetStudent(long userId);
        StudentData UpdateStudent(StudentData request);
    }
}
