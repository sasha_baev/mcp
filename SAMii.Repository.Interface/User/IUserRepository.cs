﻿using SAMii.DTO;
using SAMii.DTO.Auth;
using SAMii.DTO.Students;
using SAMii.DTO.Teachers;
using SAMii.DTO.User;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Interface
{
    public interface IUserRepository
    {
        UserData SignOn(string signOnString);
        UserData GetByEmail(string email);
        void Login(long id, string ipAddress);
        UserData GetUserProfile(long id);
        UserData CreateUser(string email, string hashPassword, bool isInvited = false);
        ActivateUserResponse Activate(Guid guid);
        UserData UpdateUserRole(UserRoleUpdateRequest request);
        UserData ResetPassword(string email);
        UserData GetUserByResetToken(Guid? token);
        void UpdateUser(UserData request);
        UserData UpdateUserDetails(TeacherProfileSetupRequest request);
        UserData UpdateUserDetails(StudentUpdateRequest request);
        void ChangePassword(long id, string hashPassword);
        string GetToken(long id);
        void UpdateUserBillingStatus(SubscriptionStatus status, long userId);
        UserInvitationData CreateUserInvitation(UserInvitationData request);
        UserInvitationData GetUserInvitationByToken(Guid? guid);
        IList<UserInvitationData> GetUserInvitationByFromId(long userId);
        UserInvitationData UpdateUserInvitation(Guid? invitationToken);
    }
}
