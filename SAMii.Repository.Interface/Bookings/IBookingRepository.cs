﻿using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Interface
{
    public interface IBookingRepository
    {
        long CreateBooking(BookingRequest request, LessonData lesson, StudentData student);
        void DeleteBooking(long bookingId);
        bool AddBookingPayment(BookingPaymentData data);
        bool AddBookingRefundPayment(BookingRefundData data);
        BookingData GetBookingById(long bookingId);
        IList<BookingData> GetBookingsByStudentId(long studentId);
        IList<BookingData> GetBookingsByTeacherId(long teacherId);
        IList<BookingData> GetBookingsByLessonSlots(IList<long> slotIds);
        void SaveStudentAttendance(IList<BookingAttendanceData> list);
        IList<BookingData> GetAllActiveBookings();
        bool CancelBooking(BookingCancelRequest request);
        void MarkPaidBookingAttendance(List<BookingAttendanceData> list, long TeacherPayoutId);
        IList<BookingAttendanceData> GetUnpaidBookingAttendance();
        IList<BookingCancellationData> GetBookingCancellationDatas(long bookingId);
        BookingData GetBookingPaymentsByBkId(long bookingId);
    }
}
