﻿using SAMii.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Interface
{
    public interface IInstrumentRepository
    {
        IList<InstrumentData> GetInstruments();
        IList<InstrumentData> AddInstruments(IList<InstrumentRequest> request);
    }
}
