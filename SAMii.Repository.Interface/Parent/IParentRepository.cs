﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.DTO.Parents;

namespace SAMii.Repository.Interface
{
    public interface IParentRepository
    {
        ParentData GetParent(long userId);
        ParentData GetParentDashboard(long userId);
        ParentData UpdateParent(ParentData request);
        void InitialiseParent(long id);
    }
}
