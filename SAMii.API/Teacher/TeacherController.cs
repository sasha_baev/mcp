﻿using SAMii.DTO;
using SAMii.DTO.Teachers;
using SAMii.Interfaces;
using SAMii.Interfaces.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SAMii.API
{
    [RoutePrefix("teacher")]
    public class TeacherController : BaseController
    {
        private readonly ITeacherService _service;
        private readonly IUserAccessService _userAccessService;

        public TeacherController(ITeacherService service, IUserAccessService userAccessService)
        {
            _service = service;
            _userAccessService = userAccessService;
        }

        [Route("{id}/dashboard")]
        public TeacherDashboardPageModel GetDashboard(long id)
        {
            return _service.GetTeacherDashboard(id);
        }

        [Route("{id}/profile/view")]
        [HttpPost]
        public TeacherProfileViewPageModel GetProfileView(TeacherProfileViewRequest request)
        {
            return _service.GetProfileView(request);
        }

        [Route("{id}/profile")]
        public TeacherProfilePageModel GetProfile(long id)
        {
            return _service.GetProfile(id);
        }

        [Route("~/public/teacher/{id}/profile")]
        public TeacherProfilePageModel GetPublicProfile(long id)
        {
            return _service.GetProfile(id);
        }

        [Route("~/public/teacher/profile/search/new")]
        [HttpPost]
        public TeacherSearchPageData GetTeacherSearchPage(TeacherProfileSearchRequest request)
        {
            return _service.GetTeacherSearchPage(request);
        }

        [Route("~/public/teacher/profile/search")]
        [HttpPost]
        public TeacherProfileSearchResponse SearchPublicProfiles(TeacherProfileSearchRequest request)
        {
            return _service.GetTeacherProfiles(request);
        }

        [Route("profile")]
        [HttpPut]
        public Task<TeacherProfileUpdateResponse> UpdateProfile(TeacherProfileUpdateRequest request)
        {
            return _service.UpdateProfile(request);
        }

        [Route("profile/setup/{userId}")]
        [HttpGet]
        public TeacherProfileSetupPageModel TeacherProfileSetupPage(long userId)
        {
            if(userId != this.UserId)
            {
                _userAccessService.ThrowForbiddenException();
            }
            return _service.TeacherProfileSetupPage(userId);
        }

        [Route("profile/setup/setup")]
        [HttpPost]
        public TeacherProfileSetupResponse TeacherProfileSetup(TeacherProfileSetupRequest request)
        {
            return _service.TeacherProfileSetup(request);
        }

        [Route("profile/setup/billing")]
        [HttpPost]
        public bool UpdateTeacherBilling(TeacherBillingData request)
        {
            return _service.UpdateTeacherBilling(request);
        }

        [Route("location/add")]
        [HttpPost]
        public TeacherLocationData AddTeacherLocation(TeacherLocationData request)
        {
            return _service.AddTeacherLocation(request);
        }
    }
}
