﻿using SAMii.DTO.Parents;
using SAMii.DTO.Students;
using SAMii.DTO.User;
using SAMii.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SAMii.API.Parent
{
    [RoutePrefix("parent")]
    public class ParentController : BaseController
    {
        private readonly IParentService _service;
        public ParentController(IParentService service)
        {
            _service = service;
        }

        [Route("{id}/dashboard")]
        public ParentDashboardPageModel GetDashboard(long id)
        {
            return _service.GetParentDashboard(this.UserId);
        }

        [Route("{id}/profile")]
        public ParentProfilePageModel GetProfile(long id)
        {
            return _service.GetParentProfile(this.UserId);
        }

        [Route("{id}/profile")]
        [HttpPost]
        public ParentData UpdateProfile(ParentData request)
        {
            return _service.SaveParent(request);
        }

        [Route("invite")]
        [HttpPost]
        public bool InviteStudent(UserInvitationData request)
        {
            request.FromUserId = this.UserId;
            return _service.InviteStudent(request);
        }

        [Route("{id}/students")]
        public IList<StudentData> GetStudents()
        {
            return _service.GetStudentList(this.UserId);
        }
    }
}
