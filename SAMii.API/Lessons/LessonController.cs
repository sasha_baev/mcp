﻿using SAMii.DTO;
using SAMii.Interfaces.Auth;
using SAMii.Interfaces.Lessons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SAMii.API
{
    [RoutePrefix("lesson")]
    public class LessonController : BaseController
    {
        readonly ILessonService _service;
        readonly IUserAccessService _userAccessService;

        public LessonController(ILessonService service, IUserAccessService userAccessService)
        {
            _service = service;
            _userAccessService = userAccessService;
        }

        [Route("all/{userId}")]
        [HttpGet]
        public IList<LessonData> GetAll(long userId)
        {
            return _service.GetAll(userId);
        }

        [Route("dashboard/{userId}")]
        [HttpGet]
        public LessonDashboardPageModel GetLessonDashboard(long userId)
        {
            return _service.GetLessonDashboard(userId);
        }

        [Route("{id}")]
        public LessonData GetById (long id)
        {
            return _service.GetById(id);
        }

        [Route("new")]
        [HttpPost]
        public LessonCreatePageModel GetNewLesson(LessonNewPageRequest request)
        {
            return _service.GetNewLesson(request);
        }

        [Route("add")]
        public async Task<LessonData> Add(LessonSaveRequest request)
        {
            return await _service.AddLesson(request);
        }

        [Route("{id}/delete")]
        [HttpDelete]
        public bool Delete(long id)
        {
            _service.DeleteLesson(id);
            return true;
        }

        [Route("unavailability")]
        [HttpPost]
        public UnavailabilityPageModel GetUnavailability(UnavailabilityNewPageRequest request)
        {
            return _service.GetUnavailability(request);
        }

        [Route("unavailability/save")]
        [HttpPost]
        public UnavailabilityData SaveUnavailability(UnavailabilityData request)
        {
            return _service.SaveUnavailability(request);
        }
    }
}
