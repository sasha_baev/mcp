﻿using SAMii.DTO;
using SAMii.DTO.Auth;
using SAMii.DTO.Payments;
using SAMii.Interfaces;
using SAMii.Interfaces.Auth;
using SAMii.Interfaces.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SAMii.API.User
{
    [RoutePrefix("user")]
    public class UserController : BaseController
    {
        readonly IUserService _service;
        readonly IAuthService _authService;
        readonly IUserAccessService _userAccessService;
        readonly IPaymentService _paymentService;
        public UserController(IUserService service, 
            IAuthService authService,
            IPaymentService paymentService,
            IUserAccessService userAccessService)
        {
            _service = service;
            _authService = authService;
            _userAccessService = userAccessService;
            _paymentService = paymentService;
        }

        [Route("avatar/{id}")]
        [HttpGet]
        public string GetUserAvatar(long id)
        {
            return _service.GetUserAvatar(id);
        }

        [Route("role")]
        [HttpPut]
        public UserRoleUpdateResponse UpdateUserRole(UserRoleUpdateRequest request)
        {
            return _service.UpdateUserRole(request);
        }

        [HttpPost]
        [Route("change")]
        public PasswordChangeResponse Change(PasswordChangeRequest request)
        {
            //TODO: enable below once the authorization headers are done in the forntend
            //if (this.UserId != request.Id)
            //    throw new HttpResponseException(HttpStatusCode.Forbidden);

            //return _authService.ResetPassword(this.UserId, request);

            return _authService.ResetPassword(request.Id, request);
        }

        [Route("payment/info")]
        [HttpGet]
        public CardData GetCardInfo()
        {
            return _paymentService.GetUserCardInfo(this.UserId);
        }
    }
}
