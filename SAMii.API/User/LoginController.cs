﻿using SAMii.API.Auth;
using SAMii.DTO;
using SAMii.DTO.Auth;
using SAMii.DTO.User;
using SAMii.Enum;
using SAMii.Interfaces.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SAMii.API.User
{
    //[Authorize(Roles = "Admin")]
    [AllowAnonymous]
    [RoutePrefix("public")]
    //[EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class LoginController : BaseController
    {
        readonly IAuthService _user;
        readonly IConfigurationHelper _settings;

        public LoginController(IAuthService user, IConfigurationHelper settings)
        {
            _user = user;
            _settings = settings;
        }

        [HttpPost]
        [Route("login")]
        public async Task<LoginResponse> Post(LoginRequest request)
        {
            return await _user.Login(request, this.IPAddress);
        }

        [HttpPost]
        [Route("signup")]
        public SignUpResponse SignUp(SignUpRequest data)
        {
            return _user.SignUp(data);
        }

        [HttpPost]
        [Route("activate")]
        public ActivateUserResponse Activate(ActivateUserRequest data)
        {
            return _user.Activate(data);
        }

        [HttpPost]
        [Route("forgot")]
        public ResetPasswordResponse ForgotPassword(ForgotRequest data)
        {
            return _user.ResetPasswordRequest(data, this.IPAddress);
        }

        [HttpPost]
        [Route("verify")]
        // verify reset password token
        public VerifyTokenResponse VerifyResetToken(VerifyTokenRequest request)
        {
            return _user.VerifyResetToken(request);
        }

        [HttpPost]
        [Route("reset")]
        public PasswordChangeResponse ResetPassword(PublicPasswordChangeRequest request)
        {
            return _user.PublicResetPassword(request);
        }

        [HttpPost]
        [Route("invitation")]
        public UserInvitationData GetUserInvitation(VerifyTokenRequest request)
        {
            return _user.GetUserInvitation(request);
        }
    }
}
