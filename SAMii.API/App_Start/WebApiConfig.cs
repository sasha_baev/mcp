﻿using Autofac;
using Autofac.Integration.WebApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SAMii.API.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace SAMii.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            try
            {
                // Web API routes
                config.MapHttpAttributeRoutes();

                config.EnableCors();

                config.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "api/{controller}/{id}",
                    defaults: new { id = RouteParameter.Optional }
                );

                SerializationConfig(config);
                AutofacConfig.Register(config);
                SecurityConfig(config);
            }
            catch(Exception ex)
            {
                var log = log4net.LogManager.GetLogger("Startup");
                log.Fatal("Uncaught exception in startup", ex);
            }
        }

        private static void SerializationConfig(HttpConfiguration config)
        {
            // Web API configuration and services
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            var formatter = config.Formatters.JsonFormatter;
            formatter.SerializerSettings = new JsonSerializerSettings
            {
                //Formatting = Formatting.Indented,
                //TypeNameHandling = TypeNameHandling.Objects,
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                //ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }
        private static void SecurityConfig(HttpConfiguration config)
        {
            // Web API configuration and services
            var handler = (config.DependencyResolver as AutofacWebApiDependencyResolver).Container.Resolve<SecurityMessageHandler>();
            config.MessageHandlers.Add(handler);
        }
    }
}
