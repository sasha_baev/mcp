﻿#define DEBUG
using Ganemo.Framework.Util.Log4net;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

[assembly: PreApplicationStartMethod(typeof(SAMii.API.GlobalExceptionHandler), "Register")]

namespace SAMii.API
{
    //A global exception handler that will be used to catch any error
    public class GlobalExceptionHandler : ExceptionHandler
    {
        private ILog log = log4net.LogManager.GetLogger("GlobalExceptionHandler");

        public static void Register()
        {
            log4net.Config.XmlConfigurator.Configure();
            GlobalConfiguration.Configuration
                               .Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());
        }

        public override void Handle(ExceptionHandlerContext context)
        {
            Exception ex = context.Exception;
            if (!(ex is HttpException)) //ignore "file not found"
            {
                var msg = "";
                if (context.Request != null && context.Request.RequestUri != null)
                    msg = context.Request.RequestUri.ToString();

                if (context.Exception.InnerException != null &&
                   context.Exception.InnerException.Message.Contains("UnauthorizedAccessException"))
                {
                    context.Result = new ErrorMessageResult(context.Request, new HttpResponseMessage(HttpStatusCode.Forbidden));
                }
                else
                {
                    var result = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
#if !DEBUG
                    Content = new StringContent("Internal Server Error Occurred"),
#else
                        Content = new StringContent("Server Error: \n" + ex.ToString()),
#endif
                        ReasonPhrase = "Exception"
                    };

                    context.Result = new ErrorMessageResult(context.Request, result);
                }

                log.Fatal(msg, ex);
            }
        }

        public class ErrorMessageResult : IHttpActionResult
        {
            private HttpRequestMessage _request;
            private readonly HttpResponseMessage _httpResponseMessage;

            public ErrorMessageResult(HttpRequestMessage request, HttpResponseMessage httpResponseMessage)
            {
                _request = request;
                _httpResponseMessage = httpResponseMessage;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                return Task.FromResult(_httpResponseMessage);
            }
        }

    }
}