﻿using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using Ganemo.Framework.Security.Jwt;
using Ganemo.Framework.Util.Email;
using SAMii.API.Auth;
using SAMii.Chargebee;
using SAMii.Xero;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Http;

namespace SAMii.API
{
    public class AutofacConfig  : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Ganemo.Framework.Util.Log4net.Logger>().As<Ganemo.Framework.Util.Log4net.ILogger>().SingleInstance();
        }

        public static void Register(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            // scan all assemblies to load services from references libraries 
            var assemblies = BuildManager.GetReferencedAssemblies().Cast<Assembly>();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            foreach (var assembly in assemblies.Where(x => x.FullName.Contains("SAMii")))
            {
                // load all the default interfaces (or services) from all referenced libraries in the alpha vet namespace
                builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces();

                // load all specific modules with services defined, do this after the default load of all assemblies
                // so that coded services can override the default services loaded above.
                builder.RegisterAssemblyModules(assembly);
            }


            List<Type> profiles = RegisterAutoMapperProfileTypes(builder, assemblies);

            //https://autofaccn.readthedocs.io/en/latest/faq/injecting-configured-parameters.html
            builder.Register(ctx =>
            {
                EmailSettings _emailSettings = new EmailSettings
                {
                    ServerHost = ConfigurationManager.AppSettings["Emailer.Server.Host"],
                    ServerPort = ConfigurationManager.AppSettings["Emailer.Server.Port"],
                    UserName = ConfigurationManager.AppSettings["Emailer.Server.UserName"],
                    Password = ConfigurationManager.AppSettings["Emailer.Server.Password"],
                    FromEmail = ConfigurationManager.AppSettings["Emailer.FromEmail.Address"],
                    FromName = ConfigurationManager.AppSettings["Emailer.FromEmail.Name"]
                };

                return new Emailer(_emailSettings);
            }).As<IEmailer>();

            builder.Register(ctx =>
            {
                JwtSecuritySettings _jwtSettings = new JwtSecuritySettings
                {
                    SecretKey = ConfigurationManager.AppSettings["JwtToken.SecretKey"],
                    TokenAudience = ConfigurationManager.AppSettings["JwtToken.Audience"],
                    TokenIssuer = ConfigurationManager.AppSettings["JwtToken.Issuer"],
                    TokenLifetimeMinutes = Convert.ToDouble(ConfigurationManager.AppSettings["JwtToken.LifetimeMinutes"]),
                    SiteName = ConfigurationManager.AppSettings["SiteName"]
                };

                return new JwtTokenManager(_jwtSettings);
            }).As<IJwtTokenManager>();

            builder.RegisterType<SecurityMessageHandler>().As<SecurityMessageHandler>().SingleInstance();

            builder.Register(ctx =>
            {
                ChargebeeSettings _chargebeeSettings = new ChargebeeSettings
                {
                    SiteKey = ConfigurationManager.AppSettings["CB.SiteKey"],
                    AccessKey = ConfigurationManager.AppSettings["CB.AccessKey"],
                };

                return new ChargebeeApi(_chargebeeSettings);
            }).As<IChargebeeApi>();


            builder.Register(ctx =>
            {
                XeroSettings _xeroSettings = new XeroSettings
                {
                    ApiURL = ConfigurationManager.AppSettings["XeroApiUrl"],
                    AccountCode = ConfigurationManager.AppSettings["AccountCode"],
                    CertificateFilepath = ConfigurationManager.AppSettings["SigningCertificate"],
                    CertificatePassword = ConfigurationManager.AppSettings["SigningCertificatePassword"],
                    ConsumerKey = ConfigurationManager.AppSettings["ConsumerKey"],
                    ConsumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"]
                };

                return new XeroHelperApi(_xeroSettings);
            }).As<IXeroHelperApi>();

            // update the dependency injector in .net to use the loaded dependencies 
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);



            RegisterAutoMapper(container, profiles);
        }

        private static List<Type> RegisterAutoMapperProfileTypes(ContainerBuilder builder, IEnumerable<Assembly> assemblies)
        {
            var profiles = new List<Type>();

            // get the all automapper profile implementations from references assemblies 
            foreach (var assembly in assemblies)
            {
                var assemblyProfiles = assembly.ExportedTypes.Where(type => type.IsSubclassOf(typeof(Profile)));
                profiles.AddRange(assemblyProfiles);
            }

            // register the profile types with autofac
            builder.RegisterTypes(profiles.ToArray());

            return profiles;
        }

        private static void RegisterAutoMapper(IContainer container, IEnumerable<Type> loadedProfiles)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.ConstructServicesUsing(container.Resolve);

                foreach (var profile in loadedProfiles)
                {
                    var resolvedProfile = container.Resolve(profile) as Profile;
                    cfg.AddProfile(resolvedProfile);
                }

            });
        }
    }
}