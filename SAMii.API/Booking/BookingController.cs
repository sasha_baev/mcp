﻿using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SAMii.API
{
    [RoutePrefix("booking")]
    public class BookingController : BaseController
    {
        private readonly IBookingService _service;
        public BookingController(IBookingService service)
        {
            _service = service;
        }

        [Route("new")]
        [HttpPost]
        public NewBookingPageData GetNewBooking(NewBookingRequest request)
        {
            return _service.GetNewBooking(request);
        }

        [Route("create")]
        [HttpPost]
        public BookingCreateResponse CreateBooking(BookingRequest request)
        {
            return _service.CreateBooking(request);
        }

        [Route("cancel")]
        [HttpPost]
        public BookingCancelResponse CancelBooking(BookingCancelRequest request)
        {
            return _service.CancelBooking(request);
        }
    }
}
