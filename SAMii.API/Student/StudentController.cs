﻿using SAMii.DTO;
using SAMii.DTO.Students;
using SAMii.Interfaces;
using SAMii.Interfaces.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SAMii.API
{
    [RoutePrefix("student")]
    public class StudentController : BaseController
    {
        private readonly IStudentService _service;
        private readonly IUserAccessService _userAccessService;

        public StudentController(IStudentService service, IUserAccessService userAccessService)
        {
            _service = service;
            _userAccessService = userAccessService;
        }

        [Route("{id}/profile")]
        public StudentProfilePageModel GetProfile(long id)
        {
            return _service.GetStudentProfile(id);
        }

        [Route("{id}/profile")]
        [HttpPost]
        public StudentData UpdateProfile(StudentData request)
        {
            return _service.SaveStudent(request);
        }

        [Route("{id}/dashboard")]
        public StudentDashboardPageModel GetDashboard(long id)
        {
            return _service.GetDashboard(id);
        }

        [Route("{id}/profile/updatefromlesson")]
        public Task<bool> UpdateProfileFromLesson(StudentUpdateRequest request) {
            return _service.UpdateProfileFromLesson(request);
        }
    }
}
