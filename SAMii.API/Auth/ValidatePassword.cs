﻿using Ganemo.Framework.Security.Jwt;
using SAMii.Interfaces.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;

namespace SAMii.API.Auth
{
    public class ValidatePassword : IValidatePassword
    {
        IConfigurationHelper _config;
        IJwtTokenManager _jwtManager;
        public ValidatePassword(IConfigurationHelper config, IJwtTokenManager jwtManager)
        {
            _config = config;
            _jwtManager = jwtManager;
            //_jwtManager = new JwtTokenManager(_config.JwtSettings);
        }

        public bool Validate(string hash, string password)
        {
            return Crypto.VerifyHashedPassword(hash, password);
        }

        public string HashPassword(string plainText)
        {
            return Crypto.HashPassword(plainText);
        }

        //public string GeneratePassword()
        //{
        //    return System.Web.Security.Membership.GeneratePassword(8, 0);
        //}

        public string GenerateToken(long id, string role)
        {
            //var parms = _config.JwtSettings;
            var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Role, role),
                    new Claim("uid", id.ToString())
                };
            //IJwtTokenManager jwtManager = new JwtTokenManager(parms);
            return _jwtManager.CreateJwtToken(claims);
        }

        public string GenerateToken(IEnumerable<Claim> claims)
        {
            //var parms = _config.JwtSettings;
            //IJwtTokenManager jwtManager = new JwtTokenManager(parms);
            return _jwtManager.CreateJwtToken(claims);
        }

        public ClaimsPrincipal ValidateToken(string token)
        {
            //var parms = _config.JwtSettings;

            //IJwtTokenManager jwtManager = new JwtTokenManager(parms);
            return _jwtManager.ValidateJwtToken(token);
        }
    }
}