﻿using Ganemo.Framework.Security.Jwt;
using Ganemo.Framework.Util.Log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SAMii.API.Auth
{
    public class SecurityMessageHandler : DelegatingHandler
    {
        private IJwtTokenManager _jwtManager;
        private readonly ILogger _logger;

        public SecurityMessageHandler(IJwtTokenManager jwtManager, ILogger logger)
        {
            _jwtManager = jwtManager;
            _logger = logger;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            ClaimsPrincipal principal = ParseJwtToken(request.Headers);

            if (HttpContext.Current != null)
                HttpContext.Current.User = principal;
            else
                request.GetRequestContext().Principal = principal;


            Thread.CurrentPrincipal = principal;

            return base.SendAsync(request, cancellationToken);
        }

        #region Private
        private ClaimsPrincipal ParseJwtToken(HttpRequestHeaders headers)
        {
            string jwtToken = null;
            if (!TryRetrieveToken(headers, out jwtToken))
            {
                return null;
            }
            return _jwtManager.ValidateJwtToken(jwtToken);
        }
        private static bool TryRetrieveToken(HttpRequestHeaders headers, out string token)
        {
            token = null;

            if (!headers.Contains("Authorization"))
            {
                return false;
            }

            string authzHeader = headers.GetValues("Authorization").First();

            // Verify Authorization header contains 'Bearer' scheme
            token = authzHeader.StartsWith("Bearer ", StringComparison.InvariantCulture) ? authzHeader.Split(' ')[1] : null;

            return token != null;
        }
        #endregion
    }
}