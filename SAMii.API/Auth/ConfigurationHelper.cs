﻿using Ganemo.Framework.Security.Jwt;
using Ganemo.Framework.Util.Email;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SAMii.API.Auth
{
    public class ConfigurationHelper : IConfigurationHelper
    {
        public string ReleaseVersion { get { return ConfigurationManager.AppSettings["ReleaseVersion"]; } }

        private JwtSecuritySettings _jwtSettings = null;
        public JwtSecuritySettings JwtSettings
        {
            get
            {
                if (_jwtSettings == null)
                {
                    _jwtSettings = new JwtSecuritySettings
                    {
                        SecretKey = ConfigurationManager.AppSettings["JwtToken.SecretKey"],
                        TokenAudience = ConfigurationManager.AppSettings["JwtToken.Audience"],
                        TokenIssuer = ConfigurationManager.AppSettings["JwtToken.Issuer"],
                        TokenLifetimeMinutes = Convert.ToDouble(ConfigurationManager.AppSettings["JwtToken.LifetimeMinutes"]),
                        SiteName = ConfigurationManager.AppSettings["SiteName"],
                        //TokenRefreshAfterMinutes = Convert.ToDouble(ConfigurationManager.AppSettings["JwtToken.RefreshAfterMinutes"]),
                    };

                    if (!string.IsNullOrWhiteSpace(_jwtSettings.SiteName))
                        _jwtSettings.SiteName = _jwtSettings.SiteName.Trim(System.IO.Path.DirectorySeparatorChar);
                }
                return _jwtSettings;
            }
        }
        //private EmailSettings _emailSettings = null;
        //public EmailSettings EmailSettings
        //{
        //    get
        //    {
        //        if (_emailSettings == null)
        //        {
        //            _emailSettings = new EmailSettings
        //            {
        //                ServerHost = ConfigurationManager.AppSettings["Emailer.Server.Host"],
        //                ServerPort = ConfigurationManager.AppSettings["Emailer.Server.Port"],
        //                UserName = ConfigurationManager.AppSettings["Emailer.Server.UserName"],
        //                Password = ConfigurationManager.AppSettings["Emailer.Server.Password"],
        //                FromEmail = ConfigurationManager.AppSettings["Emailer.FromEmail.Address"],
        //                FromName = ConfigurationManager.AppSettings["Emailer.FromEmail.Name"]
        //            };
        //        }
        //        return _emailSettings;
        //    }
        //}
        public string GetGoogleMapKey
        {
            get { return ConfigurationManager.AppSettings["GoogleMapApi"]; }
        }

        public string GetGoogleCalendarKey
        {
            get { return ConfigurationManager.AppSettings["GoogleCalendarApi"]; }
        }
        public string GetGoogleGeocodeKey
        {
            get { return ConfigurationManager.AppSettings["GoogleGeocodeApi"]; }
        }
        public string TimeZone
        {
            get { return ConfigurationManager.AppSettings["TimeZone.Default"]; }
        }
    }
}