﻿using Ganemo.Framework.Security.Jwt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.API.Auth
{
    public interface IConfigurationHelper
    {
        string ReleaseVersion { get; }
        JwtSecuritySettings JwtSettings { get; }
        string GetGoogleMapKey { get; }
        string GetGoogleCalendarKey { get; }
        string GetGoogleGeocodeKey { get; }
        string TimeZone { get; }
    }
}
