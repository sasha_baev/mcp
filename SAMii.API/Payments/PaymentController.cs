﻿using SAMii.DTO.Payments;
using SAMii.Interfaces.Auth;
using SAMii.Interfaces.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SAMii.API.Payments
{
    [RoutePrefix("payment")]
    public class PaymentController : BaseController
    {
        readonly IPaymentService _paymentService;
        readonly IUserAccessService _userAccessService;

        public PaymentController(IPaymentService paymentService,
            IUserAccessService userAccessService)
        {
            _paymentService = paymentService;
            _userAccessService = userAccessService;
        }

        [Route("update")]
        [HttpPost]
        public bool CreateOrUpdateSubscription(BillingCreateData data)
        {
            if ((data.ParentId > 0 && data.ParentId != this.UserId) ||
                (data.ParentId == 0 && this.UserId != data.UserId))
            {
                _userAccessService.ThrowForbiddenException();
            }
            return _paymentService.CreateOrUpdateSubscription(data);
        }
    }
}
