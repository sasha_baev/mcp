﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SAMii.API
{
    public class BaseController : ApiController
    {
        protected string[] DocumentExtensions = { ".pdf", ".doc", ".docx" };
        protected string[] EvidenceExtensions = { ".png", ".jpg", ".jpeg", ".bmp", ".pdf", ".doc", ".docx", ".ppt", ".pptx", ".pps", ".ppsx", "rtf" };
        protected string[] ImageExtensions = { ".png", ".jpg", ".jpeg", ".bmp", ".exif", ".gif", ".tiff", ".jfif", ".ppm", ".pgm", ".pbm", ".pnm" };
        protected string[] SpreadsheetExtensions = { ".xlsx", ".csv", "xls" };

        public string[] AllExtensions()
        {
            string[] res = new string[] { };
            res.Concat(DocumentExtensions);
            res.Concat(EvidenceExtensions);
            res.Concat(ImageExtensions);
            res.Concat(SpreadsheetExtensions);
            return res;
        }

        public string IPAddress
        {
            get
            {
                if (this.Request.Properties.ContainsKey("MS_HttpContext"))
                {
                    var val = Request.Properties["MS_HttpContext"];
                    if (val is HttpContextWrapper ctx)
                    {
                        var ip = ctx.Request.UserHostAddress;
                        return ip == "::1" ? "127:0:0:1" : ip;
                    }
                }
                return string.Empty;
            }
        }

        public IEnumerable<Claim> Claims
        {
            get
            {
                return (User as ClaimsPrincipal).Claims;
            }
        }
        public long UserId
        {

            get
            {
                return Convert.ToInt64(Claims.First(c => c.Type == "uid").Value);
            }
        }
    }

    
}
