﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class PaginationData
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
