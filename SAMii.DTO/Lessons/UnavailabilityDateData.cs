﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Lessons
{
    public class UnavailabilityDateData
    {
        public long Id { get; set; }
        public long UnavailabilityId { get; set; }
        public UnavailabilityData UnvailabilityData { get; set; }
        public DayOfWeek Day { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
