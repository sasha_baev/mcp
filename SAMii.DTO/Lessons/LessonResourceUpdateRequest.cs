﻿using Newtonsoft.Json;
using SAMii.DTO;
using SAMii.DTO.Lessons;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class LessonResourceUpdateRequest
    {
        public long LessonId { get; set; }
        public IList<LessonResourceData> LessonResources { get; set; }
    }
}
