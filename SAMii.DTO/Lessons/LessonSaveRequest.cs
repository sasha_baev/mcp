﻿using Newtonsoft.Json;
using SAMii.DTO;
using SAMii.DTO.Lessons;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class LessonSaveRequest
    {
        public long UserId { get; set; }
        public LessonData Lesson { get; set; }

        // front end properties
        public string LessonIntroductionFileData { get; set; }
        public string LessonIntroductionMimeType { get; set; }
        public string LessonIntroductionFileName { get; set; }
        public bool NewLessonIntroduction { get; set; }
    }
}
