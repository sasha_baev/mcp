﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class LessonCreatePageModel
    {
        public LessonData Lesson { get; set; }
        public IList<InstrumentData> Instruments { get; set; }
        public IList<TeacherLocationData> TeacherLocations { get; set; }
        public IList<LessonData> CurrentLessons { get; set; }
        public double LessonFeePercentage { get; set; }
    }
}
