﻿using Newtonsoft.Json;
using SAMii.DTO;
using SAMii.DTO.Lessons;
using SAMii.Enum;
using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class UnavailabilityData
    {
        public long Id { get; set; }
        public long TeacherId { get; set; }
        public TeacherData Teacher { get; set; }


        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public IList<UnavailabilityDate> Dates { get; set; }
        public int DurationInMin { get; set; }


        public LessonRecurrence RecurrenceType { get; set; }
        public LessonRepeat? RepeatType { get; set; }
        public int? RepeatAmount { get; set; }
    }
}
