﻿using SAMii.DTO.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Lessons
{
    public class LessonSlotData
    {
        public long Id { get; set; }
        public long LessonDateId { get; set; }
        public DateTime StartTime { get; set; }
        public LessonDateData LessonDate { get; set; }

        // front-end only
        public int BookingAmount { get; set; }
        public IList<BookingSummaryData> Bookings { get; set; }
    }
}
