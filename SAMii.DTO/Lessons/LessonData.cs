﻿using Newtonsoft.Json;
using SAMii.DTO.Lessons;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class LessonData
    {
        public long Id { get; set; }
        public long TeacherId { get; set; }
        public TeacherData Teacher { get; set; }
        [JsonIgnore]
        public Guid Guid { get; set; }

        public string Title { get; set; }
        public long InstrumentId { get; set; }
        public InstrumentData Instrument { get; set; }
        public long DurationInMin { get; set; }
        public bool IsGroupLesson { get; set; }
        public int StudentPerGroup { get; set; }
        public decimal Price { get; set; }
        public bool IsCostAbsorb { get; set; }

        public long TeacherLocationId { get; set; }
        public TeacherLocationData TeacherLocation { get; set; }

        [JsonIgnore]
        public string LessonIntroductionS3Key { get; set; }
        public string LessonIntroductionFileName { get; set; }
        public string LessonIntroductionVideoUrl { get; set; }
        public string LessonContent { get; set; } // How I Teach
        public string LessonMethod { get; set; } // What I Teach

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CancelledDate { get; set; }
        public IList<LessonDateData> Dates { get; set; }

        public LessonStatus Status { get; set; }
        public int LessonStep { get; set; }

        public LessonRecurrence RecurrenceType { get; set; }
        public LessonRepeat? RepeatType { get; set; }
        public int? RepeatAmount { get; set; }

        //front-end
        public string LessonIntroductionPresignedUrl { get; set; }
        public IList<LessonResourceData> LessonResources { get; set; }
    }
}
