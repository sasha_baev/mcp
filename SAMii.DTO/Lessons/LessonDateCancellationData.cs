﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Lessons
{
    public class LessonDateCancellationData
    {
        public long Id { get; set; }
        public long LessonDateId { get; set; }
        public LessonDateData LessonDate { get; set; }

        public DateTime CancelDate { get; set; }
        public DateTime CancelEndDate { get; set; }
    }
}
