﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class UnavailabilityNewPageRequest
    {
        public long UserId { get; set; }
        public long? UnavailabilityId { get; set; }
    }
}
