﻿using SAMii.DTO.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class LessonDashboardPageModel
    {
        public IList<LessonData> LessonSummary { get; set; }
        public IList<TeacherLocationData> TeacherLocations { get; set; }
        public IList<UnavailabilityData> Unavailabilities { get; set; }
    }
}
