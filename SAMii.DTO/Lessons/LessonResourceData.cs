﻿using Newtonsoft.Json;
using SAMii.DTO;
using SAMii.DTO.Lessons;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class LessonResourceData
    {
        public long Id { get; set; }
        public long LessonId { get; set; }
        public string FileName { get; set; }
        [JsonIgnore]
        public string LessonResourceS3Url { get; set; }

        // front-end
        public string LessonResourcePresignedUrl { get; set; }

        // request properties
        public bool NewResource { get; set; }
        public string MimeType { get; set; }
        public string FileData { get; set; }
    }
}
