﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class LessonNewPageRequest
    {
        public long UserId { get; set; }
        public long? LessonId { get; set; }
    }
}
