﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Lessons
{
    public class LessonDateData
    {
        public long Id { get; set; }
        public long LessonId { get; set; }
        public LessonData Lesson { get; set; }
        public DayOfWeek Day { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime? BreakStartTime { get; set; }
        public DateTime? CancelledDate { get; set; }
        public LessonDateStatus Status { get; set; }

        public IList<LessonDateCancellationData> Cancellations { get; set; }
        public IList<LessonSlotData> LessonSlots { get; set; }
    }
}
