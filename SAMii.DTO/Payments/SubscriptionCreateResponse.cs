﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Payments
{
    public class SubscriptionCreateResponse : ApiResponse
    {
        public string SubscriptionId { get; set; }
        public string PlanId { get; set; }
    }
}
