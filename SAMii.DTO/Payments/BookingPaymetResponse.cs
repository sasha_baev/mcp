﻿using SAMii.DTO.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Payments
{
    public class BookingPaymetResponse : ApiResponse
    {
        public BookingPaymentData Payment { get; set; }
    }
}
