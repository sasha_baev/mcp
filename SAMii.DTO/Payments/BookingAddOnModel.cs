﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Payments
{
    public class BookingAddOnModel
    {
        public string SubscriptionId { get; set; }
        public BookingPaymentType PaymentType { get; set; }
        public int UnitPrice { get; set; }
        public int LessonsQunatity { get; set; }
    }
}
