﻿using SAMii.DTO.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Payments
{
    public class BookingRefundResponse : ApiResponse
    {
        public BookingRefundData Payment { get; set; }
    }
}

