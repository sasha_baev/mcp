﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Payments
{
    public class SubscriptionCreateRequest : ApiResponse
    {
        public UserData User { get; set; }
        public string CB_Token { get; set; }
    }
}
