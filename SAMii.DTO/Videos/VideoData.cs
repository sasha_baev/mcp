﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class VideoData
    {
        public long Id { get; set; }
        public long TeacherProfileId { get; set; }
        public string Url { get; set; }
    }
}
