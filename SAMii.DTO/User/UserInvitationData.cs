﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.User
{
    public class UserInvitationData
    {
        public long Id { get; set; }

        public long FromUserId { get; set; }
        public UserData FromUser { get; set; }

        public string ToEmail { get; set; }
        public long? ToUserId { get; set; }
        public UserData ToUser { get; set; }

        public DateTime DateSent { get; set; }
        public Guid Token { get; set; }
        public DateTime? TokenExpiry { get; set; }

        public bool Accepted { get; set; }
        public DateTime? DateAccepted { get; set; }
    }
}
