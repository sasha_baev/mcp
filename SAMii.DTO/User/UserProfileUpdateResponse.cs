﻿using Newtonsoft.Json;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class UserProfileUpdateResponse
    {
        public IList<InstrumentData> Instruments { get; set; }
        public IList<InstrumentData> SelectedInstruments { get; set; }
    }
}
