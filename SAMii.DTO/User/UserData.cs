﻿using Newtonsoft.Json;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class UserData
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserType RoleType { get; set; }
        public string Email { get; set; }

        public long AddressId { get; set; }
        public AddressData Address { get; set; }

        public DateTime? DOB { get; set; }
        public string PhoneNumber { get; set; }
        public int ProfileStep { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
        [JsonIgnore]
        public Guid Identifier { get; set; }
        [JsonIgnore]
        public string AvatarS3Key { get; set; }
        public string AvatarFileName { get; set; }
        [JsonIgnore]
        public DateTime CreatedDate { get; set; }
        [JsonIgnore]
        public DateTime Updated { get; set; }

        public bool IsInvited { get; set; }

        public bool Activated { get; set; }
        public bool Suspended { get; set; }
        public bool IsDeleted { get; set; }

        [JsonIgnore]
        public DateTime? LastSignInDate { get; set; }
        [JsonIgnore]
        public Guid? ResetToken { get; set; }
        [JsonIgnore]
        public DateTime? ResetTokenExpiry { get; set; }
        [JsonIgnore]
        public Guid? ActivationToken { get; set; }
        [JsonIgnore]
        public DateTime? ActivationTokenExpiry { get; set; }

        public string AvatarPresignedUrl { get; set; }

        //Front-end properties
        public string AvatarUrl { get; set; }
        //{
        //    get
        //    {
        //        if (!string.IsNullOrWhiteSpace(AvatarS3Key))
        //            return Ganemo.Framework.Data.Util.AwsS3.Instance.GetPresignedUrl(AvatarS3Key, DateTime.Now.AddMinutes(60));

        //        return null;
        //    }
        //}
        public SubscriptionStatus BillingStatus { get; set; }
        public string FullName
        {
            get
            {
                if (string.IsNullOrEmpty(LastName))
                    return FirstName;
                else if (string.IsNullOrEmpty(FirstName))
                    return LastName;

                return string.Format("{0} {1}", FirstName, LastName);
            }
        }
    }
}
