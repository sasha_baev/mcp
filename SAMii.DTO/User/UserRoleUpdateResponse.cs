﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class UserRoleUpdateResponse
    {
        public string AccessToken { get; set; }
        public UserType UserRoleType { get; set; }
        public bool IsProfileSetUp { get; set; }
    }
}
