﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class EducationData
    {
        public long Id { get; set; }
        public long TeacherProfileId { get; set; }
        public string University { get; set; }
        public string Degree { get; set; }
        public string Major { get; set; }
        public int Year { get; set; }

        public bool IsUrl { get; set; }
        public string UrlLink { get; set; }
        public string AttachmentS3Key { get; set; }
        public string AttachmentName { get; set; }

        [JsonIgnore]
        public Guid Identifier { get; set; }

        // Front-end properties
        public string PresignedURL { get; set; }
        public bool NewAttachment { get; set; }
        public string FileData { get; set; }
        public string MimeType { get; set; }
    }
}
