﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Parents
{
    public class ParentProfilePageModel
    {
        public ParentData Parent { get; set; }
    }
}
