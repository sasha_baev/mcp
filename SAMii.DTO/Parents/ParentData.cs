﻿using SAMii.DTO.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Parents
{
    public class ParentData
    {
        public long Id { get; set; }
        public UserData User { get; set; }
        public IList<StudentData> Students { get; set; }
    }
}
