﻿using SAMii.DTO.Students;
using SAMii.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Parents
{
    public class ParentDashboardPageModel
    {
        public ParentData Parent { get; set; }

        // list of inivitation
        public IList<UserInvitationData> Invitations { get; set; }
    }
}
