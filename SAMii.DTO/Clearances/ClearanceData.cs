﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class ClearanceData
    {
        public long Id { get; set; }
        public long TeacherProfileId { get; set; }
        public string ClearanceS3Key { get; set; }
        public string FileName { get; set; }
        public Guid GUID { get; set; }

        // Front-end properties
        public string PresignedURL { get; set; }
        public bool NewClearance { get; set; }
        public string FileData { get; set; }
        public string MimeType { get; set; }
    }
}
