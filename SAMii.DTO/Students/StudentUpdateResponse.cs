﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Students
{
    public class StudentUpdateResponse
    {
        public string AvatarFileData { get; set; }
    }
}
