﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Students
{
    public class StudentUpdateRequest
    {
        public long UserId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public bool NewAvatar { get; set; }
        public string AvatarFileData { get; set; }
        public string AvatarFileName { get; set; }
        public string AvatarMimeType { get; set; }

        public string AvatarS3Key { get; set; }
    }
}
