﻿using SAMii.DTO.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Students
{
    public class StudentDashboardPageModel
    {
        public IList<BookingData> Bookings { get; set; }
    }
}
