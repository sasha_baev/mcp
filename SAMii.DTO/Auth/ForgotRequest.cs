﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class ForgotRequest
    {
        public string Email { get; set; }
    }
}
