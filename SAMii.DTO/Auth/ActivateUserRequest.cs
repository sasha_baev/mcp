﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Auth
{
    public class ActivateUserRequest
    {
        public string Token { get; set; }
    }
}
