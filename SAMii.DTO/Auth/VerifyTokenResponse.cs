﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class VerifyTokenResponse
    {
        public TokenResponseEnum Result { get; set; }
        public UserData UserData { get; set; }
    }
}
