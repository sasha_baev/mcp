﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Auth
{
    public class SignUpResponse
    {
        public SignUpResponseEnum Result { get; set; }
    }
}
