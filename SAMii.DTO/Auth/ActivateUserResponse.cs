﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Auth
{
    public class ActivateUserResponse
    {
        public TokenResponseEnum Result { get; set; }
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserType UserRoleType { get; set; }
    }
}
