﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class NewBookingRequest
    {
        public long StudentUserId { get; set; }
        public long TeacherUserId { get; set; }
    }
}
