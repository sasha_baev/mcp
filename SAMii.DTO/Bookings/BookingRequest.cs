﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingRequest
    {
        public long StudentUserId { get; set; }
        public long LessonSlotId { get; set; }
        public DateTime StartDate { get; set; }
        public BookingPaymentType PaymentType { get; set; }
        public int? WeeksPaid { get; set; }
        
        // Todo
        public BookingPaymentRequest PaymentDetails { get; set; }
    }
}
