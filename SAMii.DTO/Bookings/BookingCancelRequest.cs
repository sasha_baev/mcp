﻿using SAMii.DTO.Lessons;
using SAMii.DTO.Students;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingCancelRequest
    {
        public long BookingId { get; set; }
        public DateTime CancelDate { get; set; }
        public BookingCancelType CancelType { get; set; }
    }
}
