﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class InstrumentLessons
    {
        public InstrumentData Instrument { get; set; }
        public IList<LessonData> Lessons { get; set; }
    }
}
