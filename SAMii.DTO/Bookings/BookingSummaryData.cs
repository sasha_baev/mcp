﻿using SAMii.DTO.Lessons;
using SAMii.DTO.Students;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingSummaryData
    {
        public long LessonSlotId { get; set; }
        public DateTime StartDate { get; set; }
        public BookingStatus Status { get; set; }
        public bool Cancelled { get; set; }
        public int TotalCount { get; set; }
        public IList<BookingCancellationData> Cancellations { get; set; }
    }
}
