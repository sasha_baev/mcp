﻿using SAMii.DTO.Lessons;
using SAMii.DTO.Students;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingData
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public StudentData Student { get; set; }
        public long LessonSlotId { get; set; }
        public LessonSlotData LessonSlot { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public BookingStatus Status { get; set; }
        public BookingPaymentType PaymentType { get; set; }
        public bool Cancelled { get; set; }
        public DateTime? CancelledEndDate { get; set; }
        public int TotalCount { get; set; }
        public int AttendedCount { get; set; }
        public int InCreditCount { get; set; }
        public IList<BookingPaymentData> Payments { get; set; }
        public IList<BookingCancellationData> Cancellations { get; set; }
    }
}
