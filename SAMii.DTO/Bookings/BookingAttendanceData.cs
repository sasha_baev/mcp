﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingAttendanceData
    {
        public long Id { get; set; }
        public long BookingId { get; set; }
        public BookingData Booking { get; set; }
        public DateTime AttendedDate { get; set; }
        public bool TeacherPaid { get; set; }
        public long? TeacherPayoutId { get; set; }
    }
}
