﻿using SAMii.DTO.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class NewBookingPageData
    {
        public IList<InstrumentLessons> LessonsByInstrument { get; set; }
        public IList<UnavailabilityData> Unavailabilities { get; set; }
        public TeacherData Teacher { get; set; }
        public StudentData Student { get; set; }
        public double LessonFeePercentage { get; set; }
    }
}
