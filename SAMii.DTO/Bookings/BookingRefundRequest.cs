﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingRefundRequest
    {
        public long BookingId { get; set; }
        public string InvoiceId { get; set; }
        public int RefundAmount { get; set; }
        public int Quantity { get; set; }
    }
}
