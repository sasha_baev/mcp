﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingCreateResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
