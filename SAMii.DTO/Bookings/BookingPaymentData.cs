﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingPaymentData
    {
        public long Id { get; set; }
        public long BookingId { get; set; }
        public string GatewayId { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
    }
}
