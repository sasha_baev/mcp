﻿using SAMii.DTO.Lessons;
using SAMii.DTO.Students;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingCancellationEmailData
    {
        public TeacherData Teacher { get; set; }
        public StudentData Student { get; set; }

        public DateTime CancellationDate { get; set; }
        public LessonData Lesson { get; set; }
        public BookingCancelType CancelType { get; set; }
    }
}
