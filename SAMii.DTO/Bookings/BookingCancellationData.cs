﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Bookings
{
    public class BookingCancellationData
    {
        public long Id { get; set; }
        public long BookingId { get; set; }
        public DateTime CancelledDate { get; set; }
        public BookingCancelType CancelType { get; set; }
    }
}
