﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Teachers
{
    public class TeacherProfileViewPageModel
    {
        public TeacherData TeacherData { get; set; }
        public IList<LessonData> WeeklyLessons { get; set; }
        public IList<UnavailabilityData> Unavailabilities { get; set; }
    }
}
