﻿using SAMii.DTO.Bookings;
using SAMii.DTO.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Teachers
{
    public class TeacherPayoutData
    {
        public long Id { get; set; }
        public long TeacherId { get; set; }
        public TeacherData Teacher { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceRef { get; set; }
        public string InvoiceId { get; set; }
        public decimal AmountPaid { get; set; }
        public List<BookingAttendanceData> Bookings { get; set; }
        public IList<InvoiveLineItem> LineItems { get; set; }
        public string XeroContactId { get; set; }
    }
}
