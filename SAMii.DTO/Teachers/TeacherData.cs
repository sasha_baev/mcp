﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class TeacherData
    {
        public long Id { get; set; }
        // Step 1
        public long UserId { get; set; }
        public UserData User { get; set; }
        public IList<InstrumentData> Instruments { get; set; }
        public IList<TeacherLocationData> TeacherLocations { get; set; }

        // Step 2 - Self Introduction
        [JsonIgnore]
        public string BannerS3Key { get; set; }
        public string BannerFileName { get; set; }
        public string BannerPresignedUrl { get; set; }
        [JsonIgnore]
        public string IntroductionMediaS3Key { get; set; }
        public string IntroductionMediaFileName { get; set; }
        public string IntroductionMediaVideoUrl { get; set; }
        public string IntroductionMediaPresignedUrl { get; set; }
        public string Introduction { get; set; }
        public string TeachingContent { get; set; }
        public string TeachingMethod { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedInLink { get; set; }
        public string InstagramLink { get; set; }
        public string YouTubeLink { get; set; }

        // Step 3 - Education
        public IList<VideoData> Videos { get; set; }
        public IList<EducationData> Educations { get; set; }
        public IList<ClearanceData> Clearances { get; set; }
        public IList<TestimonialData> Testimonials { get; set; }

        public bool TeachingAddressAlsoResidential { get; set; }

        public bool Public { get; set; }
    }
}
