﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class TeacherProfileUpdateRequest
    {
        public int? Step { get; set; }
        public TeacherData TeacherProfileData { get; set; }

        public bool NewBanner { get; set; }
        public string BannerFileData { get; set; }
        public string BannerFileName { get; set; }
        public string BannerMimeType { get; set; }

        public bool NewAvatar { get; set; }
        public string AvatarFileData { get; set; }
        public string AvatarFileName { get; set; }
        public string AvatarMimeType { get; set; }

        public bool NewIntroductionMedia { get; set; }
        public string IntroductionMediaFileData { get; set; }
        public string IntroductionMediaFileName { get; set; }
        public string IntroductionMediaMimeType { get; set; }

        public string IntroductionMediaVideoUrl { get; set; }
    }
}
