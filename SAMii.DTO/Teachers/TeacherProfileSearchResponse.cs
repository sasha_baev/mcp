﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class TeacherProfileSearchResponse
    {
        public IList<TeacherData> Teachers { get; set; }
        public int TotalTeachersFound { get; set; }
        public string DetectedCity { get; set; }
        public string DetectedState { get; set; }
    }
}
