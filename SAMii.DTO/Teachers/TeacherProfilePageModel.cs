﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Teachers
{
    public class TeacherProfilePageModel
    {
        public TeacherData ProfileData { get; set; }
        public IList<InstrumentData> Instruments { get; set; }
    }
}
