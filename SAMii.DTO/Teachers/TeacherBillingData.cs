﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class TeacherBillingData
    {
        public long TeacherBillingId { get; set; }
        public string ABN { get; set; }
        public bool IsGSTRegister { get; set; }
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public bool IsWeeklyPayment { get; set; }
        public string XeroContactId { get; set; }
    }
}
