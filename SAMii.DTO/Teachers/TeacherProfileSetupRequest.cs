﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Teachers
{
    public class TeacherProfileSetupRequest
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public IList<InstrumentData> Instruments { get; set; }
        public AddressData TeachingAddress { get; set; }
        public bool TeachingAddressAlsoResidential { get; set; }
        public IList<TeacherLocationData> TeacherLocations { get; set; }
    }
}
