﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class TeacherProfileSearchRequest
    {
        public long? InstrumentId { get; set; }
        public string SuburbSearch { get; set; }
        public IList<DayOfWeek> Days { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public PaginationData Pagination { get; set; }
    }
}
