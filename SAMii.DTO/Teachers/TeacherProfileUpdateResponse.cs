﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Teachers
{
    public class TeacherProfileUpdateResponse
    {
        public long ProfileId { get; set; }
        public string BannerS3URL { get; set; }
        public string AvatarS3URL { get; set; }
        public string IntroductionMediaS3URL { get; set; }
        public string BannerFileName { get; set; }
        public string AvatarFileName { get; set; }
        public string IntroductionMediaFileName { get; set; }
        public IList<EducationData> Educations { get; set; }
        public IList<TestimonialData> Testimonials { get; set; }
        public IList<TeacherLocationData> TeacherLocations { get; set; }
    }
}
