﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Teachers
{
    public class TeacherProfileSetupPageModel
    {
        public long TeacherId { get; set; }
        public TeacherProfileSetupResponse Setup { get; set; }
        public TeacherBillingData Billing { get; set; }

        public IList<InstrumentData> Instruments { get; set; }
        public long ProfileStep { get; set; }
    }
}
