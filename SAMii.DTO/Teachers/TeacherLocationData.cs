﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class TeacherLocationData
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public AddressData Address { get; set; }
        public long TeacherId { get; set; }
    }
}
