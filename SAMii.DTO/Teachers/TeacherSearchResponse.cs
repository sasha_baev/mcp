﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO.Teachers
{
    public class TeacherSearchResponse
    {
        public IList<TeacherData> Teachers { get; set; }
        public int TotalCount { get; set; }
    }
}
