﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class TestimonialData
    {
        public long Id { get; set; }
        public long TeacherProfileId { get; set; }

        public string Name { get; set; }
        public string Comment { get; set; }
        public double Rating { get; set; }

        public string TestimonialS3Key { get; set; }
        public string FileName { get; set; }
        public Guid Identifier { get; set; }

        // Front-end properties
        public string PresignedURL { get; set; }
        public bool NewTestimonial { get; set; }
        public string FileData { get; set; }
        public string MimeType { get; set; }
    }
}
