﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public static class AppConstants
    {
        public const int TeacherProfileComplete = 2;
        public const int StudentProfileComplete = 1;
        public const int ParentProfileComplete = 1;
        public const double LessonFeePercentage = 4.95;

        public const string DefaultTeacherVideoUrl = "https://youtu.be/NHkohQimn0g";
    }
}
