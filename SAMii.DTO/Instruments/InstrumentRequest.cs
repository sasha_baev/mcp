﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.DTO
{
    public class InstrumentRequest
    {
        public string Name { get; set; }
        public long UserId { get; set; }
    }
}
