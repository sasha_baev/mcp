﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.DTO;
using SAMii.DTO.Teachers;

namespace SAMii.Interfaces
{
    public interface ITeacherService
    {
        TeacherDashboardPageModel GetTeacherDashboard(long userId);
        TeacherSearchPageData GetTeacherSearchPage(TeacherProfileSearchRequest request);
        TeacherProfileSearchResponse GetTeacherProfiles(TeacherProfileSearchRequest request);
        TeacherProfilePageModel GetProfile(long id);
        Task<TeacherProfileUpdateResponse> UpdateProfile(TeacherProfileUpdateRequest request);
        TeacherProfileSetupPageModel TeacherProfileSetupPage(long userId);
        TeacherProfileSetupResponse TeacherProfileSetup(TeacherProfileSetupRequest request);
        bool UpdateTeacherBilling(TeacherBillingData request);
        TeacherLocationData AddTeacherLocation(TeacherLocationData request);
        TeacherProfileViewPageModel GetProfileView(TeacherProfileViewRequest request);
    }
}
