﻿using SAMii.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Interfaces.Lessons
{
    public interface ILessonService
    {
        LessonData GetById(long id);
        IList<LessonData> GetAll(long userId);
        LessonCreatePageModel GetNewLesson(LessonNewPageRequest request);
        Task<LessonData> AddLesson(LessonSaveRequest request);
        LessonDashboardPageModel GetLessonDashboard(long userId);
        void DeleteLesson(long lessonId);
        UnavailabilityPageModel GetUnavailability(UnavailabilityNewPageRequest request);
        UnavailabilityData SaveUnavailability(UnavailabilityData request);

    }
}
