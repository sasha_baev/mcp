﻿using SAMii.DTO;
using SAMii.DTO.Teachers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Interfaces.Payments
{
    public interface IXeroService
    {
        void AddorUpdateContact(TeacherBillingData request);
        void GetBankDetails(TeacherBillingData data);
        bool CreateBill(TeacherPayoutData data);
    }
}
