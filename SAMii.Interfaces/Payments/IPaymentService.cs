﻿using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.Payments;
using SAMii.DTO.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Interfaces.Payments
{
    public interface IPaymentService
    {
        CardData GetUserCardInfo(long userid);
        bool CreateOrUpdateSubscription(BillingCreateData data);
        bool LessonBookingPayment(LessonData lessonData, StudentData studentData, BookingRequest bookingRequest, long bookingId);
        Tuple<bool, bool> BookingRecurringPayments(BookingData booking);
        decimal GetTeacherLessonPrice(Decimal lessonPrice, bool absorbCost);
        bool ProcessRefunds(BookingCancelRequest request);
    }
}
