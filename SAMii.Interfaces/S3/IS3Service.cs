﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Interfaces.S3
{
    public interface IS3Service
    {
        void UploadFile(string key, string filePath, string contentType);
        void UploadFile(string key, System.IO.Stream fileStream, string contentType);
        Task<Stream> DownloadFile(string key);
        void DeleteKey(string key);
        string GetPresignedUrl(string key, DateTime expiryTime);
    }
}
