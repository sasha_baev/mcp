﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Interfaces.Schedule
{
    public interface IScheduleService
    {
        int StudentMarkAttendace(int markAttendanceDays);
        Tuple<int, int> ProcessBookingRecurringPayments();
        void SendErrorNotification(string email, string context, string exception);
        void ProcessTeacherPayments();
    }
}
