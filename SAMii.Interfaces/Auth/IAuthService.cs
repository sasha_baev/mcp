﻿using SAMii.DTO;
using SAMii.DTO.Auth;
using SAMii.DTO.User;
using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Interfaces.Auth
{
    public interface IAuthService
    {
        string CreateJwtToken(UserData user);
        Task<LoginResponse> Login(LoginRequest request, string ipAddress);
        SignUpResponse SignUp(SignUpRequest data);
        ActivateUserResponse Activate(ActivateUserRequest data);
        ResetPasswordResponse ResetPasswordRequest(ForgotRequest data, string ipAddress);
        VerifyTokenResponse VerifyResetToken(VerifyTokenRequest request);
        PasswordChangeResponse ResetPassword(long userId, PasswordChangeRequest request);
        PasswordChangeResponse PublicResetPassword(PublicPasswordChangeRequest request);
        int GetRoleStepComplete(UserData user);
        UserInvitationData GetUserInvitation(VerifyTokenRequest request);
    }
}
