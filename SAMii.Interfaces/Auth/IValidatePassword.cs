﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Interfaces.Auth
{
    public interface IValidatePassword
    {
        bool Validate(string hash, string password);
        string HashPassword(string plainText);
        //string GeneratePassword();
        ClaimsPrincipal ValidateToken(string token);
        string GenerateToken(long id, string role);
        string GenerateToken(IEnumerable<Claim> claims);
    }
}
