﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.DTO;
using SAMii.DTO.Bookings;

namespace SAMii.Interfaces
{
    public interface IBookingService
    {
        NewBookingPageData GetNewBooking(NewBookingRequest request);
        BookingCreateResponse CreateBooking(BookingRequest request);
        BookingCancelResponse CancelBooking(BookingCancelRequest request);
    }
}
