﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.DTO.Parents;
using SAMii.DTO.Students;
using SAMii.DTO.User;

namespace SAMii.Interfaces
{
    public interface IParentService
    {
        ParentProfilePageModel GetParentProfile(long id);
        ParentData SaveParent(ParentData request);
        bool InviteStudent(UserInvitationData request);
        ParentDashboardPageModel GetParentDashboard(long id);
        IList<StudentData> GetStudentList(long userId);
    }
}
