﻿using SAMii.DTO;
using SAMii.DTO.Bookings;
using SAMii.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Interfaces.Utilities
{
    public interface IEmailService
    {
        string SendRegisterNewUserEmail(UserData user);
        string SendResetPasswordEmail(UserData user);
        void SendBookingTopupEmail(UserData user);
        void SendStudentInvitationEmail(UserInvitationData invitation);
        void SendBookingCancellationEmails(BookingCancellationEmailData data);
        void SendErrorNotification(string email, string context, string exception);
    }
}
