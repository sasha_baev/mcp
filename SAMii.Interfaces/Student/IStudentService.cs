﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.DTO;
using SAMii.DTO.Students;

namespace SAMii.Interfaces
{
    public interface IStudentService
    {
        StudentProfilePageModel GetStudentProfile(long userId);
        StudentData SaveStudent(StudentData request);
        StudentDashboardPageModel GetDashboard(long userId);
        Task<bool> UpdateProfileFromLesson(StudentUpdateRequest request);
    }
}
