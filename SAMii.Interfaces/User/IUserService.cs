﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.DTO;

namespace SAMii.Interfaces
{
    public interface IUserService
    {
        string GetUserAvatar(long userId);
        UserRoleUpdateResponse UpdateUserRole(UserRoleUpdateRequest request);
    }
}
