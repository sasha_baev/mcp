﻿using Autofac;
using Ganemo.Framework.Util.Email;
using SAMii.Interfaces.Schedule;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Process
{
    public class Program
    {
        static void Main(string[] args)
        {
            var log = log4net.LogManager.GetLogger("Process");
            IScheduleService scheduler = null;
            try
            {

                Console.WriteLine(" ***** START ***** : " + DateTime.Now.ToString("yyyy-MM-dd HHmmss"));
              
                var container = AutofacConfigSetup.Register();
                var markAttendanceDays = Convert.ToInt32(ConfigurationManager.AppSettings["MarkAttendanceDays"]);
                scheduler = container.Resolve<IScheduleService>();

                log.Info("---ProcessStart ---");

                //STEP 1 - Mark Attendance
                var count = scheduler.StudentMarkAttendace(markAttendanceDays);
                Console.WriteLine(string.Format("MarkAttendanceDays({0}) : {1}", markAttendanceDays, count));

                //STEP 2 - Process payments and send notifications
                Tuple<int, int> result = scheduler.ProcessBookingRecurringPayments();
                Console.WriteLine(string.Format("Booking Notify Count : {0}", result.Item1));
                Console.WriteLine(string.Format("Booking Payment Count : {0}",  result.Item2));

                //STEP 3 - Process Teacher Payouts
                scheduler.ProcessTeacherPayments();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                log.Fatal("Uncaught exception in Process", ex);

                SupportErrorNotification(scheduler, "Schedule Task Error", ex);
            }
            finally
            {
                Console.WriteLine("DONE");
                log.Info("---ProcessDone ---");
                //Console.ReadKey();
            }
        }

        private static void SupportErrorNotification(IScheduleService scheduler, string context, Exception ex)
        {
            string toList = ConfigurationManager.AppSettings["ErrorEmailRecipients"];
            if (!string.IsNullOrWhiteSpace(toList))
            {
                string[] emails = toList.Split(';');

                foreach (string email in emails)
                {
                    string sername = ConfigurationManager.AppSettings["ErrorServerName"];
                    context = string.IsNullOrWhiteSpace(context) ? "Application Error" : context;
                    context = string.Concat(sername, " ", context);

                    scheduler.SendErrorNotification(email, context, ex.ToString());
                }
            }
        }
    }
}
