﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Autofac;
using Ganemo.Framework.Util.Email;
using System.Configuration;
using SAMii.Chargebee;
using SAMii.Xero;
using AutoMapper;

namespace SAMii.Process
{
    public class AutofacConfigSetup : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Ganemo.Framework.Util.Log4net.Logger>().As<Ganemo.Framework.Util.Log4net.ILogger>().SingleInstance();
        }

        public static IContainer Register()
        {
            var builder = new ContainerBuilder();
            List<Assembly> assemblies = new List<Assembly>();

            var files = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll", SearchOption.TopDirectoryOnly);

            foreach (string file in files)
            {
                var filename = Path.GetFileName(file);

                if (filename.Contains("SAMii"))
                {
                    var assembly = Assembly.LoadFrom(file);
                    // load all the default interfaces (or services) from all referenced libraries in the alpha vet namespace
                    builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces();
                    // load all specific modules with services defined, do this after the default load of all assemblies
                    // so that coded services can override the default services loaded above.
                    builder.RegisterAssemblyModules(assembly);
                    assemblies.Add(assembly);
                }
            }

            List<Type> profiles = RegisterAutoMapperProfileTypes(builder, assemblies);

            //https://autofaccn.readthedocs.io/en/latest/faq/injecting-configured-parameters.html
            builder.Register(ctx =>
            {
                EmailSettings _emailSettings = new EmailSettings
                {
                    ServerHost = ConfigurationManager.AppSettings["Emailer.Server.Host"],
                    ServerPort = ConfigurationManager.AppSettings["Emailer.Server.Port"],
                    UserName = ConfigurationManager.AppSettings["Emailer.Server.UserName"],
                    Password = ConfigurationManager.AppSettings["Emailer.Server.Password"],
                    FromEmail = ConfigurationManager.AppSettings["Emailer.FromEmail.Address"],
                    FromName = ConfigurationManager.AppSettings["Emailer.FromEmail.Name"]
                };

                return new Emailer(_emailSettings);
            }).As<IEmailer>();

            builder.Register(ctx =>
            {
                ChargebeeSettings _chargebeeSettings = new ChargebeeSettings
                {
                    SiteKey = ConfigurationManager.AppSettings["CB.SiteKey"],
                    AccessKey = ConfigurationManager.AppSettings["CB.AccessKey"],
                };

                return new ChargebeeApi(_chargebeeSettings);
            }).As<IChargebeeApi>();


            builder.Register(ctx =>
            {
                XeroSettings _xeroSettings = new XeroSettings
                {
                    ApiURL = ConfigurationManager.AppSettings["XeroApiUrl"],
                    AccountCode = ConfigurationManager.AppSettings["AccountCode"],
                    CertificateFilepath = ConfigurationManager.AppSettings["SigningCertificate"],
                    CertificatePassword = ConfigurationManager.AppSettings["SigningCertificatePassword"],
                    ConsumerKey = ConfigurationManager.AppSettings["ConsumerKey"],
                    ConsumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"]
                };

                return new XeroHelperApi(_xeroSettings);
            }).As<IXeroHelperApi>();

            // update the dependency injector in .net to use the loaded dependencies 
            var container = builder.Build();
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            RegisterAutoMapper(container, profiles);

            return container;
        }

        private static List<Type> RegisterAutoMapperProfileTypes(ContainerBuilder builder, IEnumerable<Assembly> assemblies)
        {
            var profiles = new List<Type>();

            // get the all automapper profile implementations from references assemblies 
            foreach (var assembly in assemblies)
            {
                var assemblyProfiles = assembly.ExportedTypes.Where(type => type.IsSubclassOf(typeof(Profile)));
                profiles.AddRange(assemblyProfiles);
            }

            // register the profile types with autofac
            builder.RegisterTypes(profiles.ToArray());

            return profiles;
        }

        private static void RegisterAutoMapper(IContainer container, IEnumerable<Type> loadedProfiles)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.ConstructServicesUsing(container.Resolve);

                foreach (var profile in loadedProfiles)
                {
                    var resolvedProfile = container.Resolve(profile) as Profile;
                    cfg.AddProfile(resolvedProfile);
                }

            });
        }
    }
}
