﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Ganemo.Framework.Security.Jwt
{
    public class JwtTokenManager : IJwtTokenManager
    {
        private JwtSecuritySettings _securitySettings;

        public JwtTokenManager(JwtSecuritySettings securitySettings)
        {
            _securitySettings = securitySettings;
        }

        SymmetricSecurityKey SecurityKey
        {
            get
            {
                return new SymmetricSecurityKey(Encoding.Default.GetBytes(_securitySettings.SecretKey));
            }
        }

        public string CreateJwtToken(IEnumerable<Claim> claims)
        {
            var now = DateTime.UtcNow;
            var signingCredentials = new SigningCredentials( SecurityKey, SecurityAlgorithms.HmacSha256Signature);
            var secToken = new JwtSecurityToken(
                issuer: _securitySettings.TokenIssuer,
                audience: _securitySettings.TokenAudience,
                claims: claims,
                notBefore: now,
                expires: now.AddMinutes(_securitySettings.TokenLifetimeMinutes),
                signingCredentials: signingCredentials);

            var handler = new JwtSecurityTokenHandler();
            return handler.WriteToken(secToken);
        }

        public ClaimsPrincipal ValidateJwtToken(string securityToken)
        {
            var handler = new JwtSecurityTokenHandler();
            TokenValidationParameters validationParams =
                new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    RequireSignedTokens = true,
                    ValidAudience = _securitySettings.TokenAudience,
                    ValidIssuer = _securitySettings.TokenIssuer,
                    ValidateIssuer = true,
                    IssuerSigningKey = SecurityKey,
                };
            SecurityToken t;
            return handler.ValidateToken(securityToken, validationParams, out t);
        }

    }
}
