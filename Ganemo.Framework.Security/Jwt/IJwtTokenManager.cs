﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Security.Jwt
{
    public interface IJwtTokenManager
    {
        string CreateJwtToken(IEnumerable<Claim> claims);
        ClaimsPrincipal ValidateJwtToken(string securityToken);
    }
}
