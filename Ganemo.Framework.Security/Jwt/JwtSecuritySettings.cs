﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Security.Jwt
{
    public class JwtSecuritySettings
    {
        public string SecretKey { get; set; }
        public string TokenIssuer { get; set; }
        public string TokenAudience { get; set; }
        public double TokenLifetimeMinutes { get; set; }
        public double? TokenRefreshAfterMinutes { get; set; }
        public string SiteName { get; set; }
    }
}
