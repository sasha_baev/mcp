﻿using SAMii.Repository.EF.Contexts.Config;
using SAMii.Repository.Models;
using SAMii.Repository.Models.Common;
using SAMii.Repository.Models.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.EF.Contexts
{
    public class BaseContext : DbContext,
                                ILessonContext,
                                IUserContext,
                                ITeacherContext,
                                IInstrumentContext,
                                IStudentContext,
                                IBookingContext,
                                IParentContext
    {                   
        public BaseContext() : base("SAMiiContext")
        {
            // if you have an existing database, you nee this to avoid blowing it away, 
            // when enabling migrations 
            Database.SetInitializer<BaseContext>(null);
            //this.Configuration.LazyLoadingEnabled = false;
        }

        // Application Log
        public DbSet<ApplicationLog> ApplicationLog { get; set; }

        // Address
        public DbSet<Address> Addresses { get; set; }

        // Lessons
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<LessonDate> LessonDates { get; set; }
        public DbSet<LessonResource> LessonResources { get; set; }
        public DbSet<LessonSlot> LessonSlots { get; set; }
        public DbSet<LessonDateCancellation> LessonDateCancellations { get; set; }
        public DbSet<Unavailability> Unavailabilities { get; set; }
        public DbSet<UnavailabilityDate> UnavailabilityDates { get; set; }

        // User
        public DbSet<User> Users { get; set; }
        public DbSet<UserInvitation> UserInvitations { get; set; }

        // Teacher
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<TeacherLocation> TeacherLocations { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Clearance> Clearances { get; set; }
        public DbSet<Testimonial> Testimonials { get; set; }
        public DbSet<TeacherBilling> TeacherBilling { get; set; }
        public DbSet<TeacherPayout> TeacherPayout { get; set; }
        public DbSet<AppSetting> AppSettings { get; set; }
        // Student
        public DbSet<Student> Students { get; set; }

        // Parent
        public DbSet<Parent> Parents { get; set; }

        // Instruments
        public DbSet<Instrument> Instruments { get; set; }
        public DbSet<InstrumentTeacher> InstrumentTeachers { get; set; }

        // Bookings
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<BookingRefund> BookingRefunds { get; set; }
        public DbSet<BookingPayment> BookingPayments { get; set; }
        public DbSet<BookingCancellation> BookingCancellations { get; set; }
        public DbSet<BookingAttendance> BookingAttendances { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new TeacherConfig());
            modelBuilder.Configurations.Add(new InstrumentConfig());
            modelBuilder.Configurations.Add(new InstrumentTeacherConfig());
            modelBuilder.Configurations.Add(new LessonConfig());
            modelBuilder.Configurations.Add(new LessonSlotConfig());
            modelBuilder.Configurations.Add(new BookingConfig());
            base.OnModelCreating(modelBuilder);
        }
    }
}
