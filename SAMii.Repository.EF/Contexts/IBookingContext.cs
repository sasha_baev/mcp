﻿using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.EF.Contexts
{
    public interface IBookingContext
    {
        DbSet<Booking> Bookings { get; set; }
        DbSet<BookingPayment> BookingPayments { get; set; }
        DbSet<BookingRefund> BookingRefunds { get; set; }
        DbSet<BookingCancellation> BookingCancellations { get; set; }
        DbSet<BookingAttendance> BookingAttendances { get; set; }
        int SaveChanges();
    }
}
