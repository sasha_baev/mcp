﻿using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.EF.Contexts
{
    public interface IParentContext
    {
        DbSet<Parent> Parents { get; set; }
        int SaveChanges();
    }
}
