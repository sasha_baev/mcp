﻿using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.EF.Contexts
{
    public interface IUserContext
    {
        DbSet<User> Users { get; set; }
        DbSet<UserInvitation> UserInvitations { get; set; }
        DbSet<Address> Addresses { get; set; }
        int SaveChanges();
    }
}
