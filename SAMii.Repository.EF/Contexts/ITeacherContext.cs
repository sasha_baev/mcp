﻿using SAMii.Repository.Models;
using SAMii.Repository.Models.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.EF.Contexts
{
    public interface ITeacherContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Teacher> Teachers { get; set; }
        DbSet<TeacherLocation> TeacherLocations { get; set; }
        DbSet<InstrumentTeacher> InstrumentTeachers { get; set; }
        DbSet<Education> Educations { get; set; }
        DbSet<Video> Videos { get; set; }
        DbSet<Clearance> Clearances { get; set; }
        DbSet<Testimonial> Testimonials { get; set; }
        DbSet<TeacherBilling> TeacherBilling { get; set; }
        DbSet<Address> Addresses { get; set; }
        DbSet<TeacherPayout> TeacherPayout { get; set; }
        DbSet<AppSetting> AppSettings { get; set; }
        int SaveChanges();
    }
}
