﻿using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.EF.Contexts
{
    public interface ILessonContext
    {
        DbSet<Lesson> Lessons { get; set; }
        DbSet<LessonDate> LessonDates { get; set; }
        DbSet<LessonResource> LessonResources { get; set; }
        DbSet<LessonSlot> LessonSlots { get; set; }
        DbSet<LessonDateCancellation> LessonDateCancellations { get; set; }
        DbSet<Unavailability> Unavailabilities { get; set; }
        DbSet<UnavailabilityDate> UnavailabilityDates { get; set; }

        int SaveChanges();
    }
}
