﻿using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.EF.Contexts
{
    public interface IInstrumentContext
    {
        DbSet<Instrument> Instruments { get; set; }
        DbSet<InstrumentTeacher> InstrumentTeachers { get; set; }

        int SaveChanges();
    }
}
