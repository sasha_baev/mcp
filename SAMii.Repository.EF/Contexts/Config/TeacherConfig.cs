﻿using SAMii.Repository.Models;
using System.Data.Entity.ModelConfiguration;

namespace SAMii.Repository.EF.Contexts.Config
{
    internal sealed class TeacherConfig : EntityTypeConfiguration<Teacher>
    {
        public TeacherConfig()
        {
            this.HasOptional(x => x.TeacherBilling).WithRequired(x => x.Teacher);
            this.HasMany(x => x.Instruments).WithRequired(x => x.Teacher).WillCascadeOnDelete(false);
        }
    }
}


