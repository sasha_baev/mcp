﻿using SAMii.Repository.Models;
using System.Data.Entity.ModelConfiguration;

namespace SAMii.Repository.EF.Contexts.Config
{
    internal sealed class InstrumentConfig : EntityTypeConfiguration<Instrument>
    {
        public InstrumentConfig()
        {
            this.HasMany(x => x.Teachers).WithRequired(x => x.Instrument).WillCascadeOnDelete(false);
        }
    }
}


