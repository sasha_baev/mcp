﻿using SAMii.Repository.Models;
using System.Data.Entity.ModelConfiguration;

namespace SAMii.Repository.EF.Contexts.Config
{
    internal sealed class InstrumentTeacherConfig : EntityTypeConfiguration<InstrumentTeacher>
    {
        public InstrumentTeacherConfig()
        {
            this.HasRequired(x => x.Teacher).WithMany().WillCascadeOnDelete(false);
            this.HasRequired(x => x.Instrument).WithMany().WillCascadeOnDelete(false);
        }
    }
}


