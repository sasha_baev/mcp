﻿using SAMii.Repository.Models;
using System.Data.Entity.ModelConfiguration;

namespace SAMii.Repository.EF.Contexts.Config
{
    internal sealed class LessonConfig : EntityTypeConfiguration<Lesson>
    {
        public LessonConfig()
        {
            this.HasRequired(x => x.TeacherLocation).WithMany().WillCascadeOnDelete(false);
        }
    }
}


