﻿using SAMii.Repository.Models;
using System.Data.Entity.ModelConfiguration;

namespace SAMii.Repository.EF.Contexts.Config
{
    internal sealed class LessonDateConfig : EntityTypeConfiguration<LessonDate>
    {
        public LessonDateConfig()
        {
            this.HasMany(x => x.LessonSlots).WithRequired(x => x.LessonDate).WillCascadeOnDelete(false);
        }
    }
}


