﻿using SAMii.Repository.Models;
using System.Data.Entity.ModelConfiguration;

namespace SAMii.Repository.EF.Contexts.Config
{
    internal sealed class BookingConfig : EntityTypeConfiguration<Booking>
    {
        public BookingConfig()
        {
            this.HasRequired(x => x.LessonSlot).WithMany(x => x.Bookings).WillCascadeOnDelete(false);
            this.HasRequired(x => x.Student).WithMany(x => x.Bookings).WillCascadeOnDelete(false);
        }
    }
}


