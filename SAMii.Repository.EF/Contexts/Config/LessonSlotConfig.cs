﻿using SAMii.Repository.Models;
using System.Data.Entity.ModelConfiguration;

namespace SAMii.Repository.EF.Contexts.Config
{
    internal sealed class LessonSlotConfig : EntityTypeConfiguration<LessonSlot>
    {
        public LessonSlotConfig()
        {
            this.HasMany(x => x.Bookings).WithRequired(x => x.LessonSlot).WillCascadeOnDelete(false);
        }
    }
}


