﻿using SAMii.DTO;
using SAMii.Repository.EF.Contexts;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;
using SAMii.Repository.Models;
using SAMii.DTO.Students;

namespace SAMii.Repository.EF.Repositories {
    public class StudentRepository: IStudentRepository
    {
        private readonly IStudentContext _context;

        public StudentRepository(IStudentContext context)
        {
            _context = context;
        }

        public void InitialiseStudent(long userId, long? parentId = null)
        {
            var student = new Student{ UserId = userId };
            if (parentId.HasValue)
                student.ParentId = parentId.Value;

            _context.Students.Add(student);
            _context.SaveChanges();
        }

        public StudentData GetStudent(long userId)
        {
            var record = _context.Students
                                 .AsNoTracking()
                                 .Include(x => x.User)
                                 .Include("User.Address")
                                 .First(x => x.UserId == userId);

            return Mapper.Map<StudentData>(record);
        }

        public StudentData UpdateStudent(StudentData request)
        {
            var record = _context.Students
                                 .Include(x => x.User)
                                 .First(x => x.Id == request.Id);

            record = Mapper.Map(request, record);

            if (record.User.ProfileStep < request.User.ProfileStep)
                record.User.ProfileStep = request.User.ProfileStep;

            _context.SaveChanges();

            return Mapper.Map<StudentData>(record);
        }
    }
}
