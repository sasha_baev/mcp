﻿using AutoMapper;
using SAMii.DTO;
using SAMii.DTO.Auth;
using SAMii.Repository.EF.Contexts;
using SAMii.Repository.Interface;
using SAMii.Repository.Models;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.DTO.Teachers;
using SAMii.DTO.Students;
using SAMii.Enum;
using SAMii.DTO.User;

namespace SAMii.Repository.EF.Repositories.User
{
    public class UserRepository : IUserRepository
    {
        private enum UserDataSubset
        {
            None = 0,
            Login = 1,
            EmailVerify = 2,
            Profile = 3,
            ResetToken = 4
        }

        readonly IUserContext _context;
        readonly IInstrumentRepository _instrumentRepository;
        public UserRepository(IUserContext context, IInstrumentRepository instrumentRepository)
        {
            _context = context;
            _instrumentRepository = instrumentRepository;
        }

        public UserData SignOn(string signOnString)
        {
            IQueryable<SAMii.Repository.Models.User> query = _context.Users.AsNoTracking().Where(x => x.Email == signOnString);
            return GetUserDataSubset(query, UserDataSubset.Login).FirstOrDefault();
        }

        public UserData GetByEmail(string email)
        {
            IQueryable<SAMii.Repository.Models.User> query = _context.Users.AsNoTracking()
                             .Where(x => email.Equals(x.Email, StringComparison.OrdinalIgnoreCase) && !x.IsDeleted);
                             
            return GetUserDataSubset(query, UserDataSubset.EmailVerify).FirstOrDefault();
        }

        public UserData CreateUser(string email, string hashPassword, bool isInvited = false)
        {
            var newUser = new SAMii.Repository.Models.User()
            {
                Email = email,
                Password = hashPassword,
                Identifier = Guid.NewGuid(),
                Activated = false,
                ActivationToken = Guid.NewGuid(),
                ActivationTokenExpiry = DateTime.UtcNow.AddDays(1),
                IsInvited = isInvited,
            };

            // only student can be invited to signup now
            if (isInvited)
                newUser.RoleType = UserType.Student;

            _context.Users.Add(newUser);
            _context.SaveChanges();

            return Mapper.Map<UserData>(newUser);
        }

        /// <summary>
        /// Update database noting that they are signed in.
        /// </summary>
        public void Login(long id, string ipAddress)
        {
            var user = _context.Users.First(x => x.Id == id && !x.IsDeleted);
            user.LastSignInDate = DateTime.UtcNow;
            user.ResetToken = null;
            user.LastIp = ipAddress;
            _context.SaveChanges();
        }

        public UserData GetUserProfile(long id)
        {
            var user = _context.Users
                               .AsNoTracking()
                               .Include(x => x.Address)
                               .First(x => x.Id == id);

            return new UserData
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Id = user.Id,
                RoleType = user.RoleType,
                Suspended = user.Suspended,
                IsDeleted = user.IsDeleted,
                DOB = user.DOB,
                PhoneNumber = user.PhoneNumber,
                Address = Mapper.Map<AddressData>(user.Address),
                ProfileStep = user.ProfileStep,
                AvatarS3Key = user.AvatarS3Key,
                AvatarFileName = user.AvatarFileName,
                Identifier = user.Identifier,
                BillingStatus = user.BillingStatus,
                Email = user.Email
            };
        }

        public void UpdateUserBillingStatus(SubscriptionStatus status, long userId)
        {
            var user = _context.Users
                               .First(x => x.Id == userId);

            user.BillingStatus = status;
            _context.SaveChanges();
        }

        private IQueryable<UserData> GetUserDataSubset(IQueryable<SAMii.Repository.Models.User> user, UserDataSubset userDataSubset = UserDataSubset.None)
        {
            if (userDataSubset == UserDataSubset.Login)
            {
                return user.Select(u => new UserData
                {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Id = u.Id,
                    Password = u.Password,
                    RoleType = u.RoleType,
                    Suspended = u.Suspended,
                    IsDeleted = u.IsDeleted,
                    Activated = u.Activated,
                    ProfileStep = u.ProfileStep,
                    Identifier = u.Identifier,
                    AvatarS3Key = u.AvatarS3Key,
                    IsInvited = u.IsInvited
                });
            }
            else if (userDataSubset == UserDataSubset.EmailVerify)
            {
                return user.Select(u => new UserData()
                {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Id = u.Id,
                    Email = u.Email,
                    Suspended = u.Suspended,
                    IsDeleted = u.IsDeleted,
                    Activated = u.Activated,
                    Identifier = u.Identifier
                });
            }
            else if (userDataSubset == UserDataSubset.Profile)
            {
                return user.Select(u => new UserData()
                {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Id = u.Id,
                    RoleType = u.RoleType,
                    Suspended = u.Suspended,
                    IsDeleted = u.IsDeleted,
                    DOB = u.DOB,
                    PhoneNumber = u.PhoneNumber,
                    Address = Mapper.Map<AddressData>(u.Address),
                    ProfileStep = u.ProfileStep,
                    AvatarS3Key = u.AvatarS3Key,
                    Identifier = u.Identifier
                });
            }
            else if (userDataSubset == UserDataSubset.ResetToken)
            {
                return user.Select(u => new UserData() {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Id = u.Id,
                    Suspended = u.Suspended,
                    IsDeleted = u.IsDeleted,
                    Activated = u.Activated,
                    ResetToken = u.ResetToken,
                    ResetTokenExpiry = u.ResetTokenExpiry,
                    Identifier = u.Identifier
                });
            }
            else
            {
                return user.Select(u => new UserData
                {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Id = u.Id,
                    Password = u.Password,
                    RoleType = u.RoleType,
                    Suspended = u.Suspended,
                    IsDeleted = u.IsDeleted,
                    Activated = u.Activated,
                    Identifier = u.Identifier
                });
            }
        }

        public void UpdateUser(UserData request)
        {
            var record = _context.Users
                                 .Include(x => x.Address)
                                 .First(x => x.Id == request.Id);
            record.FirstName = request.FirstName;
            record.LastName = request.LastName;

            if (request.DOB.HasValue)
                request.DOB = request.DOB.Value.ToUniversalTime();
            record.DOB = request.DOB;

            record.PhoneNumber = request.PhoneNumber;

            if (record.AddressId == null && request.Address != null)
            {
                var address = Mapper.Map<Address>(request.Address);
                record.Address = address;
            }
            else
            {
                record.Address = Mapper.Map(request.Address, record.Address);
            }

            record.AvatarS3Key = request.AvatarS3Key;
            record.AvatarFileName = request.AvatarFileName;
            _context.SaveChanges();
        }

        public UserData UpdateUserDetails(TeacherProfileSetupRequest request)
        {
            // User record
            var record = _context.Users
                                 .Include(x => x.Address)
                                 .First(x => x.Id == request.UserId);

            record.FirstName = request.FirstName;
            record.LastName = request.LastName;

            if (request.DateOfBirth.HasValue)
                request.DateOfBirth = request.DateOfBirth.Value.ToUniversalTime();
            record.DOB = request.DateOfBirth;

            record.PhoneNumber = request.PhoneNumber;

            if (record.AddressId == null && request.TeachingAddress != null)
            {
                var address = Mapper.Map<Address>(request.TeachingAddress);
                record.Address = address;
            } else
            {
                record.Address = Mapper.Map(request.TeachingAddress, record.Address);
            }

            _context.SaveChanges();
            return Mapper.Map<UserData>(record);
        }

        public UserData UpdateUserDetails(StudentUpdateRequest request)
        {
            var record = _context.Users
                                 .First(x => x.Id == request.UserId);

            record.FirstName = request.FirstName;
            record.LastName = request.LastName;

            record.DOB = null;
            if (request.DOB.HasValue)
                record.DOB = request.DOB.Value.ToUniversalTime();

            record.PhoneNumber = request.Phone;

            record.AvatarS3Key = request.AvatarS3Key;
            record.AvatarFileName = request.AvatarFileName;

            _context.SaveChanges();
            return Mapper.Map<UserData>(record);
        }

        public ActivateUserResponse Activate(Guid guid)
        {
            var record = _context.Users.FirstOrDefault(x => x.ActivationToken == guid);

            if (record != null)
            {
                // check token expiry
                if (DateTime.Compare(DateTime.UtcNow, record.ActivationTokenExpiry.Value) > 0)
                {
                    return new ActivateUserResponse { Result = Enum.TokenResponseEnum.TokenExpired };
                } 
                else
                {
                    record.Activated = true;
                    record.ActivationToken = null;
                    record.ActivationTokenExpiry = null;

                    _context.SaveChanges();

                    return new ActivateUserResponse
                    {
                        Result = Enum.TokenResponseEnum.NoError,
                        Id = record.Id,
                        FirstName = record.FirstName,
                        LastName = record.LastName,
                        UserRoleType = record.RoleType
                    };
                }
            }

            return new ActivateUserResponse { Result = Enum.TokenResponseEnum.TokenInvalid };
        }

        public UserData UpdateUserRole(UserRoleUpdateRequest request)
        {
            var record = _context.Users.First(x => x.Id == request.UserId);
            record.RoleType = request.UserRoleType;

            _context.SaveChanges();
            return new UserData()
            {
                Id = record.Id,
                RoleType = record.RoleType,
                ProfileStep = record.ProfileStep
            };
        }

        public UserData ResetPassword(string email)
        {
            var record = _context.Users.FirstOrDefault(x => x.Email == email && ! x.IsDeleted && ! x.Suspended && x.Activated);
            if (record != null)
            {
                record.ResetToken = Guid.NewGuid();
                record.ResetTokenExpiry = DateTime.UtcNow.AddDays(1);
                _context.SaveChanges();

                return new UserData()
                {
                    Id = record.Id,
                    FirstName = record.FirstName,
                    LastName = record.LastName,
                    Email = record.Email,
                    ResetToken = record.ResetToken,
                    ResetTokenExpiry = record.ResetTokenExpiry
                };
            }
            return null;
        }

        public string GetToken(long id)
        {
            return _context.Users
                           .Where(x => x.Id == id)
                           .Select(u => u.Password)
                           .FirstOrDefault();
        }

        public UserData GetUserByResetToken(Guid? token)
        {
            IQueryable<SAMii.Repository.Models.User> query = _context.Users.AsNoTracking().Where(x => x.ResetToken == token.Value);
            return GetUserDataSubset(query, UserDataSubset.ResetToken).FirstOrDefault();
        }

        public void ChangePassword(long id, string hashPassword)
        {
            var record = _context.Users.First(x => x.Id == id);
            record.Password = hashPassword;
            record.ResetToken = null;
            record.ResetTokenExpiry = null;
            _context.SaveChanges();
        }


        #region UserInvitation  
        public UserInvitationData CreateUserInvitation(UserInvitationData request)
        {
            var record = Mapper.Map<UserInvitation>(request);
            record.Token = Guid.NewGuid();
            record.TokenExpiry = DateTime.UtcNow.AddDays(1);    // invitation valid for 24h
            record.DateSent = DateTime.UtcNow;

            _context.UserInvitations.Add(record);
            _context.SaveChanges();

            return Mapper.Map<UserInvitationData>(record);
        }

        public UserInvitationData GetUserInvitationByToken(Guid? invitationToken)
        {
            var record = _context.UserInvitations.AsNoTracking().FirstOrDefault(x => x.Token == invitationToken.Value);
            return Mapper.Map<UserInvitationData>(record);
        }

        public IList<UserInvitationData> GetUserInvitationByFromId(long userId)
        {
            var record = _context.UserInvitations.AsNoTracking().Where(x => x.FromUserId == userId).ToList();
            return Mapper.Map<IList<UserInvitationData>>(record);
        }

        public UserInvitationData UpdateUserInvitation(Guid? invitationToken)
        {
            var invitation = _context.UserInvitations.FirstOrDefault(x => x.Token == invitationToken.Value);
            if (invitation != null)
            {
                invitation.Accepted = true;
                invitation.DateAccepted = DateTime.UtcNow;
                invitation.TokenExpiry = null;

                _context.SaveChanges();
            }

            return Mapper.Map<UserInvitationData>(invitation);
        }
        #endregion

    }
}
