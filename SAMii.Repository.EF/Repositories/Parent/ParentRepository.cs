﻿using SAMii.DTO.Parents;
using SAMii.Repository.EF.Contexts;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using SAMii.Repository.Models;

namespace SAMii.Repository.EF.Repositories
{
    public class ParentRepository : IParentRepository
    {
        private readonly IParentContext _context;
        public ParentRepository(IParentContext context)
        {
            _context = context;
        }

        public ParentData GetParent(long userId)
        {
            var record = _context.Parents
                                 .Include(x => x.User)
                                 .Include("User.Address")
                                 .AsNoTracking()
                                 .First(x => x.UserId == userId);

            return Mapper.Map<ParentData>(record);
        }

        public ParentData GetParentDashboard(long userId)
        {
            var record = _context.Parents
                                 .Include(x => x.User)
                                 .Include(x => x.Students)
                                 .Include("Students.User")
                                 .AsNoTracking()
                                 .First(x => x.UserId == userId);
            return Mapper.Map<ParentData>(record);
        }

        public void InitialiseParent(long userId)
        {
            _context.Parents.Add(new Parent
            {
                UserId = userId
            });
            _context.SaveChanges();
        }

        public ParentData UpdateParent(ParentData request)
        {
            var record = _context.Parents
                                 .Include(x => x.User)
                                 .First(x => x.Id == request.Id);

            record = Mapper.Map(request, record);

            if (record.User.ProfileStep < request.User.ProfileStep)
                record.User.ProfileStep = request.User.ProfileStep;

            _context.SaveChanges();

            return Mapper.Map<ParentData>(record);
        }
    }
}
