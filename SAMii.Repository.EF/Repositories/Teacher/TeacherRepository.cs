﻿using SAMii.DTO;
using SAMii.Repository.EF.Contexts;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;
using SAMii.Repository.Models;
using SAMii.DTO.Teachers;

namespace SAMii.Repository.EF.Repositories {
    public class TeacherRepository: ITeacherRepository
    {
        private readonly ITeacherContext _context;

        public TeacherRepository(ITeacherContext context)
        {
            _context = context;
        }

        public void InitialiseTeacher(UserData user)
        {
            var teacher = new Teacher
            {
                UserId = user.Id,
                Public = false,
                Introduction = "<p>Thank you for visiting. To get started have a look at my profile. View my videos, pictures and testimonials. The good thing about SAMii is you can book directly into my available lesson time slots based on the day and time that work well for you. Another cool feature is that secure payments are made easy for both of us. Let's get started by clicking the \"Enrol\" button now and I look forward to inspiring the next generation of musicians. </p>",
                TeachingMethod = "<p>SAMii provides educators with a platform where inspiration and teaching can happen. Find out more about me through the information I've provide on my profile.</p>",
                TeachingContent = "<p>SAMii allows me to teach using my preferred methods of teaching, which may include one on one lessons, group lessons, online lessons, and using the pedagogy practice that I believe is important for my students.</p>",
                IntroductionMediaS3Key = "IntroductionMedia/" + user.Identifier.ToString(),
                IntroductionMediaFileName = "DefaultTeacherIntroduction.png",
            };
            _context.Teachers.Add(teacher);
            _context.Videos.Add(new Video 
            { 
                Teacher = teacher,
                Url = AppConstants.DefaultTeacherVideoUrl
            });
            _context.SaveChanges();
        }

        public TeacherSearchResponse GetTeacherProfiles(TeacherProfileSearchRequest request)
        {
            bool evaluateInstruments = (request.InstrumentId.HasValue);
            bool evaluateSuburb = (!String.IsNullOrEmpty(request.SuburbSearch));
            bool evaluateLessonDays = (request.Days != null);
            bool evaluateLessonTime = (request.StartTime.HasValue && request.EndTime.HasValue);

            if (request.Days == null)
                request.Days = new List<DayOfWeek>();

            var startTime = DateTime.Now.TimeOfDay;
            var endTime = DateTime.Now.TimeOfDay;
            var utcOffset = TimeSpan.Zero;
            if (evaluateLessonTime)
            {
                startTime = request.StartTime.Value.TimeOfDay;
                endTime = request.EndTime.Value.TimeOfDay;
                utcOffset = TimeZoneInfo.Local.GetUtcOffset(request.StartTime.Value);
            }

            var records = _context.Teachers
                                   .Include(x => x.User)
                                   .Include("User.Address")
                                   .Include(x => x.Lessons)
                                   .Include("Lessons.Dates")
                                   .Include("Lessons.TeacherLocation")
                                   .Include("Lessons.TeacherLocation.Address")
                                   .AsNoTracking()
                                   .Where(x => x.Public &&
                                              (!evaluateInstruments || x.Lessons.FirstOrDefault(l => l.Status == Enum.LessonStatus.Published && l.InstrumentId == request.InstrumentId) != null) &&
                                              (!evaluateSuburb || (x.User.Address.Suburb.ToLower().Contains(request.SuburbSearch.ToLower())) || 
                                                x.Lessons.FirstOrDefault(l => l.Status == Enum.LessonStatus.Published && l.TeacherLocation.Address.Suburb.ToLower().Contains(request.SuburbSearch.ToLower())) != null) &&
                                              (!evaluateLessonDays || x.Lessons.FirstOrDefault(l => l.Status == Enum.LessonStatus.Published && l.Dates.FirstOrDefault(d => request.Days.Contains(d.Day)) != null) != null) &&
                                              (!evaluateLessonTime || x.Lessons.FirstOrDefault(l => l.Status == Enum.LessonStatus.Published && l.Dates.FirstOrDefault(
                                                d => ((d.StartTime.Hour*60 + d.StartTime.Minute + utcOffset.Hours*60 + utcOffset.Minutes)%(24*60) >= startTime.Hours*60 + startTime.Minutes) &&
                                                     ((endTime.Hours == 0 && endTime.Minutes == 0) || 
                                                        (d.StartTime.Hour*60 + d.StartTime.Minute + l.DurationInMin + utcOffset.Hours * 60 + utcOffset.Minutes)%(24*60) <= endTime.Hours*60 + endTime.Minutes)
                                              ) != null) != null)
                                         )
                                   .OrderBy(x => x.User.FirstName)
                                   .OrderBy(x => x.User.LastName)
                                   .ToList();
            var totalCount = records.Count();

            records = records.Skip(request.Pagination.PageSize * (request.Pagination.PageNumber - 1))
                             .Take(request.Pagination.PageSize)
                             .ToList();

            var teachers = Mapper.Map<IList<TeacherData>>(records);
            return new TeacherSearchResponse
            {
                Teachers = teachers,
                TotalCount = totalCount
            };
        }

        public TeacherData GetTeacher(long userId)
        {
            var record = _context.Teachers
                                 .Include(x => x.Educations)
                                 .Include(x => x.Videos)
                                 .Include(x => x.Clearances)
                                 .Include(x => x.Testimonials)
                                 .Include(x => x.User)
                                 .Include("User.Address")
                                 .First(x => x.UserId == userId);

            var instruments = _context.InstrumentTeachers
                                      .AsNoTracking()
                                      .Include(x => x.Instrument)
                                      .Where(x => x.TeacherId == record.Id)
                                      .ToList();

            var teacherLocations = _context.TeacherLocations
                                           .AsNoTracking()
                                           .Include(x => x.Address)
                                           .Where(x => x.TeacherId == record.Id)
                                           .ToList();


            var result = Mapper.Map<TeacherData>(record);
            result.Instruments = instruments.Select(x => new InstrumentData
            {
                Id = x.Instrument.Id,
                Name = x.Instrument.Name
            }).ToList();

            result.TeacherLocations = Mapper.Map<IList<TeacherLocationData>>(teacherLocations);

            return result;
        }

        public TeacherProfileSetupPageModel GetTeacherProfileSetupPage(long userId)
        {
            var record = _context.Teachers
                                 .First(x => x.UserId == userId);

            var billing = _context.TeacherBilling
                                  .FirstOrDefault(x => x.TeacherBillingId == record.Id);

            var instruments = _context.InstrumentTeachers
                                      .Include(x => x.Instrument)
                                      .Where(x => x.TeacherId == record.Id)
                                      .Select(x => x.Instrument)
                                      .ToList();

            var locations = _context.TeacherLocations
                                    .Include(x => x.Address)
                                    .Where(x => x.TeacherId == record.Id)
                                    .ToList();

            return new TeacherProfileSetupPageModel
            {
                TeacherId = record.Id,
                Setup = new TeacherProfileSetupResponse
                {
                    Instruments = Mapper.Map<IList<InstrumentData>>(instruments),
                    TeacherLocations = Mapper.Map<IList<TeacherLocationData>>(locations),
                    TeachingAddressAlsoResidential = record.TeachingAddressAlsoResidential
                },
                Billing = Mapper.Map<TeacherBillingData>(billing)
            };
        }

        public Tuple<IList<InstrumentData>, IList<TeacherLocationData>> TeacherProfileSetup(TeacherProfileSetupRequest request)
        {
            var record = _context.Teachers
                                 .Include(x => x.User)
                                 .First(x => x.UserId == request.UserId);

            var instruments = _context.InstrumentTeachers
                                      .Include(x => x.Instrument)
                                      .Where(x => x.TeacherId == record.Id)
                                      .ToList();

            var locations = _context.TeacherLocations
                                    .Include(x => x.Address)
                                    .Where(x => x.TeacherId == record.Id)
                                    .ToList();

            // Teacher record
            record.TeachingAddressAlsoResidential = request.TeachingAddressAlsoResidential;

            // Instruments
            var selectedInstrumentIds = instruments.Select(x => x.InstrumentId).ToList();
            foreach (var instrument in request.Instruments)
            {
                if (!selectedInstrumentIds.Contains(instrument.Id))
                {
                    _context.InstrumentTeachers.Add(new InstrumentTeacher
                    {
                        Teacher = record,
                        InstrumentId = instrument.Id
                    });
                }
                else
                {
                    selectedInstrumentIds.Remove(instrument.Id);
                }
            }

            if (selectedInstrumentIds.Count > 0)
                _context.InstrumentTeachers.RemoveRange(_context.InstrumentTeachers.Where(x => selectedInstrumentIds.Contains(x.InstrumentId)).ToList());

            // Teacher Locations
            var selectedLocations = locations.Select(x => x.Id).ToList();
            foreach (var location in request.TeacherLocations)
            {
                if (location.Id == 0)
                {
                    _context.TeacherLocations.Add(new TeacherLocation
                    {
                        Name = location.Name,
                        Address = Mapper.Map<Address>(location.Address),
                        TeacherId = record.Id
                    });
                }
                else
                {
                    var loc = locations.First(x => x.Id == location.Id);
                    loc.Name = location.Name;
                    loc.Address = Mapper.Map(location.Address, loc.Address);
                    selectedLocations.Remove(location.Id);
                }
            }
            if (selectedLocations.Count > 0)
            {
                _context.TeacherLocations.RemoveRange(_context.TeacherLocations.Where(x => selectedLocations.Contains(x.Id)).ToList());
                _context.Addresses.RemoveRange(_context.TeacherLocations
                                       .Include(x => x.Address)
                                       .Where(x => selectedLocations.Contains(x.Id))
                                       .Select(x => x.Address)
                                       .ToList());
            }

            // update profile step
            if (record.User.ProfileStep < 1)
            {
                var userRecord = _context.Users.First(x => x.Id == record.UserId);
                userRecord.ProfileStep = 1;
            }

            _context.SaveChanges();
            return Tuple.Create(Mapper.Map<IList<InstrumentData>>(
                                    _context.InstrumentTeachers
                                            .Include(x => x.Instrument)
                                            .AsNoTracking()
                                            .Where(x => x.TeacherId == record.Id)
                                            .Select(x => x.Instrument)
                                            .ToList()
                               ), 
                                Mapper.Map<IList<TeacherLocationData>>(
                                    _context.TeacherLocations
                                            .AsNoTracking()
                                            .Include(x => x.Address)
                                            .Where(x => x.TeacherId == record.Id)
                                            .ToList()
                               ));
        }

        public TeacherBillingData GetTeacherBilling(long teacherId)
        {
            var record = _context.TeacherBilling
                                .FirstOrDefault(x => x.TeacherBillingId == teacherId);

            if (record != null)
                return Mapper.Map<TeacherBillingData>(record);

            return null;
        }

        public TeacherData GetTeacherData(long teacherId)
        {
            var teacher = _context.Teachers
                               .Include(x => x.User)
                               .Include("User.Address")
                               .First(x => x.Id == teacherId);

            return Mapper.Map<TeacherData>(teacher);
        }

        public void UpdateTeacherBilling(TeacherBillingData request)
        {
            var record = _context.TeacherBilling
                                 .FirstOrDefault(x => x.TeacherBillingId == request.TeacherBillingId);

            var teacher = _context.Teachers
                                  .Include(x => x.User)
                                  .First(x => x.Id == request.TeacherBillingId);

            if (record == null)
            {
                _context.TeacherBilling.Add(Mapper.Map<TeacherBilling>(request));
            } else
            {
                record = Mapper.Map(request, record);
            }

            // update profile step
            if (teacher.User.ProfileStep < AppConstants.TeacherProfileComplete)
            {
                var userRecord = _context.Users.First(x => x.Id == teacher.UserId);
                userRecord.ProfileStep = 2;
            }

            _context.SaveChanges();           
        }

        public bool UpdateProfile(TeacherProfileUpdateRequest request)
        {
            var record = _context.Teachers
                                 .Include(x => x.Educations)
                                 .Include(x => x.Videos)
                                 .Include(x => x.Clearances)
                                 .Include(x => x.Testimonials)
                                 .First(x => x.Id == request.TeacherProfileData.Id);

            record = Mapper.Map(request.TeacherProfileData, record);

            var instruments = _context.InstrumentTeachers
                                      .Include(x => x.Instrument)
                                      .Where(x => x.TeacherId == record.Id)
                                      .ToList();

            var locations = _context.TeacherLocations
                                    .Include(x => x.Address)
                                    .Where(x => x.TeacherId == record.Id)
                                    .ToList();

            // Educations
            var oldEducations = record.Educations.ToList();
            foreach (var edu in request.TeacherProfileData.Educations) {
                if (edu.Id == 0)
                {
                    record.Educations.Add(Mapper.Map<Education>(edu));
                } else
                {
                    var eduRecord = record.Educations.First(x => x.Id == edu.Id);
                    eduRecord = Mapper.Map(edu, eduRecord);
                    oldEducations.Remove(oldEducations.First(x => x.Id == edu.Id));
                }
            }

            if (oldEducations.Count > 0)
                _context.Educations.RemoveRange(oldEducations);


            // Videos
            var oldVideos = record.Videos.ToList();
            foreach (var video in request.TeacherProfileData.Videos)
            {
                if (video.Id == 0)
                {
                    record.Videos.Add(Mapper.Map<Video>(video));
                } else
                {
                    var videoRecord = record.Videos.First(x => x.Id == video.Id);
                    videoRecord = Mapper.Map(video, videoRecord);
                    oldVideos.Remove(oldVideos.First(x => x.Id == video.Id));
                }
            }

            if (oldVideos.Count > 0)
                _context.Videos.RemoveRange(oldVideos);


            // Clearances
            var oldClearances = record.Clearances.ToList();
            foreach (var clearance in request.TeacherProfileData.Clearances)
            {
                if (clearance.Id == 0)
                {
                    record.Clearances.Add(Mapper.Map<Clearance>(clearance));
                } else
                {
                    var clearanceRecord = record.Clearances.First(x => x.Id == clearance.Id);
                    clearanceRecord = Mapper.Map(clearance, clearanceRecord);
                    oldClearances.Remove(oldClearances.First(x => x.Id == clearance.Id));
                }
            }

            if (oldClearances.Count > 0)
                _context.Clearances.RemoveRange(oldClearances);

            // Testimonials
            var oldTestimonials = record.Testimonials.ToList();
            foreach (var testimonial in request.TeacherProfileData.Testimonials)
            {
                if (testimonial.Id == 0)
                {
                    record.Testimonials.Add(Mapper.Map<Testimonial>(testimonial));
                }
                else
                {
                    var testimonialRecord = record.Testimonials.First(x => x.Id == testimonial.Id);
                    testimonialRecord = Mapper.Map(testimonial, testimonialRecord);
                    oldTestimonials.Remove(oldTestimonials.First(x => x.Id == testimonial.Id));
                }
            }

            if (oldTestimonials.Count > 0)
                _context.Testimonials.RemoveRange(oldTestimonials);

            // Instruments
            var selectedInstrumentIds = instruments.Select(x => x.InstrumentId).ToList();
            foreach (var instrument in request.TeacherProfileData.Instruments)
            {
                if (!selectedInstrumentIds.Contains(instrument.Id))
                {
                    _context.InstrumentTeachers.Add(new InstrumentTeacher
                    {
                        Teacher = record,
                        InstrumentId = instrument.Id
                    });
                }
                else
                {
                    selectedInstrumentIds.Remove(instrument.Id);
                }
            }

            if (selectedInstrumentIds.Count > 0)
                _context.InstrumentTeachers.RemoveRange(_context.InstrumentTeachers.Where(x => selectedInstrumentIds.Contains(x.InstrumentId)).ToList());

            // Teacher Locations
            var selectedLocations = locations.Select(x => x.Id).ToList();
            foreach (var location in request.TeacherProfileData.TeacherLocations)
            {
                if (location.Id == 0)
                {
                    _context.TeacherLocations.Add(new TeacherLocation
                    {
                        Name = location.Name,
                        Address = Mapper.Map<Address>(location.Address),
                        TeacherId = record.Id
                    });
                } else
                {
                    var loc = locations.First(x => x.Id == location.Id);
                    loc.Name = location.Name;
                    loc.Address = Mapper.Map(location.Address, loc.Address);
                    selectedLocations.Remove(location.Id);
                }
            }
            if (selectedLocations.Count > 0)
            {
                _context.TeacherLocations.RemoveRange(_context.TeacherLocations.Where(x => selectedLocations.Contains(x.Id)).ToList());
                _context.Addresses.RemoveRange(_context.TeacherLocations
                                                       .Include(x => x.Address)
                                                       .Where(x => selectedLocations.Contains(x.Id))
                                                       .Select(x => x.Address)
                                                       .ToList());
            }

            _context.SaveChanges();
            return true;
        }

        public IList<ClearanceData> AddNewClearances(long teacherProfileId, IList<ClearanceData> request)
        {
            var records = _context.Clearances
                                  .Where(x => x.TeacherProfileId == teacherProfileId)
                                  .ToList();
            var guids = records.Select(x => x.GUID).ToList();

            foreach (var clearance in request)
            {
                if (clearance.Id == 0)
                {
                    Guid guid = Guid.Empty;
                    var isGuidUnique = false;
                    while (!isGuidUnique)
                    {
                        guid = Guid.NewGuid();
                        if (!guids.Contains(guid))
                            isGuidUnique = true;
                    }

                    var newClearance = new Clearance
                    {
                        TeacherProfileId = teacherProfileId,
                        FileName = clearance.FileName,
                        ClearanceS3Key = null,
                        GUID = guid
                    };
                    records.Add(newClearance);
                    clearance.GUID = guid;
                }
            }
            _context.SaveChanges();

            return request;
        }

        public TeacherLocationData AddTeacherLocation(TeacherLocationData request)
        {
            var loc = Mapper.Map<TeacherLocation>(request);
            _context.TeacherLocations.Add(loc);
            _context.SaveChanges();
            return Mapper.Map<TeacherLocationData>(loc);
        }

        public IList<EducationData> GetEducations(long teacherId)
        {
            var records = _context.Educations.AsNoTracking().Where(x => x.TeacherProfileId == teacherId).ToList();
            return Mapper.Map<IList<EducationData>>(records);
        }

        public IList<TestimonialData> GetTestimonials(long teacherId)
        {
            var records = _context.Testimonials.AsNoTracking().Where(x => x.TeacherId == teacherId).ToList();
            return Mapper.Map<IList<TestimonialData>>(records);
        }

        public IList<TeacherLocationData> GetTeacherLocations(long teacherId)
        {
            var records = _context.TeacherLocations
                                  .AsNoTracking()
                                  .Include(x => x.Address)
                                  .Where(x => x.TeacherId == teacherId).ToList();
            return Mapper.Map<IList<TeacherLocationData>>(records);
        }

        #region Teacher Payout
        public IList<TeacherBillingData> GetTeacherBillings()
        {
            var records = _context.TeacherBilling.AsNoTracking().ToList();
            return Mapper.Map<IList<TeacherBillingData>>(records);
        }
        public DateTime? GetLastFortnightDate()
        {
            var record = _context.AppSettings.FirstOrDefault();
            return record != null ? record.LastFornight : null;
        }
        public void UpdateFortnightDate()
        {
            var record = _context.AppSettings.AsNoTracking().FirstOrDefault();
            if (record != null)
                record.LastFornight = DateTime.UtcNow;
            else
            {
                record = _context.AppSettings.Add(new Models.Common.AppSetting
                {
                    LastFornight = DateTime.UtcNow
                });
            }
            _context.SaveChanges();
        }
        public long AddTeacherPayout(TeacherPayoutData data)
        {
            TeacherPayout record = new TeacherPayout()
            {
                TeacherId = data.TeacherId,
                InvoiceId = data.InvoiceId,
                InvoiceDate = data.InvoiceDate,
                InvoiceRef = data.InvoiceRef,
                AmountPaid = data.AmountPaid
            };
            _context.TeacherPayout.Add(record);
            _context.SaveChanges();

            return record.Id;
        }
        #endregion
    }
}
