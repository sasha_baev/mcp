﻿using SAMii.DTO;
using SAMii.Repository.EF.Contexts;
using SAMii.Repository.Interface;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using SAMii.Repository.Models;
using SAMii.Enum;
using SAMii.DTO.Lessons;

namespace SAMii.Repository.EF
{
    public class InstrumentRepository : IInstrumentRepository
    {
        readonly IInstrumentContext _context;
        public InstrumentRepository(IInstrumentContext context)
        {
            _context = context;
        }

        public IList<InstrumentData> GetInstruments()
        {
            return _context.Instruments
                           .AsNoTracking()
                           .Select(x => new InstrumentData
                           {
                               Id = x.Id,
                               Name = x.Name
                           })
                           .ToList();
        }

        /*
        public IList<InstrumentData> GetInstruments(long userId)
        {
            return _context.InstrumentUsers
                           .Include(x => x.Instrument)
                           .AsNoTracking()
                           .Where(x => x.UserId == userId)
                           .Select(x => new InstrumentData
                           {
                               Id = x.Instrument.Id,
                               Name = x.Instrument.Name
                           })
                           .ToList();
        }
        */

        public IList<InstrumentData> AddInstruments(IList<InstrumentRequest> request)
        {
            var instruments = new List<Instrument>();
            foreach (var req in request)
            {
                instruments.Add(new Instrument
                {
                    Name = req.Name,
                    UserId = req.UserId
                });
            }
            _context.Instruments.AddRange(instruments);
            _context.SaveChanges();

            return instruments.Select(x => new InstrumentData
            {
                Id = x.Id,
                Name = x.Name
            })
            .ToList();
        }
        /*
        public void UpdateUserInstruments(long userId, IList<InstrumentData> instruments)
        {
            var records = _context.InstrumentUsers
                                  .Where(x => x.UserId == userId)
                                  .ToList();

            var currentInstrumentIds = records.Select(x => x.InstrumentId).ToList();


            foreach (var instrument in instruments)
            {
                if (!currentInstrumentIds.Contains(instrument.Id))
                {
                    _context.InstrumentUsers.Add(new InstrumentTeacher 
                    {
                        UserId = userId,
                        InstrumentId = instrument.Id
                    });
                } else
                {
                    currentInstrumentIds.Remove(instrument.Id);
                }
            }

            if (currentInstrumentIds.Count > 0)
                _context.InstrumentUsers.RemoveRange(records.Where(x => currentInstrumentIds.Contains(x.InstrumentId)).ToList());

            _context.SaveChanges();
        }
        */
    }
}
