﻿using SAMii.DTO;
using SAMii.Repository.EF.Contexts;
using SAMii.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AutoMapper;
using SAMii.Repository.Models;
using SAMii.DTO.Bookings;
using SAMii.DTO.Students;
using SAMii.Enum;

namespace SAMii.Repository.EF.Repositories {
    public class BookingRepository : IBookingRepository
    {
        private readonly IBookingContext _context;

        public BookingRepository(IBookingContext context)
        {
            _context = context;
        }

        public long CreateBooking(BookingRequest request, LessonData lesson, StudentData student)
        {
            // verify there are slots available
            var studentLimit = 1;
            if (lesson.IsGroupLesson)
                studentLimit = lesson.StudentPerGroup;

            var bookingDate = request.StartDate.ToUniversalTime();

            var lessonBookings = _context.Bookings
                                         .AsNoTracking()
                                         .Where(x => x.LessonSlotId == request.LessonSlotId && x.StartDate == bookingDate)
                                         .Count();

            if (lessonBookings >= studentLimit)
                return 0;

            Booking booking = new Booking
            {
                StudentId = student.Id,
                LessonSlotId = request.LessonSlotId,
                StartDate = request.StartDate.ToUniversalTime(),
                CreatedDate = DateTime.UtcNow,
                Status = BookingStatus.Active,
                PaymentType = request.PaymentType
            };

            _context.Bookings.Add(booking);
            _context.SaveChanges();

            return booking.Id;
        }

        // WARNING - should not be used outside of createbooking call. Needs to be refactored to a DB transaction.
        public void DeleteBooking(long bookingId)
        {
            _context.Bookings.Remove(_context.Bookings.First(x => x.Id == bookingId));
            _context.SaveChanges();
        }

        public bool AddBookingPayment(BookingPaymentData data)
        {
            BookingPayment record = AutoMapper.Mapper.Map<BookingPayment>(data);
            _context.BookingPayments.Add(record);

            if (record != null)
            {
                Booking booking = _context.Bookings
                                           .First(x => x.Id == data.BookingId);

                booking.TotalCount = booking.TotalCount + record.Quantity;
                booking.InCreditCount = booking.InCreditCount + record.Quantity;
                _context.SaveChanges();

                return true;
            }


            return false;
        }

        public bool AddBookingRefundPayment(BookingRefundData data)
        {
            BookingRefund record = AutoMapper.Mapper.Map<BookingRefund>(data);

            _context.BookingRefunds.Add(record);
            _context.SaveChanges();

            return record != null;
        }

        public BookingData GetBookingById(long bookingId)
        {
            var record = _context.Bookings
                                 .Include(x => x.LessonSlot)
                                 .Include("LessonSlot.LessonDate")
                                 .Include("LessonSlot.LessonDate.Lesson")
                                 .Include(x => x.Student)
                                 .Include("Student.User")
                                 .Include("LessonSlot.LessonDate.Lesson.Teacher")
                                 .Include("LessonSlot.LessonDate.Lesson.Teacher.User")
                                 .Include("LessonSlot.LessonDate.Lesson.Instrument")
                                 .AsNoTracking()
                                 .First(x => x.Id == bookingId);

            return Mapper.Map<BookingData>(record);
        }

        public IList<BookingData> GetBookingsByStudentId(long studentId)
        {
            var list = _context.Bookings
                               .Include(x => x.LessonSlot)
                               .Include("LessonSlot.LessonDate")
                               .Include("LessonSlot.LessonDate.Lesson")
                               .Include("LessonSlot.LessonDate.Lesson.LessonResources")
                               .Include("LessonSlot.lessonDate.Lesson.Instrument")
                               .Include("LessonSlot.lessonDate.Lesson.Teacher")
                               .Include("LessonSlot.lessonDate.Lesson.Teacher.User")
                               .Include(x => x.Cancellations)
                               .AsNoTracking()
                               .Where(x => x.StudentId == studentId)
                               .ToList();

            return Mapper.Map<IList<BookingData>>(list);
        }

        public IList<BookingData> GetBookingsByTeacherId(long teacherId)
        {
            var list = _context.Bookings
                               .Include(x => x.LessonSlot)
                               .Include("LessonSlot.LessonDate")
                               .Include("LessonSlot.LessonDate.Lesson")
                               .Include("LessonSlot.LessonDate.Lesson.LessonResources")
                               .Include("LessonSlot.lessonDate.Lesson.Instrument")
                               .Include("LessonSlot.lessonDate.Lesson.Teacher")
                               .Include("LessonSlot.lessonDate.Lesson.Teacher.User")
                               .Include(x => x.Cancellations)
                               .AsNoTracking()
                               .Where(x => x.LessonSlot.LessonDate.Lesson.TeacherId == teacherId)
                               .ToList();

            return Mapper.Map<IList<BookingData>>(list);
        }

        public IList<BookingData> GetBookingsByLessonSlots(IList<long> slotIds)
        {
            var list = _context.Bookings
                               .Include(x => x.Student)
                               .Include("Student.User")
                               .AsNoTracking()
                               .Where(x => slotIds.Contains(x.LessonSlotId));

            return Mapper.Map<IList<BookingData>>(list);
        }

        public IList<BookingData> GetAllActiveBookings()
        {
            var list = _context.Bookings
                               .Include(x => x.Student)
                               .Include(x => x.LessonSlot)
                               .Include("Student.User")
                               .Include("LessonSlot.LessonDate")
                               .Include("LessonSlot.LessonDate.Lesson")
                               .AsNoTracking()
                               .Where(x => !x.Cancelled);

            return Mapper.Map<IList<BookingData>>(list);
        }
        public void SaveStudentAttendance(IList<BookingAttendanceData> list)
        {
            //Save Attendace
            IList<BookingAttendance> records = AutoMapper.Mapper.Map<IList<BookingAttendance>>(list);
            _context.BookingAttendances.AddRange(records);
            _context.SaveChanges();

            //Update booking data
            IList<long> bookingIds = records.Select(x => x.BookingId).ToList();

            var bookings = _context.Bookings
                                    .Where(x => bookingIds.Contains(x.Id));

            foreach(Booking booking in bookings)
            {
                booking.AttendedCount += 1;
                booking.InCreditCount -= 1;
            }
            _context.SaveChanges();
        }

        public IList<BookingCancellationData> GetBookingCancellationDatas(long bookingId)
        {
            var records = _context.BookingCancellations
                                   .AsNoTracking()
                                   .Where(x => x.BookingId == bookingId && x.CancelType == BookingCancelType.Single);

            if (records == null)
                return null;

            return Mapper.Map<IList<BookingCancellationData>>(records);
        }
        public bool CancelBooking(BookingCancelRequest request)
        {
            var booking = _context.Bookings
                                  .First(x => x.Id == request.BookingId);
            BookingCancellation cancellationToAdd = null;

            if (booking.TotalCount > 1)
            {
                if (DateTime.Compare(request.CancelDate.ToUniversalTime(), booking.StartDate) == 0 && (
                    (request.CancelType == BookingCancelType.Single && booking.TotalCount == 1) ||
                        (request.CancelType == BookingCancelType.All)
                    ))
                {
                    booking.Status = BookingStatus.Cancelled;
                    booking.Cancelled = true;
                    
                } else
                {
                    cancellationToAdd = new BookingCancellation
                    {
                        BookingId = booking.Id,
                        CancelledDate = request.CancelDate.ToUniversalTime(),
                        CancelType = request.CancelType
                    };
                    _context.BookingCancellations.Add(cancellationToAdd);

                    if (request.CancelType == BookingCancelType.All)
                    {
                        booking.Cancelled = true;
                        booking.CancelledEndDate = request.CancelDate.ToUniversalTime();
                    }
                }
            }
            else
            {
                booking.Status = BookingStatus.Cancelled;
                booking.Cancelled = true;
            }

            // check if booking is cancelled via filled BookingCancel table
            if (!booking.Cancelled)
            {
                List<DateTime> bookingDates = new List<DateTime>();
                var date = booking.StartDate;
                for(var i = 0; i < booking.TotalCount; i++)
                {
                    bookingDates.Add(date);
                    date = date.AddDays(7);
                }

                var cancellations = _context.BookingCancellations
                                            .Where(x => x.BookingId == booking.Id)
                                            .ToList();

                List<BookingCancellation> updatedCancellations = new List<BookingCancellation>();
                foreach(var c in cancellations)
                {
                    updatedCancellations.Add(c);
                }
                updatedCancellations.Add(cancellationToAdd);

                foreach (var cancellation in updatedCancellations)
                {
                    if (cancellation.CancelType == BookingCancelType.Single)
                    {
                        bookingDates.Remove(bookingDates.First(x => DateTime.Compare(x, cancellation.CancelledDate) == 0));
                    } else if (cancellation.CancelType == BookingCancelType.All)
                    {
                        var datesToRemove = bookingDates.Where(x => DateTime.Compare(x, cancellation.CancelledDate) >= 0).ToList();
                        foreach (var d in datesToRemove)
                        {
                            bookingDates.Remove(d);
                        }
                    }
                }

                if (bookingDates.Count == 0)
                {
                    booking.Status = BookingStatus.Cancelled;
                    booking.Cancelled = true;
                    booking.CancelledEndDate = request.CancelDate.ToUniversalTime();
                }
            }

            _context.SaveChanges();

            return true;
        }


        public IList<BookingAttendanceData> GetUnpaidBookingAttendance()
        {
            var list = _context.BookingAttendances
                              .Include(x => x.Booking)
                              .Include("Booking.LessonSlot")
                              .Include("Booking.LessonSlot.LessonDate")
                              .Include("Booking.LessonSlot.LessonDate.Lesson")
                              .AsNoTracking()
                              .Where(x => !x.TeacherPaid);

            return Mapper.Map<IList<BookingAttendanceData>>(list);
        }

        public BookingData GetBookingPaymentsByBkId(long bookingId)
        {
            var record = _context.Bookings
                                 .Include(x => x.LessonSlot)
                                 .Include("LessonSlot.LessonDate")
                                 .Include("LessonSlot.LessonDate.Lesson")
                                 .Include(x => x.Payments)
                                 .AsNoTracking()
                                 .First(x => x.Id == bookingId);

            return Mapper.Map<BookingData>(record);
        }
        public void MarkPaidBookingAttendance(List<BookingAttendanceData> list, long TeacherPayoutId)
        {
            var ids = list.Select(x => x.Id).ToList();
            List<BookingAttendance> records = _context.BookingAttendances.Where(x => ids.Contains(x.Id)).ToList();

            foreach(BookingAttendance record in records)
            {
                record.TeacherPaid = true;
                record.TeacherPayoutId = TeacherPayoutId;
            }

            _context.SaveChanges();
        }
    }
}
