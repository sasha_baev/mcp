﻿using SAMii.DTO;
using SAMii.Repository.EF.Contexts;
using SAMii.Repository.Interface;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using SAMii.Repository.Models;
using SAMii.Enum;
using SAMii.DTO.Lessons;
using SAMii.DTO.Bookings;

namespace SAMii.Repository.EF
{
    public class LessonRepository : ILessonRepository
    {
        readonly ILessonContext _context;
        public LessonRepository(ILessonContext context)
        {
            _context = context;
        }

        public IList<LessonData> GetAll(long userId)
        {
            var records = _context.Lessons
                                  .Include(x => x.Instrument)
                                  .Include(x => x.Teacher)
                                  .Include(x => x.TeacherLocation)
                                  .Include("TeacherLocation.Address")
                                  .Include(x => x.Dates)
                                  .Include("Dates.Cancellations")
                                  .Include("Dates.LessonSlots")
                                  .Include("Dates.LessonSlots.Bookings")
                                  .AsNoTracking()
                                  .Where(x => x.Teacher.UserId == userId)
                                  .ToList();

            var res = Mapper.Map<IList<LessonData>>(records);
            return res;
        }

        public IList<LessonData> GetAllPublished(long userId)
        {
            var records = _context.Lessons
                                  .Include(x => x.Instrument)
                                  .Include(x => x.Teacher)
                                  .Include(x => x.TeacherLocation)
                                  .Include("TeacherLocation.Address")
                                  .Include(x => x.Dates)
                                  .Include("Dates.Cancellations")
                                  .Include("Dates.LessonSlots")
                                  .Include("Dates.LessonSlots.Bookings")
                                  .Include("Dates.LessonSlots.Bookings.Cancellations")
                                  .AsNoTracking()
                                  .Where(x => x.Teacher.UserId == userId && x.Status == LessonStatus.Published)
                                  .ToList();

            var res = Mapper.Map<IList<LessonData>>(records);

            var lessonSlots = records.SelectMany(x => x.Dates)
                                     .SelectMany(x => x.LessonSlots)
                                     .ToList();

            foreach (var record in res)
            {
                foreach (var date in record.Dates)
                {
                    foreach(var slot in date.LessonSlots)
                    {
                        var dbSlot = lessonSlots.First(x => x.Id == slot.Id);
                        slot.Bookings = new List<BookingSummaryData>();
                        foreach (var booking in dbSlot.Bookings)
                        {
                            slot.Bookings.Add(new BookingSummaryData 
                            { 
                                LessonSlotId = dbSlot.Id,
                                StartDate = booking.StartDate,
                                Status = booking.Status,
                                Cancelled = booking.Cancelled,
                                TotalCount = booking.TotalCount,
                                Cancellations = Mapper.Map<IList<BookingCancellationData>>(booking.Cancellations)
                            });
                        }
                    }
                }
            }

            return res;
        }

        public LessonData GetById(long id)
        {
            var record = _context.Lessons
                                 .Include(x => x.Instrument)
                                 .Include(x => x.Dates)
                                 .Include("Dates.LessonSlots")
                                 .Include("Dates.LessonSlots.Bookings")
                                 .Include(x => x.TeacherLocation)
                                 .Include("TeacherLocation.Address")
                                 .Include(x => x.LessonResources)
                                 .AsNoTracking()
                                 .First(x => x.Id == id);

            var res = Mapper.Map<LessonData>(record);       
            return res;
        }

        public LessonData GetBySlotId(long lessonSlotId)
        {
            var lessonSlot = _context.LessonSlots
                                     .First(x => x.Id == lessonSlotId);

            var lessonDate = _context.LessonDates
                                     .First(x => x.Id == lessonSlot.LessonDateId);

            var record = _context.Lessons
                     .Include(x => x.Instrument)
                     .Include(x => x.Dates)
                     .Include("Dates.LessonSlots")
                     .Include("Dates.LessonSlots.Bookings")
                     .Include(x => x.TeacherLocation)
                     .Include("TeacherLocation.Address")
                     .Include(x => x.LessonResources)
                     .AsNoTracking()
                     .First(x => x.Id == lessonDate.LessonId);

            var res = Mapper.Map<LessonData>(record);
            return res;
        }

        public IList<LessonData> GetTeacherLessonByDateRange(long teacherId, DateTime startDate, DateTime endDate)
        {
            var startDateUTC = startDate.ToUniversalTime();
            var endDateUTC = endDate.ToUniversalTime();
            var records = _context.Lessons
                                  .Include(x => x.Instrument)
                                  .Include(x => x.Dates)
                                  .Include("Dates.LessonSlots")
                                  .Include("Dates.LessonSlots.Bookings")
                                  .Include(x => x.TeacherLocation)
                                  .Include("TeacherLocation.Address")
                                  .AsNoTracking()
                                  .Where(x => (x.TeacherId == teacherId && DateTime.Compare(x.StartDate, endDateUTC) <= 0) &&
                                              (x.Status == LessonStatus.Published))
                                  .OrderBy(x => x.StartDate)
                                  .ToList();


            var res = Mapper.Map<IList<LessonData>>(records);
            return res;
        }

        public LessonData AddLesson(LessonData request)
        {
            Lesson record = null;
            if (request.Id != 0)
            {
                record = _context.Lessons
                    .Include(x => x.Instrument)
                    .Include(x => x.TeacherLocation)
                    .Include(x => x.Dates)
                    .Include(x => x.LessonResources)
                    .First(x => x.Id == request.Id);

                var lessonDates = _context.LessonDates
                                          .Include(x => x.LessonSlots)
                                          .Where(x => x.LessonId == record.Id)
                                          .ToList();

                if (record.DurationInMin != request.DurationInMin)
                    _context.LessonDates.RemoveRange(_context.LessonDates.Where(x => x.LessonId == record.Id));

                record = Mapper.Map(request, record);
                record.StartDate = request.StartDate.ToUniversalTime(); 

                if (request.EndDate.HasValue)
                    record.EndDate = request.EndDate.Value.ToUniversalTime();


                IList<LessonDate> datesToDelete = new List<LessonDate>();
                if (request.Dates != null && request.Dates.Count > 0)
                {
                    foreach (var date in lessonDates)
                    {
                        var requestDate = request.Dates.FirstOrDefault(x => x.Day == date.Day);
                        if (requestDate != null)
                        {
                            date.StartTime = requestDate.StartTime.ToUniversalTime();
                            date.EndTime = requestDate.StartTime.ToUniversalTime().AddMinutes(request.DurationInMin);
                            foreach (var slot in date.LessonSlots)
                            {
                                slot.StartTime = date.StartTime;
                            }
                        }
                        else
                        {
                            datesToDelete.Add(date);
                        }
                    }
                    foreach (var date in request.Dates)
                    {
                        var currentDate = lessonDates.FirstOrDefault(x => x.Day == date.Day);
                        if (currentDate == null)
                        {
                            var newDate = new LessonDate
                            {
                                LessonId = record.Id,
                                StartTime = date.StartTime.ToUniversalTime(),
                                EndTime = date.StartTime.ToUniversalTime().AddMinutes(request.DurationInMin),
                                Day = date.Day
                            };
                            _context.LessonDates.Add(newDate);
                            LessonSlot slot = new LessonSlot
                            {
                                LessonDate = newDate,
                                StartTime = newDate.StartTime
                            };
                            _context.LessonSlots.Add(slot);
                        }
                    }
                } else
                {
                    datesToDelete = lessonDates;
                }

                if (datesToDelete.Count > 0)
                    _context.LessonDates.RemoveRange(datesToDelete);
            }
            else
            {
                record = Mapper.Map<Lesson>(request);
                record.LessonStep = 2;
                record.Status = LessonStatus.Published;
                record.Guid = Guid.NewGuid();

                record.StartDate = request.StartDate.ToUniversalTime();
                if (request.EndDate != null)
                    record.EndDate = request.EndDate.Value.ToUniversalTime();

                _context.Lessons.Add(record);

                // add dates
                foreach (var date in request.Dates)
                {
                    LessonDate mappedDate = Mapper.Map<LessonDate>(date);
                    mappedDate.StartTime = mappedDate.StartTime.ToUniversalTime();
                    mappedDate.EndTime = mappedDate.StartTime.ToUniversalTime().AddMinutes(request.DurationInMin);
                    mappedDate.Status = LessonDateStatus.Active;
                    _context.LessonDates.Add(mappedDate);

                    // add slot
                    LessonSlot slot = new LessonSlot
                    {
                        LessonDate = mappedDate,
                        StartTime = mappedDate.StartTime
                    };
                    _context.LessonSlots.Add(slot);
                }
            }

            _context.SaveChanges();

            return Mapper.Map<LessonData>(record);
        }

        public void DeleteLesson(long lessonId)
        {
            _context.Lessons.Remove(_context.Lessons.First(x => x.Id == lessonId));
            _context.SaveChanges();
        }

        public IList<LessonResourceData> UpdateLessonResources(LessonResourceUpdateRequest request)
        {
            var records = _context.LessonResources
                                  .Where(x => x.LessonId == request.LessonId)
                                  .ToList();

            var recordsToDelete = records;

            foreach (var resource in request.LessonResources) {
                if (resource.Id == 0)
                {
                    _context.LessonResources.Add(Mapper.Map<LessonResource>(resource));
                } else
                {
                    var record = records.First(x => x.Id == resource.Id);
                    record = Mapper.Map(resource, record);
                    recordsToDelete.Remove(recordsToDelete.First(x => x.Id == resource.Id));
                }
            }

            if (recordsToDelete.Count > 0)
                _context.LessonResources.RemoveRange(recordsToDelete);

            _context.SaveChanges();
            return Mapper.Map<IList<LessonResourceData>>(records);
        }

        public IList<LessonSlotData> GetLessonSlotsByDateRange(DateTime startDate, DateTime endDate)
        {
            //StartDate and EndDate should be in UTC
            var lessonData = _context.LessonSlots
                                     .AsNoTracking()
                                     .Include(x => x.LessonDate)
                                     .Include("LessonDate.Lesson")
                                     .Where(x => 
                                        // Date
                                        (DateTime.Compare(x.LessonDate.Lesson.StartDate, endDate) <= 0) &&
                                        (!x.LessonDate.Lesson.EndDate.HasValue || 
                                           (DateTime.Compare(x.LessonDate.Lesson.EndDate.Value, startDate) >= 0))
                                     )
                                     .ToList();

            return Mapper.Map<IList<LessonSlotData>>(lessonData);
        }

        public IList<UnavailabilityData> GetUnavailabilitiesByTeacherId(long id)
        {
            var records = _context.Unavailabilities
                                  .Include(x => x.Dates)
                                  .AsNoTracking()
                                  .Where(x => x.TeacherId == id)
                                  .ToList();

            return Mapper.Map<IList<UnavailabilityData>>(records);
        }

        public UnavailabilityData GetUnavailabilityById(long id)
        {
            var record = _context.Unavailabilities
                                 .Include(x => x.Teacher)
                                 .Include(x => x.Dates)
                                 .AsNoTracking()
                                 .First(x => x.Id == id);

            return Mapper.Map<UnavailabilityData>(record);
        }

        public IList<UnavailabilityData> GetUnavailabilitiesByDateRange(long teacherId, DateTime startDate, DateTime endDate)
        {
            var startDateUTC = startDate.ToUniversalTime();
            var endDateUTC = endDate.ToUniversalTime();
            var records = _context.Unavailabilities
                                  .Include(x => x.Dates)
                                  .AsNoTracking()
                                  .Where(x => (x.TeacherId == teacherId && DateTime.Compare(x.StartDate, endDateUTC) <= 0))
                                  .OrderBy(x => x.StartDate)
                                  .ToList();


            return Mapper.Map<IList<UnavailabilityData>>(records);
        }

        public Tuple<UnavailabilityData, IList<BookingCancelRequest>> SaveUnavailability(UnavailabilityData request)
        {
            Unavailability record = null;
            if (request.Id != 0)
            {
                record = _context.Unavailabilities
                                 .Include(x => x.Dates)
                                 .First(x => x.Id == request.Id);

                var unavailabilityDates = _context.UnavailabilityDates
                                                  .Where(x => x.UnavailabilityId == record.Id)
                                                  .ToList();

                record = Mapper.Map(request, record);
                record.StartDate = request.StartDate.ToUniversalTime();

                if (request.EndDate.HasValue)
                    record.EndDate = request.EndDate.Value.ToUniversalTime();


                IList<UnavailabilityDate> datesToDelete = new List<UnavailabilityDate>();
                if (request.Dates != null && request.Dates.Count > 0)
                {
                    foreach (var date in unavailabilityDates)
                    {
                        var requestDate = request.Dates.FirstOrDefault(x => x.Day == date.Day);
                        if (requestDate != null)
                        {
                            date.StartTime = requestDate.StartTime.ToUniversalTime();
                            date.EndTime = requestDate.StartTime.ToUniversalTime().AddMinutes(request.DurationInMin);
                        }
                        else
                        {
                            datesToDelete.Add(date);
                        }
                    }
                    foreach (var date in request.Dates)
                    {
                        var currentDate = unavailabilityDates.FirstOrDefault(x => x.Day == date.Day);
                        if (currentDate == null)
                        {
                            var newDate = new UnavailabilityDate
                            {
                                UnavailabilityId = record.Id,
                                StartTime = date.StartTime.ToUniversalTime(),
                                EndTime = date.StartTime.ToUniversalTime().AddMinutes(request.DurationInMin),
                                Day = date.Day
                            };
                            _context.UnavailabilityDates.Add(newDate);
                        }
                    }
                }
                else
                {
                    datesToDelete = unavailabilityDates;
                }

                if (datesToDelete.Count > 0)
                    _context.UnavailabilityDates.RemoveRange(datesToDelete);
            }
            else
            {
                record = Mapper.Map<Unavailability>(request);

                record.StartDate = request.StartDate.ToUniversalTime();
                if (request.EndDate != null)
                    record.EndDate = request.EndDate.Value.ToUniversalTime();

                _context.Unavailabilities.Add(record);

                // add dates
                foreach (var date in request.Dates)
                {
                    UnavailabilityDate mappedDate = Mapper.Map<UnavailabilityDate>(date);
                    mappedDate.StartTime = mappedDate.StartTime.ToUniversalTime();
                    mappedDate.EndTime = mappedDate.StartTime.ToUniversalTime().AddMinutes(request.DurationInMin);
                    _context.UnavailabilityDates.Add(mappedDate);
                }
            }

            // cancel lessons
            var lessons = _context.Lessons
                                  .Include(x => x.Dates)
                                  .Include("Dates.LessonSlots")
                                  .Include("Dates.LessonSlots.Bookings")
                                  .Where(x => x.TeacherId == request.TeacherId && x.Status == LessonStatus.Published)
                                  .ToList();

            var lessonDates = new List<LessonDate>();
            var lessonDateIds = new List<long>();
            foreach (var lesson in lessons)
            {
                foreach (var lessonDate in lesson.Dates)
                {
                    lessonDates.Add(lessonDate);
                    lessonDateIds.Add(lessonDate.Id);
                }
            }

            var lessonDateCancellations = _context.LessonDateCancellations
                                                  .Where(x => lessonDateIds.Contains(x.LessonDateId))
                                                  .ToList();

            List<BookingCancelRequest> bookingCancellations = new List<BookingCancelRequest>();
            var requestStartDate = request.StartDate.ToUniversalTime();
            var utcOffset = TimeZoneInfo.Local.GetUtcOffset(request.StartDate);

            foreach (var date in request.Dates)
            {
                var startTime = date.StartTime.ToUniversalTime();
                var endTime = date.StartTime.ToUniversalTime().AddMinutes(request.DurationInMin);

                var todayLessons = lessons.Where(x => x.Dates.FirstOrDefault(d => d.Day == date.Day) != null).ToList();
                foreach (var lesson in todayLessons)
                {
                    var todayDates = lesson.Dates.Where(x => x.Day == date.Day).ToList();
                    foreach (var lessonDate in todayDates)
                    {
                        //var lessonDateStartTime = new DateTime(requestStartDate.Year, requestStartDate.Month, requestStartDate.Day, lessonDate.StartTime.Hour, lessonDate.StartTime.Minute, lessonDate.StartTime.Second);
                        var lessonTimeToLocal = lessonDate.StartTime.AddHours(utcOffset.Hours).AddMinutes(utcOffset.Minutes);
                        var lessonDateStartTime = new DateTime(request.StartDate.Year, request.StartDate.Month, request.StartDate.Day, lessonTimeToLocal.Hour, lessonTimeToLocal.Minute, lessonTimeToLocal.Second).ToUniversalTime();

                        // alter lessonStartTime if a different day has been selected
                        var cancellationDay = request.StartDate.DayOfWeek;
                        if (lessonDate.Day != cancellationDay)
                        {
                            if (lessonDate.Day < cancellationDay)
                            {
                                lessonDateStartTime = lessonDateStartTime.AddDays((int)cancellationDay);
                            } else if (lessonDate.Day > cancellationDay)
                            {
                                var offset = System.Enum.GetNames(typeof(DayOfWeek)).Length - (int)lessonDate.Day;
                                lessonDateStartTime = lessonDateStartTime.AddDays(offset + (int)cancellationDay);
                            }
                        }
                       
                        var lessonDateEndTime = lessonDateStartTime.AddMinutes(lesson.DurationInMin);
                        var lessonDateEndTimeAdjusted = lessonDateStartTime.AddMinutes(lesson.DurationInMin);

                        if (request.EndDate.HasValue)
                        {
                            var requestEndDate = request.EndDate.Value.ToUniversalTime();
                            var futureLessonDateEndTimeAdjusted = lessonDateEndTimeAdjusted.AddDays(7);
                            while (DateTime.Compare(futureLessonDateEndTimeAdjusted, requestEndDate) <= 0)
                            {
                                lessonDateEndTimeAdjusted = futureLessonDateEndTimeAdjusted;
                                futureLessonDateEndTimeAdjusted = futureLessonDateEndTimeAdjusted.AddDays(7);
                            }
                        }

                        // check if unavailability start/end time overlaps with any lesson date
                        if ((DateTime.Compare(lessonDateStartTime, startTime) == 0 && DateTime.Compare(lessonDateEndTime, startTime) == 0) ||
                            (DateTime.Compare(lessonDateStartTime, startTime) <= 0 && DateTime.Compare(lessonDateEndTime, endTime) >= 0) ||
                            (DateTime.Compare(lessonDateStartTime, startTime) >= 0 && DateTime.Compare(lessonDateStartTime, endTime) < 0) ||
                            (DateTime.Compare(lessonDateEndTime, startTime) > 0 && DateTime.Compare(lessonDateEndTime, endTime) <= 0))
                        {

                            // if unavailability has no end date, cancel entire lesson date
                            if (!request.EndDate.HasValue)
                            {
                                lessonDate.Status = LessonDateStatus.Cancelled;
                                lessonDate.CancelledDate = lessonDateStartTime;

                                // check bookings
                                foreach (var slot in lessonDate.LessonSlots)
                                {
                                    if (slot.Bookings.Count > 0)
                                    {
                                        foreach (var booking in slot.Bookings)
                                        {
                                            var bookingFinalStartDate = booking.StartDate.AddDays(7 * Math.Max(0, booking.TotalCount - 1));
                                            var currBookingDate = DateTime.SpecifyKind(booking.StartDate, DateTimeKind.Utc);
                                            var cancelAll = false;

                                            while (DateTime.Compare(currBookingDate, bookingFinalStartDate) <= 0 && !cancelAll)
                                            {
                                                if (DateTime.Compare(lessonDateStartTime, currBookingDate) == 0)
                                                {
                                                    bookingCancellations.Add(new BookingCancelRequest
                                                    {
                                                        BookingId = booking.Id,
                                                        CancelDate = currBookingDate,
                                                        CancelType = BookingCancelType.All
                                                    });
                                                    cancelAll = true;
                                                }
                                                currBookingDate = currBookingDate.AddDays(7);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // if unavailability end date proceeds lesson end date, cancel lesson date 
                                var cancelled = false;
                                //if (lesson.EndDate.HasValue)
                                //{
                                //    // TODO: cancel lesson date instead of adding to cancel table, as an optimization
                                //}

                                if (!cancelled)
                                {
                                    // if unavailability has a range, add to fixed cancellation table
                                    _context.LessonDateCancellations.Add(new LessonDateCancellation
                                    {
                                        LessonDateId = lessonDate.Id,
                                        CancelDate = lessonDateStartTime,
                                        CancelEndDate = lessonDateEndTimeAdjusted
                                    });

                                    // check bookings
                                    foreach (var slot in lessonDate.LessonSlots)
                                    {
                                        if (slot.Bookings.Count > 0)
                                        {
                                            var currLessonDate = lessonDateStartTime;
                                            while (DateTime.Compare(currLessonDate, lessonDateEndTimeAdjusted) <= 0)
                                            {
                                                foreach (var booking in slot.Bookings)
                                                {
                                                    var bookingFinalStartDate = booking.StartDate.AddDays(7 * Math.Max(0, booking.TotalCount - 1));
                                                    var currBookingDate = DateTime.SpecifyKind(booking.StartDate, DateTimeKind.Utc);
                                                    var dateFound = false;
                                                    while (DateTime.Compare(currBookingDate, bookingFinalStartDate) <= 0 && !dateFound)
                                                    {
                                                        if (DateTime.Compare(currLessonDate, currBookingDate) == 0)
                                                        {
                                                            bookingCancellations.Add(new BookingCancelRequest
                                                            {
                                                                BookingId = booking.Id,
                                                                CancelDate = currBookingDate,
                                                                CancelType = BookingCancelType.Single
                                                            });
                                                            dateFound = true;
                                                        }
                                                        currBookingDate = currBookingDate.AddDays(7);
                                                    }
                                                }
                                                currLessonDate = currLessonDate.AddDays(7);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }   
                }
            }
           

            // update lesson statuses if necessary
            // TODO: cancel lessons if all lesson dates are cancelled. Required to do above TODO first
            //foreach (var lesson in lessons)
            //{
            //    if (!lesson.Dates.Any(x => x.Status != LessonDateStatus.Cancelled))
            //    {
            //        lesson.Status = LessonStatus.Cancelled;
            //        lesson.CancelledDate = request.StartDate.ToUniversalTime();
            //    }
            //}

            _context.SaveChanges();

            return Tuple.Create(Mapper.Map<UnavailabilityData>(record), (IList<BookingCancelRequest>)bookingCancellations.ToList());
        }
    }
}
