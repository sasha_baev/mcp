namespace SAMii.Repository.EF.Migrations
{
    using SAMii.Repository.EF.Contexts;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;

    internal class MigrationList
    {
        internal MigrationList(BaseContext context)
        {
            LoadSQL(context, "Instruments.sql");
        }

        private void LoadSQL(BaseContext context, string scriptName)
        {
            try
            {
                var directory = AppDomain.CurrentDomain.BaseDirectory + "../../Migrations/SeedData/";
                var filepath = directory + scriptName;

                // ToDo: Validation prod path and remove below
                if (!File.Exists(filepath))
                {
                    directory = AppDomain.CurrentDomain.BaseDirectory + "../Migrations/SeedData/";
                    filepath = directory + scriptName;
                }

                var scriptFile = File.ReadAllLines(filepath);

                string[] ignore = new string[]
                {
                "GO", //Migrations does not support GO statement, will break 
                "/*", //No comments
                "print" //no print statements
                };

                string sql = "";
                foreach (var line in scriptFile)
                {
                    if (ignore.Any(i => line.StartsWith(i)) || string.IsNullOrWhiteSpace(line))
                        continue;
                    sql = sql + line + '\n';
                }

                context.Database.ExecuteSqlCommand(sql);
            }
            catch //(Exception e)
            {
                //log.Warn("Could not execute " + scriptName + ": " + e);
            }

        }
    }
}
