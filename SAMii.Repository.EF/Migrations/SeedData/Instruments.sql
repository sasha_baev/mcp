﻿USE [samii]
SET IDENTITY_INSERT [dbo].[Instrument] ON 
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (2, N'Piano Accordion', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (3, N'Button Accordion', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (4, N'Accompanist', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (5, N'Electronic Keyboard', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (6, N'Harpsichord', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (7, N'Acoustic Guitar', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (8, N'Organ - Electronic', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (9, N'Organ - Pipe', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (10, N'Organ - Theatre', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (11, N'Pianoforte', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (12, N'Bassoon', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (13, N'Clarinet', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (14, N'Flute', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (15, N'Oboe', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (16, N'Piccolo', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (17, N'Recorder', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (18, N'Saxophone', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (19, N'Violoncello', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (20, N'Double Bass', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (21, N'Bass Guitar', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (22, N'Classical Guitar', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (23, N'Commercial Guitar', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (24, N'Folk Guitar', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (25, N'Rock Guitar', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (26, N'Violin', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (27, N'Viola', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (28, N'Ukulele', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (29, N'French Horn', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (30, N'Trumpet', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (31, N'Drums', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (32, N'Percussion', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (33, N'Singing', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (34, N'Alexander Technique', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (35, N'Chamber Music', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (36, N'Composition', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (37, N'Harmony', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (38, N'History', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (39, N'Jazz Improvisation', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (40, N'Musicianship', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (41, N'Music Technology', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (42, N'Registered Music Therapist', 0)
INSERT [dbo].[Instrument] ([Id], [Name], [UserId]) VALUES (43, N'Theory', 0)
SET IDENTITY_INSERT [dbo].[Instrument] OFF