﻿using AutoMapper;
using SAMii.DTO;
using SAMii.DTO.Lessons;
using SAMii.DTO.Students;
using SAMii.DTO.Bookings;
using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMii.DTO.Parents;

namespace SAMii.Repository.EF
{
    public class AutoMapperConfigProfile : Profile
    {
        public AutoMapperConfigProfile()
        {
            CreateMap<Address, AddressData>();
            CreateMap<AddressData, Address>();
            CreateMap<Lesson, LessonData>().ForMember(dest => dest.LessonIntroductionPresignedUrl, opt => opt.Ignore());
            CreateMap<LessonData, Lesson>().ForMember(dest => dest.Instrument, opt => opt.Ignore())
                                           .ForMember(dest => dest.Teacher, opt => opt.Ignore())
                                           .ForMember(dest => dest.TeacherLocation, opt => opt.Ignore())
                                           .ForMember(dest => dest.Dates, opt => opt.Ignore())
                                           .ForMember(dest => dest.LessonResources, opt => opt.Ignore())
                                           .ForMember(dest => dest.Guid, opt => opt.Ignore());

            CreateMap<LessonDate, LessonDateData>();
            CreateMap<LessonDateData, LessonDate>().ForMember(dest => dest.LessonSlots, opt => opt.Ignore())
                                                   .ForMember(dest => dest.Lesson, opt => opt.Ignore());

            CreateMap<LessonDateCancellation, LessonDateCancellationData>();
            CreateMap<LessonDateCancellationData, LessonDateCancellation>().ForMember(dest => dest.LessonDate, opt => opt.Ignore());

            CreateMap<LessonSlot, LessonSlotData>().ForMember(dest => dest.BookingAmount, opt => opt.Ignore())
                                                   .ForMember(dest => dest.Bookings, opt => opt.Ignore());
            CreateMap<LessonSlotData, LessonSlot>().ForMember(dest => dest.Bookings, opt => opt.Ignore())
                                                   .ForMember(dest => dest.LessonDate, opt => opt.Ignore());

            CreateMap<Address, AddressData>().ReverseMap();

            CreateMap<User, UserData>().ForMember(dest => dest.AvatarPresignedUrl, opt => opt.Ignore());
            CreateMap<UserData, User>();

            CreateMap<TeacherData, Teacher>().ForMember(dest => dest.Educations, opt => opt.Ignore())
                                             .ForMember(dest => dest.Videos, opt => opt.Ignore())
                                             .ForMember(dest => dest.Clearances, opt => opt.Ignore())
                                             .ForMember(dest => dest.Testimonials, opt => opt.Ignore())
                                             .ForMember(dest => dest.User, opt => opt.Ignore())
                                             .ForMember(dest => dest.TeacherLocations, opt => opt.Ignore())
                                             .ForMember(dest => dest.Instruments, opt => opt.Ignore());
            CreateMap<Teacher, TeacherData>().ForMember(dest => dest.Instruments, opt => opt.Ignore())
                                             .ForMember(dest => dest.IntroductionMediaPresignedUrl, opt => opt.Ignore())
                                             .ForMember(dest => dest.BannerPresignedUrl, opt => opt.Ignore()); ;

            CreateMap<Education, EducationData>();
            CreateMap<EducationData, Education>().ForMember(dest => dest.Identifier, opt => opt.Condition(src => src.Identifier != Guid.Empty));
            CreateMap<Video, VideoData>().ReverseMap();

            CreateMap<ClearanceData, Clearance>();
            CreateMap<Clearance, ClearanceData>().ForMember(dest => dest.PresignedURL, opt => opt.Ignore())
                                                 .ForMember(dest => dest.NewClearance, opt => opt.Ignore())
                                                 .ForMember(dest => dest.FileData, opt => opt.Ignore())
                                                 .ForMember(dest => dest.MimeType, opt => opt.Ignore());

            CreateMap<Testimonial, TestimonialData>().ReverseMap();

            CreateMap<TeacherBilling, TeacherBillingData>();
            CreateMap<TeacherBillingData, TeacherBilling>().ForMember(dest => dest.Teacher, opt => opt.Ignore());

            CreateMap<Instrument, InstrumentData>();
            CreateMap<InstrumentData, Instrument>().ForMember(dest => dest.Teachers, opt => opt.Ignore());

            CreateMap<TeacherLocation, TeacherLocationData>();
            CreateMap<TeacherLocationData, TeacherLocation>().ForMember(dest => dest.Teacher, opt => opt.Ignore());

            CreateMap<LessonResourceData, LessonResource>();
            CreateMap<LessonResource, LessonResourceData>().ForMember(dest => dest.LessonResourcePresignedUrl, opt => opt.Ignore())
                                                           .ForMember(dest => dest.NewResource, opt => opt.Ignore())
                                                           .ForMember(dest => dest.FileData, opt => opt.Ignore())
                                                           .ForMember(dest => dest.MimeType, opt => opt.Ignore());

            CreateMap<Unavailability, UnavailabilityData>();
            CreateMap<UnavailabilityData, Unavailability>().ForMember(dest => dest.Dates, opt => opt.Ignore())
                                                           .ForMember(dest => dest.Teacher, opt => opt.Ignore());

            CreateMap<UnavailabilityDate, UnavailabilityDateData>();
            CreateMap<UnavailabilityDateData, UnavailabilityDate>().ForMember(dest => dest.Unavailability, opt => opt.Ignore());

            CreateMap<Student, StudentData>();
            CreateMap<StudentData, Student>().ForMember(dest => dest.User, opt => opt.Ignore());

            CreateMap<Booking, BookingData>().ReverseMap();
            CreateMap<BookingPayment, BookingPaymentData>().ReverseMap();
            CreateMap<BookingRefund, BookingRefundData>().ReverseMap();
            CreateMap<BookingAttendance, BookingAttendanceData>().ReverseMap();

            CreateMap<Parent, ParentData>();
            CreateMap<ParentData, Parent>().ForMember(dest => dest.User, opt => opt.Ignore());
        }
    }
}
