﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Util.AWS
{
    public class AwsS3
    {
        #region Private Member Variables

        private static AwsS3 _instance;

        private string BucketName = ConfigurationManager.AppSettings["AWS3.Bucket"];
        private string AccessKey = ConfigurationManager.AppSettings["AWS3.AccessKey"];
        private string SecretSccessKey = ConfigurationManager.AppSettings["AWS3.SecretKey"];
        private Amazon.RegionEndpoint AmazonEndpoint = Amazon.RegionEndpoint.APSoutheast2;

        #endregion

        #region Public static methods

        public static AwsS3 Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AwsS3();
                }
                return _instance;
            }
            set { _instance = value; }
        }

        #endregion

        #region Public methods

        public void UploadPdf(string key, string filePath)
        {
            UploadFile(key, filePath, "application/pdf");
        }

        public void UploadPdf(string key, Stream fileStream)
        {
            UploadFile(key, fileStream, "application/pdf");
        }

        public void UploadHtml(string key, Stream fileStream)
        {
            UploadFile(key, fileStream, "text/html");
        }

        public void UploadDoc(string key, Stream fileStream)
        {
            UploadFile(key, fileStream, "application/word");
        }

        public void UploadDocx(string key, Stream fileStream)
        {
            UploadFile(key, fileStream, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }

        public void UploadMp4Video(string key, string filePath)
        {
            UploadFile(key, filePath, "video/mp4");
        }

        public void UploadJpeg(string key, string filePath)
        {
            UploadFile(key, filePath, "image/jpeg");
        }

        public void UploadJpeg(string key, Stream fileStream)
        {
            UploadFile(key, fileStream, "image/jpeg");
        }

        public void UploadFile(string key, string filePath, string contentType)
        {
            using (AmazonS3Client s3 = new AmazonS3Client(this.AccessKey, this.SecretSccessKey, this.AmazonEndpoint))
            {
                PutObjectRequest por = new PutObjectRequest();
                por.Timeout = new TimeSpan(0, 30, 0);
                por.BucketName = this.BucketName;
                por.Key = key;
                por.FilePath = filePath;
                por.ContentType = contentType;

                s3.PutObject(por);
            }
        }

        public void UploadFile(string key, System.IO.Stream fileStream, string contentType)
        {
            using (AmazonS3Client s3 = new AmazonS3Client(this.AccessKey, this.SecretSccessKey, this.AmazonEndpoint))
            {
                PutObjectRequest por = new PutObjectRequest();
                por.Timeout = new TimeSpan(0, 30, 0);
                por.BucketName = this.BucketName;
                por.Key = key;
                por.InputStream = fileStream;
                por.ContentType = contentType;

                s3.PutObject(por);
            }
        }

        public Stream DownloadFile(string key)
        {
            using (AmazonS3Client s3 = new AmazonS3Client(this.AccessKey, this.SecretSccessKey, this.AmazonEndpoint))
            {
                Stream fileStream = new MemoryStream();
                GetObjectRequest request = new GetObjectRequest { BucketName = this.BucketName, Key = key };
                using (GetObjectResponse response = s3.GetObject(request))
                {
                    response.ResponseStream.CopyTo(fileStream);
                }
                fileStream.Position = 0;
                return fileStream;
            }
        }

        public void DeleteKey(string key)
        {
            using (AmazonS3Client s3 = new AmazonS3Client(this.AccessKey, this.SecretSccessKey, this.AmazonEndpoint))
            {
                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = this.BucketName,
                    Key = key
                };

                s3.DeleteObject(deleteObjectRequest);
            }
        }

        public string GetPresignedUrl(string key, DateTime expiryTime)
        {
            string url = null;
            using (AmazonS3Client s3 = new AmazonS3Client(this.AccessKey, this.SecretSccessKey, this.AmazonEndpoint))
            {
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
                request.BucketName = this.BucketName;
                request.Key = key;
                request.Expires = expiryTime;
                url = s3.GetPreSignedURL(request);
            }

            return url;
        }

        public string GetCachedPresignedUrl(string key, int expiryMinutes = 60)
        {
            return CachedUrl(key, expiryMinutes);
        }

        #endregion

        /// <summary>
        /// Grabs the cached PresignedUrl. Cache is set to 5 minutes, so if a request is made for the same S3 file (with the same expiry length) within 5 minutes of the first request it will return the cached Url rather than create a new one.
        /// </summary>
        /// <param name="s3Key">The key of the S3 file</param>
        /// <param name="expiryMinutes">The number of minutes this url should be valid for (from now) - actual expiry may be up to 5 minutes less than specified due to caching policy</param>
        /// <returns>The cached PresignedUrl</returns>
        private string CachedUrl(string s3Key, int expiryMinutes)
        {
            ObjectCache cache = MemoryCache.Default;
            string cacheKey = GetCacheKey(s3Key, expiryMinutes);
            string cachedUrl = cache[cacheKey] as string;

            if (cachedUrl == null)
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(5.0);
                cachedUrl = GetPresignedUrl(s3Key, DateTime.Now.AddMinutes(expiryMinutes));
                cache.Set(cacheKey, cachedUrl, policy);
            }

            return cachedUrl;
        }

        private static string GetCacheKey(string s3Key, int expiryMinutes)
        {
            return string.Format("{0}-{1}", s3Key, expiryMinutes);
        }
    }
}
