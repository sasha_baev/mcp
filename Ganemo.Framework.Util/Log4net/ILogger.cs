﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Util.Log4net
{
    public interface ILogger
    {
        void Error(Exception ex, string message);
        void Info(string message);
        void Warning(Exception ex, string message);
        void Warning(string message);
        void Debug(string message);
    }
}
