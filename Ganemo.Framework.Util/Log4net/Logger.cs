﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Util.Log4net
{
    public class Logger : ILogger
    {
        private string LogName = "API";

        public Logger()
        {
            XmlConfigurator.Configure();
        }

        public Logger(string logname)
        {
            this.LogName = logname;
            XmlConfigurator.Configure();
        }

        public void Error(Exception ex, string message)
        {
            Error(ex, message, LogName);
        }

        public void Error(Exception ex, string message, string logName)
        {
            //Log error here
            var log = LogManager.GetLogger(logName ?? LogName);
            log.Fatal(message, ex);
        }

        public void Error(string message)
        {
            //Log error here
            var log = LogManager.GetLogger(LogName);
            log.Error(message);
        }

        public void Warning(Exception ex, string message)
        {
            var log = LogManager.GetLogger(LogName);
            log.Warn(message, ex);
        }

        public void Warning(string message)
        {
            var log = LogManager.GetLogger(LogName);
            log.Warn(message);
        }

        public void Info(string message)
        {
            var log = LogManager.GetLogger(LogName);
            log.Info(message);
        }

        public void Debug(string message)
        {
            var log = LogManager.GetLogger(LogName);
            log.Debug(message);
        }
    }
}
