﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Util.Common
{
    public static class GUIDHelper
    {
        public static string GetTokenStringRepresentation(Guid token)
        {
            return token.ToString("N");
        }

        public static Guid? GetTokenFromStringRepresentation(string strGuid)
        {
            if (!string.IsNullOrWhiteSpace(strGuid))
            {
                var token = default(Guid);

                if (Guid.TryParseExact(strGuid, "N", out token))
                    return token;

                if (Guid.TryParse(strGuid, out token))
                    return token;
            }

            return null;
        }
    }
}
