﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Util.Email
{
    public class EmailSettings
    {
        public string ServerHost { get; set; }
        public string ServerPort { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FromEmail { get; set; }
        public string FromName { get; set; }
    }
}
