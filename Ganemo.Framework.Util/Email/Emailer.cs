﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Util.Email
{
    public class Emailer : IEmailer
    {
        private EmailSettings _emailSettings;

        public Emailer(EmailSettings emailSettings)
        {
            _emailSettings = emailSettings;
        }

        public void SendEmail(string toEmail, string subject, string body, byte[] attachment = null, string attachmentName = null)
        {
            SendEmail(new MailAddress(toEmail), null, subject, body, attachment, attachmentName);
        }

        public void SendEmail(MailAddress toEmail, MailAddress replyToEmail, string subject, string body, byte[] bytes, string attachementName)
        {
            try
            {
                var appSettings = System.Configuration.ConfigurationManager.AppSettings;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_emailSettings.UserName, _emailSettings.Password);

                int port = 25;
                string portStr = _emailSettings.ServerPort;

                if (!string.IsNullOrWhiteSpace(portStr))
                    int.TryParse(portStr, out port);

                SmtpClient smtp = new SmtpClient(_emailSettings.ServerHost, port);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                smtp.Credentials = credentials;

                MailAddress fromEmail = null;
                var fromEmailAddress = _emailSettings.FromEmail;
                var fromEmailName = _emailSettings.FromName;

                if (!string.IsNullOrWhiteSpace(fromEmailName))
                    fromEmail = new MailAddress(fromEmailAddress, fromEmailName);
                else
                    fromEmail = new MailAddress(fromEmailAddress);

                if (smtp != null && fromEmail != null)
                {
                    // create message
                    MailMessage mail = new MailMessage();
                    mail.From = fromEmail;

                    if (replyToEmail != null)
                        mail.ReplyToList.Add(replyToEmail);

                    mail.To.Add(toEmail);
                    mail.Subject = subject;
                    mail.Body = body;

                    mail.IsBodyHtml = true;

                    if (bytes != null)
                    {
                        mail.Attachments.Add(new Attachment(new MemoryStream(bytes), attachementName));
                    }

                    smtp.Send(mail);

                    Console.WriteLine("mail Sent");
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.ToString());
                throw ex;
            }
        }

    }
}
