﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Ganemo.Framework.Util.Email
{
    public interface IEmailer
    {
        void SendEmail(string toEmail, string subject, string body, byte[] attachment = null, string attachmentName = null);
        void SendEmail(MailAddress toEmail, MailAddress replyToEmail, string subject, string body, byte[] bytes, string attachementName);
    }
}
