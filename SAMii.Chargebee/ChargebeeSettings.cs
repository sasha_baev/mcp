﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Chargebee
{
    public class ChargebeeSettings
    {
        public string SiteKey { get; set; }
        public string AccessKey { get; set; }
    }
}
