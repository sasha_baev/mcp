﻿using SAMii.DTO.Bookings;
using SAMii.DTO.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Chargebee
{
    public interface IChargebeeApi
    {
        CardData GetSubscriptionCardData(string subscriptionId);
        SubscriptionCreateResponse CreateSubscription(SubscriptionCreateRequest request);
        ApiResponse UpdateSubscription(string tokenId, string subscriptionId);
        BookingPaymetResponse LessonBookingPayment(BookingAddOnModel model);
        BookingPaymetResponse RecurringBookingPaymentImmediate(BookingAddOnModel model, string description);
        BookingRefundResponse BookingRefunds(BookingRefundRequest model);
    }
}
