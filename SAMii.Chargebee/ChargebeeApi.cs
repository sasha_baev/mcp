﻿using ChargeBee.Api;
using ChargeBee.Models;
using log4net;
using Newtonsoft.Json;
using SAMii.DTO.Bookings;
using SAMii.DTO.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Chargebee
{
    public class ChargebeeApi : IChargebeeApi
    {
        private const string PLAN_NAME = "recurring";
        private const string PLAN_ADDON = "booking";
        private ChargebeeSettings _chargebeeSettings { get; set; }
        private ILog log = log4net.LogManager.GetLogger("ChargebeeApi");

        public ChargebeeApi(ChargebeeSettings chargebeeSettings)
        {
            _chargebeeSettings = chargebeeSettings;
            this.Init();
        }

        #region "Private Methods"
        private void Init()
        {
            ChargeBee.Api.ApiConfig.Configure(_chargebeeSettings.SiteKey, _chargebeeSettings.AccessKey);
        }
        private string getTokenStringRepresentation(Guid token)
        {
            return token.ToString("N");
        }
        private string getCardTypeDecription(Card.CardTypeEnum? cardType)
        {
            var description = "Unknown";
            if (cardType.HasValue)
            {
                switch (cardType.Value)
                {
                    case Card.CardTypeEnum.AmericanExpress:
                        description = "American Express";
                        break;
                    case Card.CardTypeEnum.DinersClub:
                        description = "Diners Club";
                        break;
                    case Card.CardTypeEnum.Discover:
                        description = "Discover";
                        break;
                    case Card.CardTypeEnum.Jcb:
                        description = "Jcb";
                        break;
                    case Card.CardTypeEnum.Mastercard:
                        description = "Mastercard";
                        break;
                    case Card.CardTypeEnum.Visa:
                        description = "Visa";
                        break;
                    case Card.CardTypeEnum.NotApplicable:
                        description = "Not Applicable";
                        break;
                    case Card.CardTypeEnum.Other:
                        description = "Other";
                        break;
                    default:
                        break;
                }
            }
            return description;
        }
        #endregion

        #region Public
        public CardData GetSubscriptionCardData(string subscriptionId)
        {
            CardData data = new CardData();
            try
            {
                EntityResult result = Subscription.Retrieve(subscriptionId)
                                        .Request();

                if (result != null && result.Card != null)
                {
                    data.CardType = this.getCardTypeDecription(result.Card.CardType);
                    data.Last4Digits = result.Card.Last4;
                    data.ExpiryMonth = result.Card.ExpiryMonth;
                    data.ExpiryYear = result.Card.ExpiryYear;

                    data.Success = true;
                }
            }
            catch (Exception ex)
            {
                this.log.Error("CardData: " + subscriptionId, ex);
                data.ErrorMsg = ex.Message;
            }
            return data;
        }
        public SubscriptionCreateResponse CreateSubscription(SubscriptionCreateRequest request)
        {
            SubscriptionCreateResponse response = new SubscriptionCreateResponse();

            try
            {
                Subscription.CreateRequest subscriptionRequest =
                    Subscription.Create()
                                .PlanId(PLAN_NAME)
                                .Id(this.getTokenStringRepresentation(request.User.Identifier)) // ---> Id is same for Customer & Subscription
                                .CustomerEmail(request.User.Email)
                                .CustomerFirstName(request.User.FirstName)
                                .CustomerLastName(request.User.LastName)
                                .TokenId(request.CB_Token);


                EntityResult result = subscriptionRequest.Request();

                if (result?.Subscription != null)
                {
                    Subscription subscription = result.Subscription;

                    response.Success = true;
                    response.SubscriptionId = subscription.Id;
                    response.PlanId = subscription.PlanId;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMsg = ex.Message;
                this.log.Error("CreateSubscription: " + request.User.Identifier, ex);
            }
            return response;
        }
        public ApiResponse UpdateSubscription(string tokenId, string subscriptionId)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                EntityResult result = Subscription.Update(subscriptionId)
                                                .TokenId(tokenId)
                                                .Request();

                if (result != null && result.Subscription != null && result.Card != null)
                    response.Success = true;
            }
            catch (Exception ex)
            {
                response.ErrorMsg = ex.Message;
                this.log.Error("UpdateSubscription: " + subscriptionId, ex);
            }
            return response;
        }

        public BookingPaymetResponse LessonBookingPayment(BookingAddOnModel model)
        {
            BookingPaymetResponse response = new BookingPaymetResponse();
            try
            {
                EntityResult result = Invoice.ChargeAddon()
                                                .SubscriptionId(model.SubscriptionId)
                                                .AddonId(PLAN_ADDON)
                                                .AddonUnitPrice(model.UnitPrice)
                                                .AddonQuantity(model.LessonsQunatity)
                                                .Request();

                if (result != null && result.Invoice != null && result.Invoice.AmountPaid.HasValue)
                {
                    BookingPaymentData data = new BookingPaymentData();
                    data.GatewayId = result.Invoice.Id;
                    data.Quantity = model.LessonsQunatity;
                    data.Amount = this.GetAmountPaid(result.Invoice);
                    response.Payment = data;
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMsg = ex.Message;
                this.log.Error("LessonBookingAddOn: " + model.SubscriptionId, ex);
            }
            return response;
        }
        private decimal GetAmountPaid(Invoice invoice)
        {
            decimal amount = 0.0M;

            if (invoice != null && invoice.AmountPaid.HasValue)
            {
                amount = Convert.ToDecimal(invoice.AmountPaid.Value / 100); //Convert the amount from cents to dollars
            }

            return amount;
        }


        public BookingPaymetResponse RecurringBookingPaymentTermEnd(BookingAddOnModel model, string description)
        {
            BookingPaymetResponse response = new BookingPaymetResponse();
            try
            {
                int usageCost = model.LessonsQunatity * model.UnitPrice;

                EntityResult result = Subscription.AddChargeAtTermEnd(model.SubscriptionId)
                        .Amount(usageCost) // Amount in cents $35 per day / per device 
                        .Description(description)
                        .Request();

                Estimate estimate = result.Estimate;


                if (estimate != null && estimate.InvoiceEstimate != null)
                {
                    response.Payment = new BookingPaymentData();
                    foreach (InvoiceEstimate.InvoiceEstimateLineItem lineItem in estimate.InvoiceEstimate.LineItems)
                    {
                        if (string.Equals(lineItem.Description(), description))
                        {
                            response.Payment.GatewayId = lineItem.Id();
                        }
                    }
                }

                // if gatewayId for the unique description is not found, return false
                response.Success = !String.IsNullOrEmpty(response.Payment.GatewayId);
                if (!response.Success)
                {
                    response.ErrorMsg = "Failed to add lesson charge";
                    this.log.Info(response.ErrorMsg + " : " + model.SubscriptionId);
                }
            }
            catch (ApiException ex)
            {
                response.Success = false;
                response.ErrorMsg = ex.Message;
                this.log.Error("Add Lesson Charge: " + model.SubscriptionId, ex);
            }

            return response;
        }

        public BookingPaymetResponse RecurringBookingPaymentImmediate(BookingAddOnModel model, string description)
        {
            BookingPaymetResponse response = new BookingPaymetResponse();
            try
            {
                int lessonCost = model.LessonsQunatity * model.UnitPrice;

                EntityResult result = Invoice.Charge()
                                                .SubscriptionId(model.SubscriptionId)
                                                .Amount(lessonCost)
                                                .Description(description)
                                                .Request();

                if (result != null && result.Invoice != null && result.Invoice.AmountPaid.HasValue)
                {
                    BookingPaymentData data = new BookingPaymentData();
                    data.GatewayId = result.Invoice.Id;
                    data.Quantity = model.LessonsQunatity;
                    data.Amount = this.GetAmountPaid(result.Invoice);
                    response.Payment = data;
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMsg = ex.Message;
                this.log.Error("Add Lesson Charge: " + model.SubscriptionId, ex);
            }
            return response;
            #endregion
        }
        public BookingRefundResponse BookingRefunds(BookingRefundRequest model)
        {
            BookingRefundResponse response = new BookingRefundResponse();
            try
            {
                EntityResult result = Invoice.Refund(model.InvoiceId)
                                            .RefundAmount(model.RefundAmount)
                                            .Comment("Booking Cacnellation for "+ model.Quantity + " lessons")
                                            .Request();

                if (result != null && result.CreditNote != null)
                {
                    BookingRefundData data = new BookingRefundData();
                    data.BookingId = model.BookingId;
                    data.GatewayId = result.CreditNote.ReferenceInvoiceId;
                    data.Quantity = model.Quantity;
                    data.Amount = Convert.ToDecimal(result.CreditNote.Total.Value / 100);
                    response.Payment = data;
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMsg = ex.Message;
                this.log.Error("RefundError: " + model.BookingId, ex);
            }
            return response;
        }
    }
}
