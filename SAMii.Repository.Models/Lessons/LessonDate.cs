﻿using SAMii.Enum;
using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class LessonDate : BaseModel
    {
        public long LessonId { get; set; }
        public Lesson Lesson { get; set; }

        public DayOfWeek Day { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime? BreakStartTime { get; set; }
        public DateTime? CancelledDate { get; set; }
        public LessonDateStatus Status { get; set; }

        public ICollection<LessonSlot> LessonSlots { get; set; }
        public ICollection<LessonDateCancellation> Cancellations { get; set; }
    }
}
