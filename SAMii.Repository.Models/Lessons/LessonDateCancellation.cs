﻿using SAMii.Enum;
using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class LessonDateCancellation : BaseModel
    {
        public long LessonDateId { get; set; }
        public LessonDate LessonDate { get; set; }

        public DateTime CancelDate { get; set; }
        public DateTime CancelEndDate { get; set; }

    }
}
