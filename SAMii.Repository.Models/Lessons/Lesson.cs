﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Lesson : BaseModel
    {
        public long TeacherId { get; set; }
        public Teacher Teacher { get; set; }

        public Guid Guid { get; set; }

        // STEP 1 - Details
        public string Title { get; set; }
        public long DurationInMin { get; set; }
        public bool IsGroupLesson { get; set; }
        public int StudentPerGroup { get; set; }
        public decimal Price { get; set; }
        public bool IsCostAbsorb { get; set; }

        public long InstrumentId { get; set; }
        public Instrument Instrument { get; set; }

        public long TeacherLocationId { get; set; }
        public TeacherLocation TeacherLocation { get; set; }

        // STEP 2 - Teach
        public string LessonIntroductionS3Key { get; set; }
        public string LessonIntroductionFileName { get; set; }
        public string LessonIntroductionVideoUrl { get; set; }
        public string LessonContent { get; set; } // How I Teach
        public string LessonMethod { get; set; } // What I Teach

        // STEP 3 - Dates
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CancelledDate { get; set; }
        public ICollection<LessonDate> Dates { get; set; }

        // STEP 4 - Resources
        public ICollection<LessonResource> LessonResources { get; set; }

        public LessonStatus Status { get; set; }
        public int LessonStep { get; set; }

        public LessonRecurrence RecurrenceType { get; set; }
        public LessonRepeat? RepeatType { get; set; }
        public int? RepeatAmount { get; set; }
    }
}
