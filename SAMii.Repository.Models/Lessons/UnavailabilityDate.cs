﻿using SAMii.Enum;
using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class UnavailabilityDate : BaseModel
    {
        public long UnavailabilityId { get; set; }
        public Unavailability Unavailability { get; set; }

        public DayOfWeek Day { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
