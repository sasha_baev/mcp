﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class LessonResource : BaseModel
    {
        public long LessonId { get; set; }
        public Lesson Lesson { get; set; }

        public string FileName { get; set; }
        public string LessonResourceS3Url { get; set; }
    }
}
