﻿using SAMii.Enum;
using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Unavailability : BaseModel
    {
        public long TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int DurationInMin { get; set; }

        public ICollection<UnavailabilityDate> Dates { get; set; }

        public LessonRecurrence RecurrenceType { get; set; }
        public LessonRepeat? RepeatType { get; set; }
        public int? RepeatAmount { get; set; }
    }
}
