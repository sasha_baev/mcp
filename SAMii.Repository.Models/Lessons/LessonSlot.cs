﻿using SAMii.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class LessonSlot : BaseModel
    {
        public long LessonDateId { get; set; }
        public LessonDate LessonDate { get; set; }

        public DateTime StartTime { get; set; }

        public ICollection<Booking> Bookings { get; set; }
    }
}
