﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class BookingCancellation : BaseModel
    {
        public long BookingId { get; set; }
        public Booking Booking { get; set; }
        public DateTime CancelledDate { get; set; }
        public BookingCancelType CancelType { get; set; }
    }
}
