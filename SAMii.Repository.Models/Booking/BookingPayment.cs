﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class BookingPayment : BaseModel
    {
        public long BookingId { get; set; }
        public Booking Booking { get; set; }
        public string GatewayId { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
    }
}
