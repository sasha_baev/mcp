﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class BookingAttendance : BaseModel
    {
        public long BookingId { get; set; }
        public Booking Booking { get; set; }
        public DateTime AttendedDate { get; set; }
        public bool TeacherPaid { get; set; }
        public long? TeacherPayoutId { get; set; }
        public TeacherPayout TeacherPayout { get; set; }
    }
}
