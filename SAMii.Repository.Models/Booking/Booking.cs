﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Booking : BaseModel
    {
        public long StudentId { get; set; }
        public Student Student { get; set; }
        public long LessonSlotId { get; set; }
        public LessonSlot LessonSlot { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public BookingStatus Status { get; set; }
        public BookingPaymentType PaymentType { get; set; }
        public bool Cancelled { get; set; }
        public DateTime? CancelledEndDate { get; set; }
        public int TotalCount { get; set; }
        public int AttendedCount { get; set; }
        public int InCreditCount { get; set; }
        public ICollection<BookingPayment> Payments { get; set; }
        public ICollection<BookingCancellation> Cancellations { get; set; }
    }
}
