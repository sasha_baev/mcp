﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Clearance : BaseModel
    {
        public long TeacherProfileId { get; set; }
        public Teacher Teacher { get; set; }

        public string ClearanceS3Key { get; set; }
        public string FileName { get; set; }

        public Guid GUID { get; set; }
    }
}
