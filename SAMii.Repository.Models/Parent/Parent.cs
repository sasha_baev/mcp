﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Parent : BaseModel
    {
        public long UserId { get; set; }
        public User User { get; set; }

        public ICollection<Student> Students { get; set; }
    }
}
