﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Education : BaseModel
    {
        public long TeacherProfileId { get; set; }
        public Teacher Teacher { get; set; }

        public string University { get; set; }
        public string Degree { get; set; }
        public string Major { get; set; }
        public int Year { get; set; }

        public Guid Identifier { get; set; }

        public bool IsUrl { get; set; }
        public string UrlLink { get; set; }
        public string AttachmentS3Key { get; set; }
        public string AttachmentName { get; set; }
    }
}
