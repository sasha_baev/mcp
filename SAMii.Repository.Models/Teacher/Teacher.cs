﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Teacher : BaseModel
    {
        // Step 2 - Self Introduction
        public string BannerS3Key { get; set; }
        public string BannerFileName { get; set; }
        public string IntroductionMediaS3Key { get; set; }
        public string IntroductionMediaFileName { get; set; }
        public string IntroductionMediaVideoUrl { get; set; }
        public string Introduction { get; set; }
        public string TeachingContent { get; set; }
        public string TeachingMethod { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedInLink { get; set; }
        public string InstagramLink { get; set; }
        public string YouTubeLink { get; set; }
        public ICollection<TeacherLocation> TeacherLocations { get; set; }

        // Step 3 - Credibility
        public ICollection<Video> Videos { get; set; }
        public ICollection<Education> Educations { get; set; }
        public ICollection<Clearance> Clearances { get; set; }
        public ICollection<Testimonial> Testimonials { get; set; }

        public bool TeachingAddressAlsoResidential { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        public ICollection<Lesson> Lessons { get; set; }
        public TeacherBilling TeacherBilling { get; set; }

        public ICollection<InstrumentTeacher> Instruments { get; set; }

        public bool Public { get; set; }
    }
}
