﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class TeacherPayout : BaseModel
    {
        public long TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceRef { get; set; }
        public string InvoiceId { get; set; }
        public decimal AmountPaid { get; set; }
        public ICollection<BookingAttendance> Bookings { get; set; }
    }
}
