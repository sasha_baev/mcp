﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class TeacherLocation : BaseModel
    {
        public string Name { get; set; }
        public long AddressId { get; set; }
        public Address Address { get; set; }
        public long TeacherId { get; set; }
        public Teacher Teacher { get; set; }
    }
}
