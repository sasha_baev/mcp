﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class TeacherBilling
    {
        [ForeignKey("Teacher")]
        public long TeacherBillingId { get; set; }

        public string ABN { get; set; }
        public bool IsGSTRegister { get; set; }
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public bool IsWeeklyPayment { get; set; }

        public string XeroContactId { get; set; }
        public Teacher Teacher { get; set; }
    }
}
