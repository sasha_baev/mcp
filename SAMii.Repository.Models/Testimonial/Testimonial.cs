﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Testimonial : BaseModel
    {
        public long TeacherId { get; set; }
        public Teacher Teacher { get; set; }

        public string Name { get; set; }
        public string Comment { get; set; }
        public double Rating { get; set; }

        public string TestimonialS3Key { get; set; }
        public string FileName { get; set; }
        public Guid Identifier { get; set; }
    }
}
