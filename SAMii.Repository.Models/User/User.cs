﻿using SAMii.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class User : BaseModel
    {
        public User()
        {
            CreatedDate = DateTime.UtcNow;
            Updated = DateTime.UtcNow;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserType RoleType { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Guid Identifier { get; set; }
        public string AvatarS3Key { get; set; }
        public string AvatarFileName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime Updated { get; set; }
        public bool Activated { get; set; }
        public bool Suspended { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastSignInDate { get; set; }
        public Guid? ResetToken { get; set; }
        public DateTime? ResetTokenExpiry { get; set; }
        public Guid? ActivationToken { get; set; }
        public string LastIp { get; set; }
        public DateTime? ActivationTokenExpiry { get; set; }

        public bool IsInvited { get; set; }

        public DateTime? DOB { get; set; }
        public string PhoneNumber { get; set; }

        public long? AddressId { get; set; }
        public Address Address { get; set; }

        public int ProfileStep { get; set; }
        public SubscriptionStatus BillingStatus { get; set; }
    }
}
