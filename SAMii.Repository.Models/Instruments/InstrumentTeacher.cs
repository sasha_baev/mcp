﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class InstrumentTeacher : BaseModel
    {
        public long TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        public long InstrumentId { get; set; }
        public Instrument Instrument { get; set; }
    }
}
