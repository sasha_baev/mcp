﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Instrument : BaseModel
    {
        public string Name { get; set; }
        public long UserId { get; set; }

        public ICollection<InstrumentTeacher> Teachers { get; set; }
    }
}
