﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    /// <summary>
    /// Base of all classes.   Contains ID only
    /// </summary>
    public class BaseModel
    {
        [Column("Id", Order = 0)]
        public long Id { get; set; }
    }
}
