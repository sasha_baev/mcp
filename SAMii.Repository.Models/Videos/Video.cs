﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class Video : BaseModel
    {
        public long TeacherId { get; set; }
        public Teacher Teacher { get; set; }

        public string Url { get; set; }
    }
}
