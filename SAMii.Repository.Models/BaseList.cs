﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace SAMii.Repository.Models
{
    public class BaseList : BaseModel
    {
        [Column("Name", Order = 1)]
        public string Name { get; set; }
    }
}
