﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models.Common
{
    public class AppSetting : BaseModel
    {
        public DateTime? LastFornight { get; set; }
    }
}
