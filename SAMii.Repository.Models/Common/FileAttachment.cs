﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Repository.Models
{
    public class FileAttachment : BaseList
    {
        public FileAttachment()
        {
            this.Created = DateTime.UtcNow;
        }

        public string FileType { get; set; }

        [Required]
        [DataType(DataType.Upload)]
        public byte[] Content { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime Created { get; set; }
    }
}
