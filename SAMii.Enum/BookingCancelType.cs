﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum BookingCancelType
    {
        Single = 1,
        All = 2
    }
}
