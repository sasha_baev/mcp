﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum TokenResponseEnum
    {
        NoError = 0,
        TokenExpired = 1,
        TokenInvalid =2
    }
}
