﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum LoginResponseEnum
    {
        NoError = 0,
        NeedsBoth = 1,
        NeedsPassword = 2,
        NeedsName = 3,
        Unknown = 4,
        Suspended = 5,
        NotActivated = 6,
        LicenceExpired = 7
    }
}
