﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum LessonStatus
    {
        None = 0,
        Published = 1,
        Cancelled = 2
    }
}
