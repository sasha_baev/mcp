﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum BookingPaymentType
    {
        None = 0,
        Fixed = 1,
        Reoccurring = 2
    }
}
