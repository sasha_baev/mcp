﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum BookingStatus
    {
        None = 0,
        Active = 1,
        Cancelled = 2
    }
}
