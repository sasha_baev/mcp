﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum LessonRepeat
    {
        Infinite = 0,
        Date = 1,
        Amount = 2
    }
}
