﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum UserType
    {
       None = 0,
       MasterAdmin = 1,
       Admin = 2,
       Teacher = 3,
       Student = 4,
       Parent = 5,
       TeacherAdmin = 6
    }
}
