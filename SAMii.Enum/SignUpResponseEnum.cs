﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum SignUpResponseEnum
    {
        Fail = 0,

        [Description("Account Created")]
        Success = 1,

        [Description("Email addresses do not match.")]
        FailEmailAddressesNotMatch = 2,

        [Description("Email address already in use.")]
        FailEmailAddressInUse = 3,

        [Description("Invalid password. Valid password is between 6 and 32 characters long, it must contain at least one lowercase, one uppercase, and one digit character.")]
        FailPasswordNotValid = 4,
    }
}
