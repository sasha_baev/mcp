﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum PasswordChangeEnum
    {
        Success = 0,
        InvalidCurrentPassword = 1,
        InvalidNewPassword = 2,
    }
}
