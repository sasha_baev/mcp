﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Enum
{
    public enum InstrumentEnum
    {
        Guitar = 1,
        Drum = 2,
        Piano = 3
    }
}
