﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Xero.Models
{
    public class ContactModel
    {
        public string Fullname { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }

        public string ABN { get; set; }
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }

        public string XeroContactId { get; set; }

        public string GetFullName()
        {

            if (string.IsNullOrEmpty(Lastname))
                return Firstname;
            else if (string.IsNullOrEmpty(Firstname))
                return Lastname;

            return string.Format("{0} {1}", Firstname, Lastname);
        }

        public string GetBankDetails()
        {
            return string.Format("{0}-{1}", BSB, AccountNumber);
        }

        public void UpdateBankDetails(string details)
        {
            if (!string.IsNullOrWhiteSpace(details))
            {
                string[] values = details.Split("-".ToCharArray());
                if (values.Length > 0)
                {
                    this.BSB = values[0];
                    this.AccountNumber = values[1];
                }
            }
        }
    }
}
