﻿using SAMii.Xero.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xero.Api.Core;
using Xero.Api.Core.Model;

namespace SAMii.Xero
{
    public class ContactManager
    {
        public static Contact GetContact(XeroCoreApi API, ContactModel model)
        {
            try
            {
                string query = string.Format("Name != null AND Name.ToLower().Equals(\"{0}\")", model.Fullname.ToLower());
                Contact contact = API.Contacts.Where(query).Find().FirstOrDefault();

                return contact;
            }
            catch { }
            return null;
        }

        public static Contact FindContact(XeroCoreApi API, string id)
        {
            Contact contact = null;

            if (!string.IsNullOrWhiteSpace(id))
            {
                Guid guid = new Guid(id);
                contact = API.Contacts.Find(guid);
            }

            return contact;
        }

        public static Contact CreateContact(XeroCoreApi API, ContactModel model)
        {
            Contact contact = null;

            if (model != null)
            {
                contact = new Contact()
                {
                    Name = model.GetFullName(),
                    FirstName = model.Firstname,
                    LastName = model.Lastname,
                    EmailAddress = model.Email,
                    TaxNumber = model.ABN,
                    BankAccountDetails = model.GetBankDetails(),
                    //BatchPayments = new BatchPaymentContactDefaults()
                    //{
                    //   BankAccountName = model.AccountName,
                    //   BankAccountNumber = model.AccountNumber,
                    //   Code = model.BSB
                    //}
                };

                contact = API.Contacts.Create(contact);
            }
            return contact;
        }

        public static Contact UpdateContact(XeroCoreApi API, ContactModel model)
        {
            Contact contact = null;

            if (model != null)
            {
                contact = ContactManager.FindContact(API, model.XeroContactId);

                if (contact != null)
                {
                    contact.TaxNumber = model.ABN;
                    contact.BankAccountDetails = model.GetBankDetails();

                    contact = API.Contacts.Update(contact);
                }
            }
            return contact;
        }
    }
}