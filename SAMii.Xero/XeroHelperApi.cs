﻿using log4net;
using SAMii.DTO.Teachers;
using SAMii.Xero.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Xero.Api.Core;
using Xero.Api.Core.Model;
using Xero.Api.Example.Applications.Private;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;

namespace SAMii.Xero
{
    public class XeroHelperApi : IXeroHelperApi
    {
        internal XeroSettings xeroSettings { get; set; }
        internal ILog log = log4net.LogManager.GetLogger("XeroApi");
        internal XeroCoreApi API { get; set; }
        internal ContactGroup ContactGroup { get; set; }
        internal InvoiceManager invoiceManager = null;

        public XeroHelperApi(XeroSettings settings)
        {
            this.xeroSettings = settings;
            this.Init();
        }

        #region "Private Methods"
        private void Init()
        {

            string certificateFilepath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase), this.xeroSettings.CertificateFilepath);
            certificateFilepath = new Uri(certificateFilepath).LocalPath;

            //X509Certificate2 cert = new X509Certificate2(certificateFilepath, this.xeroSettings.CertificatePassword);

            this.API = new XeroCoreApi(this.xeroSettings.ApiURL, new PrivateAuthenticator(certificateFilepath, this.xeroSettings.CertificatePassword),
                new Consumer(this.xeroSettings.ConsumerKey, this.xeroSettings.ConsumerSecret), null,
                new DefaultMapper(), new DefaultMapper());

            invoiceManager = new InvoiceManager(this.API);
        }
        #endregion

        #region Public Methods
        public string CreateContact(ContactModel model)
        {
            //Create unique contact
            Contact existingContact = null;
            var lastname = model.Lastname;
            int num = 1;
            do
            {
                existingContact = ContactManager.GetContact(this.API, model);

                if (existingContact != null)
                {
                    model.Lastname = string.Concat(lastname, " (#", num, ")");
                    model.Fullname = model.GetFullName();
                    num++;
                }

            } while (existingContact != null);

            Contact contact = ContactManager.CreateContact(this.API, model);
            return contact.Id.ToString();
        }

        public bool UpdateContact(ContactModel model)
        {
            Contact contact = ContactManager.UpdateContact(this.API, model);
            return contact != null;
        }

        public ContactModel GetContact(string contactId)
        {
            Contact contact = ContactManager.FindContact(API, contactId);

            if(contact != null)
            {
                ContactModel model = new ContactModel()
                {
                    Firstname = contact.FirstName,
                    Lastname = contact.LastName,
                    Email = contact.EmailAddress,
                    ABN = contact.TaxNumber
                };
                model.UpdateBankDetails(contact.BankAccountDetails);
                return model;
            }
            return null;
        }

        public bool CreateBill(TeacherPayoutData data)
        {
            bool created = false;

            if (!string.IsNullOrWhiteSpace(data.XeroContactId))
            {
                Contact contact = ContactManager.FindContact(API, data.XeroContactId);

                if (contact != null)
                {
                    var invoice = this.invoiceManager.CreateInvoice(data, contact, xeroSettings.AccountCode);

                    if (invoice != null && invoice.Id != null)
                    {
                        data.InvoiceId = invoice.Id.ToString();
                        data.AmountPaid = invoice.Total.Value;
                        data.InvoiceDate = invoice.DueDate.Value;

                        created = true;
                    }
                }
            }

            return created;
        }
        #endregion
    }
}
