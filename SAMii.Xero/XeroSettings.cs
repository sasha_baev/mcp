﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Xero
{
    public class XeroSettings
    {
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string CertificateFilepath { get; set; }
        public string CertificatePassword { get; set; }
        public string ApiURL { get; set; }
        public string AccountCode { get; set; }
    }
}
