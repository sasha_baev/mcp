﻿using SAMii.DTO.Teachers;
using SAMii.DTO.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xero.Api.Core;
using Xero.Api.Core.Model;
using Xero.Api.Core.Model.Status;
using Xero.Api.Core.Model.Types;

namespace SAMii.Xero
{
    public class InvoiceManager
    {
        private XeroCoreApi _api = null;
        public InvoiceManager(XeroCoreApi api)
        {
            this._api = api;
        }
        public Invoice FindInvoice(string id)
        {
            Invoice invoice = null;

            if (!string.IsNullOrWhiteSpace(id))
            {
                Guid guid = new Guid(id);
                invoice = this._api.Invoices.Find(guid);
            }

            return invoice;
        }

        public Invoice CreateInvoice(TeacherPayoutData model, Contact contact, string accountCode)
        {
            if (model != null && contact != null)
            {
                List<LineItem> items = new List<LineItem>();
                foreach (InvoiveLineItem obj in model.LineItems)
                {
                    LineItem item = new LineItem
                    {
                        AccountCode = accountCode,
                        Description = obj.Description,
                        UnitAmount = obj.UnitAmount,
                        Quantity = obj.Quantity,
                        TaxType = "BASEXCLUDED",
                    };
                    items.Add(item);
                }

                var invoice = this._api.Invoices.Create(
                  new Invoice
                  {
                      Contact = contact,
                      DueDate = DateTime.UtcNow.AddDays(1),
                      Type = InvoiceType.AccountsPayable,
                      LineAmountTypes = LineAmountType.NoTax,
                      Reference = model.InvoiceRef,
                      Number = model.InvoiceRef,
                      Status = InvoiceStatus.Authorised,
                      LineItems = items
                  });

                return invoice;
            }
            return null;
        }
    }
}
