﻿using SAMii.DTO.Teachers;
using SAMii.Xero.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAMii.Xero
{
    public interface IXeroHelperApi
    {
        string CreateContact(ContactModel model);
        bool UpdateContact(ContactModel model);
        ContactModel GetContact(string contactId);
        bool CreateBill(TeacherPayoutData data);
    }
}
